#pragma once
#include <cstring>
#include <iostream>
#include <cassert>
#include <string>
using namespace std;

namespace string_realize
{
	class string
	{
	public:
		//版本1：可读可写
		typedef char* iterator; // 迭代器

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		//版本2：只读，const对象可调用
		typedef const char* const_iterator;

		const_iterator begin() const
		{
			return _str;
		}

		const_iterator end() const
		{
			return _str + _size;
		}

		// 带参构造
		// string(const char* str = nullptr)———程序崩溃
		// string(const char* str = '\0')————程序崩溃
		// string(const char* str = "\0")————正常
		string(const char* str = "") // 全缺省构造函数
		{
			_size = strlen(str);
			_capacity = _size;
			_str = new char[_capacity + 1];
			strcpy(_str, str);
		}
		// 默认构造 写了全缺省就不用写无参构造
		/*string()
		{
			_str = new char[1];
			_str[0] = '\0';
			_size = _capacity = 0;
		}*/

		// 拷贝构造 s2(s1) 调BUG调了好久，原来是没写你啊！！！
		// 传统写法
		/*string(const string& s)
		{
			_str = new char[s._capacity + 1];
			_capacity = s._capacity;
			_size = s._size;

			strcpy(_str, s._str);
		}*/

		void swap(string& s)
		{
			std::swap(_str, s._str);
			std::swap(_size, s._size);
			std::swap(_capacity, s._capacity);
		}

		// 现代写法
		string(const string& s)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			string tmp(s._str); // 构造函数
			// this->swap(tmp);
			swap(tmp);
		}

		// 传统写法
		// s = s3; 赋值重载
		/*string& operator=(const string& s)
		{
			if (this != &s)
			{
				char* tmp = new char[s._capacity + 1];
				strcpy(tmp, s._str);

				delete[] _str;
				_str = tmp;
				
				_size = s._size;
				_capacity = s._capacity;
			}

			return *this;
		}*/
		// //现代写法
		//string& operator=(const string& s)
		//{
		//	if (this != &s)
		//	{
		//		//string tmp(s._str);
		//		string tmp(s);
		//		swap(tmp);
		//	}

		//	return *this;
		//}
		// 传值传参
		string& operator=(string s)
		{
			swap(s);
			return *this;
		}

		// 析构函数
		~string()
		{
			delete[] _str;
			_str = nullptr;
			_capacity = _size = 0;
		}

		const char* c_str() const
		{
			return _str;
		}

		size_t size() const
		{
			return _size;
		}

		size_t capacity() const
		{
			return _capacity;
		}

		// 普通对象：可读可写
		char& operator[](size_t pos)
		{
			assert(pos < _size);
			return _str[pos];
		}
		 
		//const对象：只读
		const char& operator[](size_t pos) const
		{
			assert(pos < _size);
			return _str[pos];
		}

		void reserve(size_t n) 
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete[] _str;
				_str = tmp;

				_capacity = n;
			}	
		}

		void resize(size_t n, char ch = '\0')
		{
			if (n > _size)
			{
				reserve(n);
				for (size_t i = _size; i < n; i++)
				{
					_str[i] = ch;
				}

				_size = n;
				_str[_size] = '\0';
			}
			else
			{
				_str[n] = '\0';
				_size = n;
			}
		}

		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(newCapacity);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		void append(const char* str)
		{
			size_t len = strlen(str);
			if (_size + len > _capacity)
			{
				reserve(_size + len);
			}
			strcpy(_str + _size, str);
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		string& insert(size_t pos, char ch)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(newCapacity);
			}

			// 挪动数据
			/*int end = _size;
			while (end >= (int)pos)
			{
				_str[end + 1] = _str[end];
				end--;
			}*/
			size_t end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}

			_str[pos] = ch;
			_size++;

			return *this;
		}

		string& insert(size_t pos, const char* str)
		{
			assert(pos <= _size);
			if (_size == _capacity)
			{
				size_t newCapacity = _capacity == 0 ? 4 : 2 * _capacity;
				reserve(newCapacity);
			}

			/*size_t len = strlen(str);
			int end = _size;
			// >= 会发生类型提升，需要强转
			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				end--;
			}*/
			size_t len = strlen(str);
			size_t end = _size + len;
			while (end > pos + len - 1)
			{
				_str[end] = _str[end - len];
				end--;
			}

			strncpy(_str + pos, str, len);
			/*for (int i = 0; i < len; i++)
			{
				_str[i + pos] = str[i];
			}*/

			_size += len;
			return *this;
		}

		string& erase(size_t pos, size_t len = npos)
		{
			assert(pos < _size);
			if (len == npos || pos + len >= _size)
			{
				_str[pos] = '\0';
				_size = pos;
			}
			else
			{
				/*int begin = pos;
				while (begin < pos + len)
				{
					_str[begin] = _str[begin + len];
					begin++;
				}
				_size -= len;*/
				strcpy(_str + pos, _str + pos + len);
				_size -= len;
			}
			
			return *this;
		}

		size_t find(char ch, size_t pos = 0) const
		{
			assert(pos < _size);
			while (pos < _size)
			{
				if (_str[pos] = ch)
				{
					return pos;
				}
				pos++;
			}
			return npos;
		}

		size_t find(const char* str, size_t pos = 0) const
		{
			const char* ptr = strstr(_str + pos, str);
			if (ptr == nullptr)
			{
				return npos;
			}
			else
			{
				return ptr - _str;
			}
		}
		
		void clear()
		{
			_size = 0;
			_str[0] = '\0';
		}

	private:
		char* _str;
		size_t _size;
		size_t _capacity;

		const static size_t npos = -1;
		//const static size_t N = 10;
		//// 只针对不可变的常量整形——SVIP
		//int a[N];
		//const static double x;
	};

	//1、operator<
	bool operator<(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) < 0;
	}
	//2、operator==
	bool operator==(const string& s1, const string& s2)
	{
		return strcmp(s1.c_str(), s2.c_str()) == 0;
	}
	//3、operator<=
	bool operator<=(const string& s1, const string& s2)
	{
		return s1 < s2 || s1 == s2;
	}
	//4、operator>
	bool operator>(const string& s1, const string& s2)
	{
		return !(s1 <= s2);
	}
	//5、operator>=
	bool operator>=(const string& s1, const string& s2)
	{
		return !(s1 < s2);
	}
	//6、operator!=
	bool operator!=(const string& s1, const string& s2)
	{
		return !(s1 == s2);
	}

	ostream& operator<<(ostream& out, const string& s)
	{
		for (size_t i = 0; i < s.size(); i++)
		{
			out << s[i];
		}
		return out;
	}

	/*istream& operator>>(istream& in, string& s)
	{
		char ch;
		in >> ch;
		while (ch != ' ' && ch != '\n')
		{
			s += ch;
			in >> ch;
		}
		return in;
	}*/

	//istream& operator>>(istream& in, string& s)
	//{
	//	//get() 相当于C语言的getchar
	//	char ch = in.get();
	//	while (ch != ' ' && ch != '\n')
	//	{
	//		s += ch;
	//		ch = in.get();
	//	}
	//	return in;
	//}

	istream& operator>>(istream& in, string& s)
	{
		s.clear();

		char buff[128] = { '\0' };
		size_t i = 0;
		char ch = in.get();
		while (ch != ' ' && ch != '\n')
		{
			if (i == 127)
			{
				// 满了
				s += buff;
				i = 0;
			}

			buff[i++] = ch;
			ch = in.get();
		}
		if (i > 0)
		{
			buff[i] = '\0';
			s += buff;
		}
		return in;
	}

	istream& getline(istream& in, string& s)
	{
		s.clear();
		char ch;
		ch = in.get();
		//getline函数只有在遇到换行符才会停止
		while (ch != '\n')
		{
			s += ch;
			ch = in.get();
		}
		return in;
	}

	// 浮点型只能在类外面初始化
	//const double string::x = 1.1;

	void test_string1()
	{
		string s1("hello world");
		cout << s1.c_str() << endl;

		for (size_t i = 0; i < s1.size(); ++i)
		{
			s1[i]++;
		}
		cout << s1.c_str() << endl;

		string::iterator it1 = s1.begin();
		while (it1 != s1.end())
		{
			(*it1)--;
			++it1;
		}
		cout << s1.c_str() << endl;

		for (auto ch : s1)
		{
			cout << ch << " ";
		}
		cout << endl;
	}

	void test_string2()
	{
		string s2("string");
		s2 += '!';
		s2 += '!';
		s2 += ' ';
		cout << s2.c_str() << endl;

		string s3;
		s3 += '*';
		cout << s3.c_str() << endl;

		string s4;
		s4 += "string NB";
		cout << s4.c_str() << endl;
	}

	void test_string3()
	{
		string s("string test");
		string s3(s);
		s3.insert(5, ' ');
		cout << s3.c_str() << endl;

		// 有问题
		string s4(s);
		s4.insert(0, 'x');
		cout << s4.c_str() << endl;

		string s5(s);
		s5.insert(6, "!!!");
		cout << s5.c_str() << endl;
		s5.insert(0, "");
		cout << s5.c_str() << endl;
	}

	void test_string4()
	{
		string s("abcdefghijk");
		string s4(s);
		s4.erase(0, 11);
		cout << s4.c_str() << endl;

		string s5(s);
		s5.erase(0, 6);
		cout << s5.c_str() << endl;
		s5.erase(0, 100);
		cout << s5.c_str() << endl;
	}

	void test_string5()
	{
		string s("hello world");
		string s5(s);
		s5.resize(5);
		cout << s5.size() << endl;
		cout << s5.capacity() << endl;
		cout << s5.c_str() << endl << endl;

		string s6(s);
		s6.resize(15, 'x');
		cout << s6.size() << endl;
		cout << s6.capacity() << endl;
		cout << s6.c_str() << endl << endl;

		string s7(s);
		s7.resize(20, 'x');
		cout << s7.size() << endl;
		cout << s7.capacity() << endl;
		cout << s7.c_str() << endl << endl;
	}

	void test_string6()
	{
		string s("hello world");
		cout << s << endl;
		cout << s.c_str() << endl;

		s.insert(5,'\0'); // 库里面 '\0' 转化为nullptr
		cout << s.size() << endl;
		cout << s.capacity() << endl;

		cout << s << endl;
		cout << s.c_str() << endl;
	}

	void test_string7()
	{
		////cin,scanf拿不到空格
		////cin >> s1 >> s2; 当输入多项值，这里用空格，换行区分
		//string s;
		//cin >> s; // 输入"hello world"
		//cout << s << ' ';
		//// world在缓冲区，需要再次取一次
		//
		//string s1;
		//cin >> s1;
		//cout << s1 << endl;

		string s;
		cin >> s;
		cout << s << endl;
	}

	void test_string8()
	{
		string s("hello world");
		string s2(s);
		cout << s2 << endl;

		string s3("*****************************************");
		s = s3;
		cout << s << endl;
		cout << s3 << endl;
	}
}

