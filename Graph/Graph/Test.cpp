#define _CRT_SECURE_NO_WARNINGS 1
using namespace std;
#include "Graph.h"
#include "BFS.h"
#include "DFS.h"

int main()
{
	//Matrix::TestGraph();
	//LinkTable::TestGraph();
	//BFS_Matrix::TestBFS();
	DFS_Matrix::TestDFS();

	return 0;
}