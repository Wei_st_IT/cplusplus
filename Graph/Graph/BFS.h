#pragma once
#include <climits>
#include <iostream>
#include <string>
#include <vector>
#include <queue>
#include <unordered_map>

//邻接矩阵
namespace BFS_Matrix { // 模版 V->vertex->边 W->weight->权值
	template<class V, class W, W MAX_W = INT_MAX, bool Direction = false>
	class Graph {
	public:
		//构造函数
		Graph(const V* vertexs, int n)
			:_vertexs(vertexs, vertexs + n) //设置顶点集合,vector类型的迭代器初始化
			, _matrix(n, vector<int>(n, MAX_W)) { //开辟二维数组空间，vector类型的多行初始化
			//建立顶点与下标的映射关系
			for (int i = 0; i < n; i++) {
				_vIndexMap[vertexs[i]] = i;
			}
		}
		//构造函数
		//Graph(const V* vertexs, size_t n)
		//{
		//	_vertexs.reserve(n);
		//	for (size_t i = 0; i < n; i++)
		//	{
		//		_vertexs.push_back(vertexs[i]);
		//		_vIndexMap[vertexs[i]] = i;
		//	}

		//	// MAX_W 作为不存在边的权值
		//	_matrix.resize(n);
		//	for (auto& e : _matrix)
		//	{
		//		e.resize(n, MAX_W);
		//	}
		//}
		//获取顶点对应的下标
		int getVertexIndex(const V& v) {
			auto it = _vIndexMap.find(v);
			if (it != _vIndexMap.end()) { //顶点存在
				return it->second;
			}
			else { //顶点不存在
				//assert(it);
				throw invalid_argument("不存在的顶点");
				return -1;
			}
		}
		//添加边
		void AddEdge(const V& src, const V& dst, const W& weight) {
			int srci = getVertexIndex(src);
			int dsti = getVertexIndex(dst); //获取源顶点和目标顶点的下标
			_matrix[srci][dsti] = weight; //设置邻接矩阵中对应的值
			if (Direction == false) { //无向图
				_matrix[dsti][srci] = weight; //添加从目标顶点到源顶点的边
			}
		}
		//打印顶点集合和邻接矩阵
		void Print() {
			int n = _vertexs.size();
			//打印顶点集合
			for (int i = 0; i < n; i++) {
				cout << "[" << i << "]->" << _vertexs[i] << endl;
			}
			cout << endl;

			//打印邻接矩阵
			//横下标
			cout << "  ";
			for (int i = 0; i < n; i++) {
				//cout << i << " ";
				printf("%4d", i);
			}
			cout << endl;
			for (size_t i = 0; i < _matrix.size(); i++) {
				cout << i << " "; //竖下标
				for (size_t j = 0; j < _matrix[i].size(); j++) {
					if (_matrix[i][j] == MAX_W) {
						printf("%4c", '*');
					}
					else {
						printf("%4d", _matrix[i][j]);
					}
				}
				cout << endl;
			}
			cout << endl;
		}

		//void bfs(const V& src)
		//{
		//	int srci = getVertexIndex(src); // 起始顶点的下标
		//	queue<int> q;
		//	int n = _vertexs.size();
		//	vector<bool> visited(n, false);
		//	q.push(srci); // 起始顶点入队列
		//	visited[srci] = true;

		//	while (!q.empty())
		//	{
		//		int front = q.front();
		//		q.pop();
		//		cout << _vertexs[front] << "->";
		//		for (int i = 0; i < n; i++)
		//		{
		//			if (_matrix[front][i] != MAX_W && visited[i] == false)
		//			{
		//				q.push(i);
		//				visited[i] = true;
		//			}
		//		}
		//	}
		//	cout << "结束";
		//}

		//优化bfs
		void bfs(const V& src)
		{
			size_t srci = getVertexIndex(src);

			// 队列和标记数组
			queue<int> q;
			vector<bool> visited(_vertexs.size(), false);

			q.push(srci);
			visited[srci] = true;
			int levelSize = 1;

			size_t n = _vertexs.size();
			while (!q.empty())
			{
				// 一层一层出
				for (int i = 0; i < levelSize; ++i)
				{
					int front = q.front();
					q.pop();
					cout << front << ":" << _vertexs[front] << " ";
					// 把front顶点的邻接顶点入队列
					for (size_t i = 0; i < n; ++i)
					{
						if (_matrix[front][i] != MAX_W)
						{
							if (visited[i] == false)
							{
								q.push(i);
								visited[i] = true;
							}
						}
					}
				}
				cout << endl;

				levelSize = q.size();
			}

			cout << endl;
		}

	private:
		vector<V> _vertexs;               //顶点集合
		unordered_map<V, int> _vIndexMap; //顶点映射下标
		vector<vector<W>> _matrix;        //邻接矩阵
	};

	void TestBFS()
	{
		string a[] = { "张三", "李四", "王五", "赵六", "周七" };
		Graph<string, int> g1(a, sizeof(a) / sizeof(string));
		g1.AddEdge("张三", "李四", 100);
		g1.AddEdge("张三", "王五", 200);
		g1.AddEdge("王五", "赵六", 30);
		g1.AddEdge("王五", "周七", 30);
		g1.Print();

		g1.bfs("张三");
	}

}
