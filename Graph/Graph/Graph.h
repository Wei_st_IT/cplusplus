#pragma once
#include <climits>
#include <iostream>
#include <string>
#include <vector>
#include <unordered_map>

//邻接矩阵
namespace Matrix { // 模版 V->vertex->边 W->weight->权值
	template<class V, class W, W MAX_W = INT_MAX, bool Direction = false>
	class Graph {
	public:
		//构造函数
		Graph(const V* vertexs, int n)
			:_vertexs(vertexs, vertexs + n) //设置顶点集合,vector类型的迭代器初始化
			, _matrix(n, vector<int>(n, MAX_W)) { //开辟二维数组空间，vector类型的多行初始化
			//建立顶点与下标的映射关系
			for (int i = 0; i < n; i++) {
				_vIndexMap[vertexs[i]] = i;
			}
		}
		//构造函数
		//Graph(const V* vertexs, size_t n)
		//{
		//	_vertexs.reserve(n);
		//	for (size_t i = 0; i < n; i++)
		//	{
		//		_vertexs.push_back(vertexs[i]);
		//		_vIndexMap[vertexs[i]] = i;
		//	}

		//	// MAX_W 作为不存在边的权值
		//	_matrix.resize(n);
		//	for (auto& e : _matrix)
		//	{
		//		e.resize(n, MAX_W);
		//	}
		//}
		//获取顶点对应的下标
		int getVertexIndex(const V& v) {
			auto it = _vIndexMap.find(v);
			if (it != _vIndexMap.end()) { //顶点存在
				return it->second;
			}
			else { //顶点不存在
				//assert(it);
				throw invalid_argument("不存在的顶点");
				return -1;
			}
		}
		//添加边
		void AddEdge(const V& src, const V& dst, const W& weight) {
			int srci = getVertexIndex(src);
			int dsti = getVertexIndex(dst); //获取源顶点和目标顶点的下标
			_matrix[srci][dsti] = weight; //设置邻接矩阵中对应的值
			if (Direction == false) { //无向图
				_matrix[dsti][srci] = weight; //添加从目标顶点到源顶点的边
			}
		}
		//打印顶点集合和邻接矩阵
		void Print() {
			int n = _vertexs.size();
			//打印顶点集合
			for (int i = 0; i < n; i++) {
				cout << "[" << i << "]->" << _vertexs[i] << endl;
			}
			cout << endl;

			//打印邻接矩阵
			//横下标
			cout << "  ";
			for (int i = 0; i < n; i++) {
				//cout << i << " ";
				printf("%4d", i);
			}
			cout << endl;
			for (int i = 0; i < n; i++) {
				cout << i << " "; //竖下标
				for (int j = 0; j < n; j++) {
					if (_matrix[i][j] == MAX_W) {
						printf("%4c", '*');
					}
					else {
						printf("%4d", _matrix[i][j]);
					}
				}
				cout << endl;
			}
			cout << endl;
		}
	private:
		vector<V> _vertexs;               //顶点集合
		unordered_map<V, int> _vIndexMap; //顶点映射下标
		vector<vector<W>> _matrix;        //邻接矩阵
	};

	void TestGraph()
	{
		Graph<char, int, INT_MAX, true> g("1234", 4);
		g.AddEdge('0', '1', 1);
		g.AddEdge('0', '3', 4);
		g.AddEdge('1', '3', 2);
		g.AddEdge('1', '2', 9);
		g.AddEdge('2', '3', 8);
		g.AddEdge('2', '1', 5);
		g.AddEdge('2', '0', 3);
		g.AddEdge('3', '2', 6);

		g.Print();
	}
}


//邻接表
namespace LinkTable {
	//链表结点定义
	template<class W>
	struct Edge {
		//int _srci;    //源顶点的下标（可选）
		int _dsti;      //目标顶点的下标
		W _weight;      //边的权值
		Edge<W>* _next; //连接指针

		Edge(int dsti, const W& weight)
			:_dsti(dsti)
			//, _srci(srci)
			, _weight(weight)
			, _next(nullptr)
		{}
	};

	template<class V, class W, bool Direction = false>
	class Graph {
		typedef Edge<W> Edge;
	public:
		//构造函数
		Graph(const V* vertexs, int n)
			:_vertexs(vertexs, vertexs + n) //设置顶点集合
			, _linkTable(n, nullptr) { //开辟邻接表的空间
			//建立顶点与下标的映射关系
			for (int i = 0; i < n; i++) {
				_vIndexMap[vertexs[i]] = i;
			}
		}
		//获取顶点对应的下标
		int getVertexIndex(const V& v) {
			auto it = _vIndexMap.find(v);
			if (it != _vIndexMap.end()) { //顶点存在
				return it->second;
			}
			else { //顶点不存在
				throw invalid_argument("不存在的顶点");
				return -1;
			}
		}
		//添加边  dst->destination->目的地  src->source->源文件
		void AddEdge(const V& src, const V& dst, const W& weight) {
			int srci = getVertexIndex(src);
			int dsti = getVertexIndex(dst); //获取源顶点和目标顶点的下标

			//添加从源顶点到目标顶点的边 --> 头插法
			Edge* edge = new Edge(dsti, weight);
			edge->_next = _linkTable[srci];
			_linkTable[srci] = edge;

			if (Direction == false) { //无向图
				//添加从目标顶点到源顶点的边
				Edge* edge = new Edge(srci, weight);
				edge->_next = _linkTable[dsti];
				_linkTable[dsti] = edge;
			}
		}
		//打印顶点集合和邻接表
		void Print() {
			int n = _vertexs.size();
			//打印顶点集合
			for (int i = 0; i < n; i++) {
				cout << "[" << i << "]->" << _vertexs[i] << " ";
			}
			cout << endl << endl;

			//打印邻接表
			for (int i = 0; i < n; i++) {
				Edge* cur = _linkTable[i];
				cout << "[" << i << ":" << _vertexs[i] << "]->";
				while (cur) {
					cout << "[" << cur->_dsti << ":" << _vertexs[cur->_dsti] <<
						":" << cur->_weight << "]->";
					cur = cur->_next;
				}
				cout << "nullptr" << endl;
			}
		}
	private:
		vector<V> _vertexs;               //顶点集合
		unordered_map<V, int> _vIndexMap; //顶点映射下标
		vector<Edge*> _linkTable;         //邻接表（出边表）
	};

	void TestGraph()
	{
		string a[] = { "张三", "李四", "王五", "赵六" };
		Graph<string, int> g1(a, 4);
		g1.AddEdge("张三", "李四", 100);
		g1.AddEdge("张三", "王五", 200);
		g1.AddEdge("王五", "赵六", 30);

		g1.Print();
	}
}

