#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Func1()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//int main()
//{
//	Base b;
//	return 0;
//}

//class Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Base::Func1()" << endl;
//	}
//	virtual void Func2()
//	{
//		cout << "Base::Func2()" << endl;
//	}
//	void Func3()
//	{
//		cout << "Base::Func3()" << endl;
//	}
//private:
//	int _b = 1;
//};
//
//
//class Derive : public Base
//{
//public:
//	virtual void Func1()
//	{
//		cout << "Derive::Func1()" << endl;
//	}
//private:
//	int _d = 2;
//};

//int main()
//{
//	Base b;
//	Derive d;
//	return 0;
//}

//int main()
//{
//	int a;
//	cout << "栈：" << &a << endl;
//
//	int* p = new int;
//	cout << "堆：" << &p << endl;
//
//	const char* str = "helle world";
//	cout << "代码段/常量区：" << (void*)str << endl;
//
//	static int s;
//	cout << "数据段/静态区：" << &s << endl;
//
//	Base b;
//	cout << "虚表：" << (void*)*((int*)&b) << endl;
//
//	Derive d;
//	cout << "虚表：" << (void*)*((int*)&d) << endl;
//	return 0;
//}


class Base {
public:
	virtual void func1() { cout << "Base::func1" << endl; }
	virtual void func2() { cout << "Base::func2" << endl; }
private:
	int a;
};
class Derive :public Base {
public:
	virtual void func1() { cout << "Derive::func1" << endl; }
	virtual void func3() { cout << "Derive::func3" << endl; }
	virtual void func4() { cout << "Derive::func4" << endl; }
private:
	int b;
};
//int main()
//{
//	Base b;
//	Derive d;
//}

//将返回值为void，参数为void的函数指针重命名为 VFPTR
typedef void(*VFPTR)();

void PrintVTable(VFPTR vTable[])
{
	// 依次取虚表中的虚函数指针打印并调用，调用可以看出虚表中存的是哪个函数
	cout << " 虚表地址>" << vTable << endl;
	for (int i = 0; vTable[i] != nullptr; ++i)
	{
		printf(" [%d]:%p,->", i, vTable[i]);
		vTable[i]();
	}
	cout << endl;
}

int main()
{
	Base b;
	// 把虚表地址传递给虚表打印函数
	PrintVTable((VFPTR*)*((int*)&b));
	Derive d;
	PrintVTable((VFPTR*)*((int*)&d));
	return 0;
}


//class Base1 {
//public:
//	virtual void func1() { cout << "Base1::func1" << endl; }
//	virtual void func2() { cout << "Base1::func2" << endl; }
//private:
//	int b1;
//};
//class Base2 {
//public:
//	virtual void func1() { cout << "Base2::func1" << endl; }
//	virtual void func2() { cout << "Base2::func2" << endl; }
//private:
//	int b2;
//};
//class Derive : public Base1, public Base2 {
//public:
//	virtual void func1() { cout << "Derive::func1" << endl; }
//	virtual void func3() { cout << "Derive::func3" << endl; }
//private:
//	int d1;
//};
//
//typedef void(*VFPTR) ();
//void PrintVTable(VFPTR vTable[])
//{
//	cout << " 虚表地址>" << vTable << endl;
//	for (int i = 0; vTable[i] != nullptr; ++i)
//	{
//		printf(" 第%d个虚函数地址 :0X%x,->", i, vTable[i]);
//		VFPTR f = vTable[i];
//		f();
//	}
//	cout << endl;
//}
//
//int main()
//{
//	Derive d;
//	PrintVTable((VFPTR*)(*(void**)&d));
//	// PrintVTable((VFPTR*)(*(int*)((char*)&d + sizeof(Base1))));
//	Base2* ptr2 = &d;
//	PrintVTable((VFPTR*)(*(void**)ptr2));
//	return 0;;
//}
