#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <list>

using namespace std;

void test()
{
	string s1("abcd");
	//遍历它

	for (size_t i = 0; i < s1.size(); ++i)
	{
		s1[i]++;
	}
	//s1[10];
	cout << s1 << endl;
	for (auto& ch : s1)
	{
		ch--;
	}
	cout << s1 << endl;

	// 反转一下
	size_t begin = 0, end = s1.size() - 1;
	while (begin < end)
	{
		swap(s1[begin++], s1[end--]);
	}
	cout << s1 << endl;
	// iterator 迭代器 行为上像指针一样的东西
	string::iterator it1 = s1.begin();
	while (it1 != s1.end())
	{
		*it1 += 1;
		++it1;
	}

	it1 = s1.begin();
	while (it1 != s1.end())
	{
		cout << *it1 << " ";
		++it1;
	}
	cout << endl;

	vector<int> v;
	vector<int>::iterator vit = v.begin();
	while (vit != v.end())
	{
		cout << *vit << endl;
		vit++;
	}
	cout << endl;

	list<int> lt;
	list<int>::iterator lit = lt.begin();
	while (lit != lt.begin())
	{
		cout << *lit << endl;
		lit++;
	}
	cout << endl;
}

void Print(const string& s)
{
	string::const_iterator it = s.begin();
	//const string::iterator it = s.begin(); error
	while (it != s.end())
	{
		cout << *it << endl;
		++it;
	}
	cout << endl;
}

void test2()
{
	string s1("1234");
	string::iterator it = s1.begin();
	while (it != s1.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	//string::reverse_iterator rit = s1.rbegin();
	auto rit = s1.rbegin();
	while (rit != s1.rend())
	{
		cout << *rit << " ";
		++rit;
	}
	cout << endl;
}

//正向 反向 -- 可以遍历读写容器数据
//const正向 const反向 -- 只能遍历，不能修改容器数据

//总结：1、只读功能函数 const版本
     // 2、只写功能函数 非const版本
     // 3、读写功能的函数 const + 非const版本

void test3()
{
	string s = "hello world";
	cout << s.size() << endl;
	cout << s.length() << endl;
	cout << s.capacity() << endl;
	cout << s.max_size() << endl;

	string ss;
	cout << s.max_size() << endl;//没啥用

	cout << s << endl;
	s.clear(); //内容清空
	cout << s << endl;
	cout << s.capacity() << endl; //容量没变
}


void TestPushBack()
{
	string s;
	// 提前用reserve开好空间，避免扩容，提高效率
	s.reserve(1000); // 结果会比1000大，因为要考虑对齐
	size_t sz = s.capacity();
	cout << "capacity changed: " << sz << '\n';
	cout << "making s grow:\n";
	for (int i = 0; i < 1000; ++i)
	{
		s.push_back('c');
		if (sz != s.capacity())
		{
			sz = s.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}
}



void test4()
{
	//删除数据
	string s1("stay up everyday");
	s1.resize(5);
	cout << s1.size() << endl;
	cout << s1.capacity() << endl;
	cout << s1 << endl << endl;

	//插入数据
	string s2("stay up everyday");
	s1.resize(20);
	cout << s2.size() << endl;
	cout << s2.capacity() << endl;
	cout << s2 << endl << endl;

	//扩容 + 插入数据
	string s3("stay up everyday");
	s3.resize(30, '*');
	cout << s3.size() << endl;
	cout << s3.capacity() << endl;
	cout << s3 << endl << endl;
}

void test5()
{
	string s1("happy life");
	string s2(s1);
	s2.push_back(' ');
	s2.push_back('t');
	s2.push_back('o');
	s2.push_back('*');
	s2.pop_back();
	cout << s1 << endl;
	cout << s2 << endl;
	s1.append(s2);
	cout << s1 << endl;
	s1 += '!';
	s1 += s2;
	cout << s1 << endl;
}



int main()
{
	//test();
	//test2();
	//test3();
	//TestPushBack();
	test4();
	//test5();
	return 0;
}

