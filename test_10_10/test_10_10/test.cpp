#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

//int& Count()
//{
//	int n = 10;
//	n++;
//	return n;
//}
//
//int main()
//{
//	int& ret = Count();
//	return 0;
//}

//int Count()
//{
//	int n = 10;
//	n++;
//	return n;
//}
//
//int main()
//{
//	int& ret = Count();
//	return 0;
//}

using namespace std;

//int& Count()
//{
//	int n = 10;
//	n++;
//	cout << "&n:" << &n << endl;
//	return n;
//}
//
//int main()
//{
//	int& ret = Count();
//	cout << ret << endl;
//	cout << "&ret:" << &ret << endl;
//	cout << ret << endl;
//	cout << "&ret:" << &ret << endl;
//	cout << ret << endl;
//	cout << "&ret:" << &ret << endl;
//	return 0;
//}

//int& Count()
//{
//	static int n = 0;
//	n++;
//	cout << "&n: " << &n << endl;
//	return n;
//}
//int main()
//{
//	int& ret = Count();
//	cout << ret << endl;
//	cout << "&ret: " << &ret << endl;
//	cout << ret << endl;
//	return 0;
//}

//int& Add(int a, int b)
//{
//	int c = a + b;
//	return c;
//}
//int main()
//{
//	int& ret = Add(1, 1);
//	Add(1, 2);
//	printf("-------------\n");
//	cout << ret << endl;  //2
//	cout << ret << endl;  //2
//	cout << ret << endl;  //2
//	return 0;
//}

#include <time.h>
//struct A { int a[100000]; };
//void TestFunc1(A a) {}
//void TestFunc2(A& a) {}
//void TestRefAndValue()
//{
//	A a;
//	// 以值作为函数参数
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1(a);
//	size_t end1 = clock();
//	// 以引用作为函数参数
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2(a);
//	size_t end2 = clock();
//	// 分别计算两个函数运行结束后的时间
//	cout << "TestFunc1(A)_time:" << end1 - begin1 << endl;
//	cout << "TestFunc2(A&)_time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestRefAndValue();
//}


//struct A { int a[10000]; };
//A a;
//// 值返回
//A TestFunc1() { return a; }
//// 引用返回
//A& TestFunc2() { return a; }
//void TestReturnByRefOrValue()
//{
//	// 以值作为函数的返回值类型
//	size_t begin1 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc1();
//	size_t end1 = clock();
//	// 以引用作为函数的返回值类型
//	size_t begin2 = clock();
//	for (size_t i = 0; i < 100000; ++i)
//		TestFunc2();
//	size_t end2 = clock();
//	// 计算两个函数运算完成之后的时间
//	cout << "TestFunc1_time:" << end1 - begin1 << endl;
//	cout << "TestFunc2_time:" << end2 - begin2 << endl;
//}
//int main()
//{
//	TestReturnByRefOrValue();
//}


//int main()
//{
//	int a = 0;
//	//语法角度而言：ra是a的别名，没有额外开空间
//	//底层的角度：它们是一样的方式实现的
//	int& ra = a;
//	ra = 1;
//	//语法角度而言：pa存储a的空间地址，pa开了4/8字节的空间
//	//底层的角度：它们是一样的方式实现的
//	int* pa = &a;
//	*pa = 1;
//	return 0;
//}

//#include<iostream>
//using namespace std;
//
////参数类型不同构成函数重载
//int Add(int left, int right)
//{
//	cout << "int Add(int left, int right)" << endl;
//	return left + right;
//}
//double Add(double left, double right)
//{
//	cout << "double Add(double left, double right)" << endl;
//	return left + right;
//}
//
////参数个数不同构成函数重载
//void f()
//{
//	cout << "f()" << endl;
//}
//void f(int a)
//
//{
//	cout << "f(int a)" << endl;
//}
//
////参数类型顺序不同构成函数重载
//void f(int a, char b)
//{
//	cout << "f(int a,char b)" << endl;
//}
//void f(char b, int a)
//{
//	cout << "f(char b, int a)" << endl;
//}
//
//int main()
//{
//	Add(10, 20);
//	Add(1.1, 2.2);
//	f();
//	f(10);
//	f(10, 'a');
//	f('a', 10);
//	return 0;
//}

//inline int Add(int x, int y)
//{
//	return x + y;
//}
//
//int main()
//{
//	int ret = Add(1, 2);
//	cout << ret << endl;
//
//	return 0;
//}

//把Add的内部逻辑复杂化
inline int Add(int x, int y)
{
	int ret = x + y;
	ret += x + y;
	ret = x + y;
	ret /= x + y;
	ret = x + y;
	ret = x + y;
	ret *= x + y;
	ret = x + y;
	ret = x + y;
	ret -= x + y;
	ret = x + y;
	ret += x + y;
	return ret;
}

int main()
{
	int ret = Add(1, 3);
	cout << ret << endl;
	return 0;
}