#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <set>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <string>

using namespace std;

#include "HashTable.h"
#include "HashBucket.h"

void test_unordered_set()
{
	unordered_set<int> us;
	us.insert(3);
	us.insert(1);
	us.insert(3);
	us.insert(2);
	us.insert(1);
	us.insert(0);

	unordered_set<int>::iterator it = us.begin();
	while (it != us.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;
}

//
//void test_unordered_map()
//{
//	unordered_map<string, int> um;
//	unordered_map<string, int>::iterator it = um.begin();
//	um.insert("10", 1);
//	um.insert("2", 2);
//	um.insert("3", 3);
//	um.insert("4", 4);
//	um.insert("5", 5);
//	um.insert("6", 6);
//
//	/*for (const auto& e : um)
//	{
//		cout << e.first << ":" << e.second << " ";
//	}*/
//
//	while (it != um.end())
//	{
//		cout << it->first << ":" << it->second << " ";
//		it++;
//	}
//	cout << endl;
//}
//

void test_unordered_map2()
{
	string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果",
		"苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	unordered_map<string, int> countMap;
	for (auto& e : arr)
	{
		//map<string, int>::iterator it = countMap
		/*auto it = countMap.find(e);
		if (it == countMap.end())
		{
			countMap.insert(make_pair(e, 1));
		}
		else
		{
			it->second++;
		}*/

		countMap[e]++;
	}
		
	for (const auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}

}


void test_speed()
{
	const size_t N = 10000;

	unordered_set<int> us;
	set<int> s;

	vector<int> v;
	v.reserve(N);
	srand((unsigned int)time(0));
	for (size_t i = 0; i < N; ++i)
	{
		//v.push_back(rand());
		//v.push_back(rand()+i);
		v.push_back(i);
	}

	size_t begin1 = clock();
	for (auto e : v)
	{
		s.insert(e);
	}
	size_t end1 = clock();
	cout << "set insert:" << end1 - begin1 << endl;

	size_t begin2 = clock();
	for (auto e : v)
	{
		us.insert(e);
	}
	size_t end2 = clock();
	cout << "unordered_set insert:" << end2 - begin2 << endl;


	size_t begin3 = clock();
	for (auto e : v)
	{
		s.find(e);
	}
	size_t end3 = clock();
	cout << "set find:" << end3 - begin3 << endl;

	size_t begin4 = clock();
	for (auto e : v)
	{
		us.find(e);
	}
	size_t end4 = clock();
	cout << "unordered_set find:" << end4 - begin4 << endl;

	cout << s.size() << endl;
	cout << us.size() << endl;

	size_t begin5 = clock();
	for (auto e : v)
	{
		s.erase(e);
	}
	size_t end5 = clock();
	cout << "set erase:" << end5 - begin5 << endl;

	size_t begin6 = clock();
	for (auto e : v)
	{
		us.erase(e);
	}
	size_t end6 = clock();
	cout << "unordered_set erase:" << end6 - begin6 << endl;

}

void test_unordered_set2()
{
	unordered_set<int> us;
	//插入元素（去重）
	us.insert(1);
	us.insert(12);
	us.insert(6);
	us.insert(23);
	us.insert(3);
	us.insert(6);
	us.insert(0);
	//遍历容器方式一（范围for）
	for (auto e : us)
	{
		cout << e << " "; // 1 12 6 23 3 0
	}
	cout << endl; 
	//删除元素方式一
	us.erase(3);
	//删除元素方式二
	unordered_set<int>::iterator pos = us.find(1); //查找值为1的元素
	if (pos != us.end())
	{
		us.erase(pos);
	}
	//遍历容器方式二（迭代器遍历）
	unordered_set<int>::iterator it = us.begin();
	while (it != us.end())
	{
		cout << *it << " "; // 12 6 23 0
		it++;
	}
	cout << endl; 
	//容器中值为2的元素个数
	cout << us.count(2) << endl; //0
	//容器大小
	cout << us.size() << endl; //4
	//清空容器
	us.clear();
	//容器判空
	cout << us.empty() << endl; //1
	//交换两个容器的数据
	unordered_set<int> tmp{1, 2, 3, 4,};
	us.swap(tmp);
	for (auto e : us)
	{
		cout << e << " ";//1 2 3 4
	}
	cout << endl; 
}

void test_unordered_map3()
{
	unordered_map<string, string> mp;
	/*insert插入*/
	//1：借助pair构造函数
	pair<string, string> kv("string", "字符串");
	mp.insert(kv);
	//2：借助pair构造匿名对象插入
	mp.insert(pair<string, string>("blue", "蓝色"));
	//3：调用make_pair函数模板插入
	mp.insert(make_pair("sky", "天空"));
	//4：使用[]运算符重载函数进行插入
	mp["快乐"] = "happy";
	//5：使用{}
	mp.insert({ "左边", "left"});
	/*遍历*/
	//1：迭代器遍历
	unordered_map<string, string>::iterator it = mp.begin();
	while (it != mp.end())
	{
		cout << it->first << ":" << it->second << " ";
		it++;
	}
	cout << endl; // string:字符串 blue:蓝色 sky:天空 happy:快乐 left:左边
	//2：范围for
	for (auto e : mp)
	{
		cout << e.first << ":" << e.second << " ";
	}	
	cout << endl; // string:字符串 blue:蓝色 sky:天空 happy:快乐 left:左边
	/*删除*/
	//1：根据key删除
	mp.erase("string");
	//2：根据迭代器位置删除
	unordered_map<string, string>::iterator pos = mp.find("sky");
	if (pos != mp.end())
	{
		mp.erase(pos);
	}
	for (auto e : mp)
	{
		cout << e.first << ":" << e.second << " ";
	}
	cout << endl; // blue:蓝色 happy:快乐 左边:left
	/*修改*/
	//1：通过迭代器位置修改
	pos = mp.find("快乐");
	if (pos != mp.end())
	{
		pos->second = "ikun";
	}
	//2：通过[]修改
	mp["左边"] = "小黑子";
	for (auto e : mp)
	{
		cout << e.first << ":" << e.second << " ";
	}
	cout << endl; // blue:蓝色 快乐:ikun 左边:小黑子
	/*交换*/
	unordered_map<string, string> tmp{ { "2023", "年"}, {"4", "月"}, {"3", "日"}};
	mp.swap(tmp);
	for (auto e : mp)
	{
		cout << e.first << e.second << " ";
	}
	cout << endl; //2023年4月3日
}

void test()
{
	unordered_multiset<int> us;
	//插入元素（去重）
	us.insert(1);
	us.insert(12);
	us.insert(6);
	us.insert(12);
	us.insert(1);
	us.insert(6);
	us.insert(0);
	//遍历容器方式一（范围for）
	for (auto e : us)
	{
		cout << e << " "; // 1 12 6 0
	}
	cout << endl;
}

void test2()
{
	unordered_multimap<string, string> up;
	//插入元素（去重）
	up.insert(make_pair("ikun", "小黑子"));
	up.insert(make_pair("哎哟", "你干嘛"));
	up.insert(make_pair("鸡你太美", "坤坤"));
	up.insert(make_pair("唱、跳、rap", "篮球"));
	up.insert(make_pair("ikun", "小黑子"));
	up.insert(make_pair("鸡你太美", "坤坤"));

	//遍历容器方式一（范围for）
	for (auto e : up)
	{
		cout << e.first << ":" << e.second << "\n";
	}
	cout << endl;
}

int main()
{
	//test_unordered_set();
	//test_unordered_set2();
	//test_unordered_map();
	//test_unordered_map2();
	//test_unordered_map3();
	//test();
	test2();
	//test_speed();
	//HashTable_realize::TestHT1();
	//HashTable_realize::TestHT2();
	//HashBucket_realize::TestHB1();
	//HashBucket_realize::TestHB2();
	return 0;
}