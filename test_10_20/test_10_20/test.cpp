#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		cout << "A 构造" << endl;
//	}
//
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		cout << "A 拷贝构造" << endl;
//	}
//
//	A& operator=(const A& aa)
//	{
//		cout << "A 赋值重载" << endl;
//		if (this != &aa)
//		{
//			_a = aa._a;
//		}
//		return *this;
//	}
//
//	~A()
//	{
//		cout << "A 析构" << endl;
//	}
//private:
//	int _a;
//};
//
////传值传参
//void f1(A aa) {}
//
////传值返回
//A f2()
//{
//	A aa;
//	return aa;
//}
//
//int main()
//{
//	f1(A(2));
//	return 0;
//}

//class A
//{
//public:
//    A(int a)//非默认构造函数，需要传参
//        :_aa(a)
//    {}
//private:
//    int _aa;
//};
//class B
//{
//public:
//    B(int a, int ref)
//        :_a(a)
//        , _ref(ref)
//        , _n(10)
//    {}
//private:
//    A _a; // 没有默认构造函数
//    int& _ref; // 引用
//    const int _n; // const
//};

//class A
//{
//public:
//    A(int a)//非默认构造函数，需要传参
//        :_aa(a)
//    {}
//private:
//    int _aa;
//};

//class B
//{
//public:
//    B(int a, int ref, int n)
//    {
//        _a = a;
//        _ref = ref;
//        _n = n;
//    }
//    
//private:
//    int _a; //声明，定义不强求初始化
//    //只能在定义时初始化
//    int& _ref; // 引用
//    const int _n; // const
//};


//class A
//{
//public:
//	A(int n = 10)
//		:_n(n)
//	{}
//private:
//	int _n;
//};
//
//class B
//{
//public:
//	B(int a, int b)
//		//:_a(a)
//		:_b(b)
//	{
//		A aa(100);
//		_a = aa;
//	}
//private:
//	A _a;
//	int _b;
//};
//
//int main()
//{
//	B b(1, 2);
//	return 0;
//}
class Date
{
public:
	Date(int year, int month, int day)
		:_year(year)
		//, _month(month)
		, _day(day)
	{}
private:
	int _year;
	int _month = 10;
	int _day;
};

int main()
{
	Date d(2022, 1, 20);
	return 0;
}

