#pragma once
#include <vector>

template<class K>
struct HashFunc
{
	size_t operator()(const K& key)
	{
		return (size_t)key;
	}
};

template<>
struct HashFunc<string>
{
	// BKDR 算法
	size_t operator()(const string& key)
	{
		size_t hash = 0;
		for (auto ch : key)
		{
			hash *= 131;
			hash += ch;
		}
		return hash;
	}
};


namespace HashBucket_realize
{
	template<class T>
	struct HashNode
	{
		HashNode<T>* _next;
		T _data;

		HashNode(const T& data)
			:_next(nullptr)
			, _data(data)
		{}
	};

	// 前置声明
	template<class K, class T, class Hash, class KeyOfT>
	class HashBucket;

	template<class K, class T, class Hash, class KeyOfT>
	struct __HTIterator
	{
		typedef HashNode<T> Node;
		typedef __HTIterator<K, T, Hash, KeyOfT> Self;

		typedef HashBucket<K, T, Hash, KeyOfT> HB;

		Node* _node;
		HB* _hb;

		__HTIterator(Node* node, HB* hb)
			:_node(node)
			, _hb(hb)
		{}

		T& operator*()
		{
			return _node->_data;
		}

		T* operator->()
		{
			return &_node->_data;
		}

		bool operator==(const Self& s) const
		{
			return _node == s._node;
		}

		bool operator!=(const Self& s) const
		{
			return _node != s._node;
		}

		Self& operator++()
		{
			if (_node->_next)
			{
				_node = _node->_next;
			}
			else // 寻找下一个桶
			{
				KeyOfT kot;
				Hash hash;
				size_t hashi = hash(kot(_node->_data)) % _hb->_tables.size();
				++hashi;
				while (hashi < _hb->_tables.size())
				{
					if (_hb->_tables[hashi])
					{
						_node = _hb->_tables[hashi];
						break;
					}
					else
					{
						hashi++;
					}
				}
				// 后面没有桶了
				if (hashi == _hb->_tables.size())
				{
					_node = nullptr;
				}
			}

			return *this;
		}
	};

	template<class K, class T, class Hash, class KeyOfT>
	class HashBucket
	{
		typedef HashNode<T> Node;

		template<class K, class T, class Hash, class KeyOfT>
		//template<class K, class T, class Ref, class Ptr, class Hash, class KeyOfT>
		friend struct __HTIterator;
	public:
		typedef __HTIterator<K, T, Hash, KeyOfT> iterator;
		//typedef __HTIterator<K, T, T&, T*, Hash, KeyOfT> iterator;
		//typedef __HTIterator<K, T, const T&, const T*, Hash, KeyOfT> const_iterator;

		iterator begin()
		{
			for (size_t i = 0; i < _tables.size(); i++)
			{
				if (_tables[i])
				{
					return iterator(_tables[i], this);
				}
			}

			return iterator(nullptr, this);
		}

		iterator end()
		{
			return iterator(nullptr, this);
		}

		/*const_iterator begin() const
		{
			for (size_t i = 0; i < _tables.size(); ++i)
			{
				if (_tables[i])
				{
					return const_iterator(_tables[i], this);
				}
			}

			return const_iterator(nullptr, this);
		}

		const_iterator end() const
		{
			return const_iterator(nullptr, this);
		}*/

		// 构造函数
		HashBucket()
			:_n(0)
		{
			//_tables.resize(10);
			_tables.resize(__stl_next_prime(0));
		}


		//析构函数
		~HashBucket()
		{
			//将哈希表当中的结点一个个释放
			for (size_t i = 0; i < _tables.size(); i++)
			{
				Node* cur = _tables[i];
				while (cur) //将该桶的结点取完为止
				{
					Node* next = cur->_next; //记录下一个结点
					delete cur; //释放结点
					cur = next;
				}
				_tables[i] = nullptr; //将该哈希桶置空
			}
		}


		//拷贝构造函数
		HashBucket(const HashBucket& hb)
		{
			//1、将哈希表的大小调整为ht._table的大小
			_tables.resize(hb._table.size());
			//2、将hb._tables每个桶当中的结点一个个拷贝到自己的哈希表中（深拷贝）
			for (size_t i = 0; i < hb._tables.size(); i++)
			{
				if (hb._tables[i]) //桶不为空
				{
					Node* cur = hb._tables[i];
					while (cur) //将该桶的结点取完为止
					{
						Node* copy = new Node(cur->_data); //创建拷贝结点
						//将拷贝结点头插到当前桶
						copy->_next = _tables[i];
						_tables[i] = copy;
						cur = cur->_next; //取下一个待拷贝结点
					}
				}
			}
			//3、更改哈希表当中的有效数据个数
			_n = hb._n;
		}

		//赋值运算符重载函数
		HashBucket& operator=(HashBucket hb)
		{
			//交换哈希表中两个成员变量的数据
			_tables.swap(hb._table);
			swap(_n, hb._n);

			return *this; //支持连续赋值
		}


		pair<iterator, bool> Insert(const T& data)
		{
			KeyOfT kot;

			iterator it = Find(kot(data));
			if (it != end())
			{
				return make_pair(it, false);
			}

			if (_n == _tables.size())
			{
				/*HashBucket<K, V, Hash> newHB;
				newHB._tables.resize(_tables.size() * 2);
				for (auto cur : _tables)
				{
					while (cur)
					{
						newHB.Insert(cur->_kv);
						cur = cur->_next;
					}
				}
				_tables.swap(newHB._tables);*/

				vector<Node*> newTables;
				//newTables.resize(2 * _tables.size(), nullptr);
				newTables.resize(__stl_next_prime(_tables.size()), nullptr);
				for (size_t i = 0; i < _tables.size(); i++)
				{
					Node* cur = _tables[i];
					while (cur)
					{
						Node* next = cur->_next; // 保存下一节点的地址
						size_t hashi = Hash()(kot(cur->_data)) % newTables.size();
						// 头插到新表
						cur->_next = newTables[hashi]; // 这句没写
						newTables[hashi] = cur;
						cur = next;
					}
					_tables[i] = nullptr; // 
				}
				_tables.swap(newTables);
			}
			size_t hashi = Hash()(kot(data)) % _tables.size();
			Node* newNode = new Node(data); // 需要写构造函数，不然无法构造newNode
			newNode->_next = _tables[hashi];
			_tables[hashi] = newNode;
			++_n;

			return make_pair(iterator(newNode, this), true);
		}

		iterator Find(const K& key)
		{
			KeyOfT kot;
			size_t hashi = Hash()(key) % _tables.size();
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					return iterator(cur, this);
				}
				else
				{
					cur = cur->_next;
				}
			}

			return end();
		}

		bool Erase(const K& key)
		{
			KeyOfT kot;
			size_t hashi = Hash()(key) % _tables.size();
			Node* prev = nullptr;
			Node* cur = _tables[hashi];
			while (cur)
			{
				if (kot(cur->_data) == key)
				{
					if (cur == _tables[hashi]) // 特殊情况:待删节点就是_tables[hashi]
					{
						_tables[hashi] = cur->_next;
					}
					else
					{
						prev->_next = cur->_next;
					}
					delete cur;
					--_n;
					return true;
				}
				else
				{
					prev = cur;
					cur = cur->_next;
				}
			}
			return false;
		}

		inline unsigned long __stl_next_prime(unsigned long n)
		{
			static const int __stl_num_primes = 28;
			static const unsigned long __stl_prime_list[__stl_num_primes] =
			{
				53, 97, 193, 389, 769,
				1543, 3079, 6151, 12289, 24593,
				49157, 98317, 196613, 393241, 786433,
				1572869, 3145739, 6291469, 12582917, 25165843,
				50331653, 100663319, 201326611, 402653189, 805306457,
				1610612741, 3221225473, 4294967291
			};

			for (int i = 0; i < __stl_num_primes; ++i)
			{
				if (__stl_prime_list[i] > n)
				{
					return __stl_prime_list[i];
				}
			}

			return __stl_prime_list[__stl_num_primes - 1];
		}

	private:
		vector<Node*> _tables; // 指针数组
		size_t _n;
	};

}

