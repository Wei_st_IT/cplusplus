#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;
#include "UnorderedMap.h"
#include "UnorderedSet.h"

int main()
{
	unordered_set_realize::test_unordered_set();
	unordered_map_realize::test_unordered_map();
	return 0;
}