#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <array>
#include <string>

using namespace std;

namespace static_array
{
	// 定义一个模板类型参数的静态数组
	template <class T, size_t N = 10> // N:非类型模板参数
	class array
	{
	public:
		T& operator[](size_t index)
		{
			return _array[index];
		}

		const T& operator[](size_t index) const
		{
			return _array[index];
		}

		const size_t size() const 
		{
			return _size;
		}

		bool empty() const
		{
			return 0 == _size;
		}
	private:
		T _array[N]; // 利用非类型模板参数指定静态数组的大小
		size_t _size;
	};
}


//int main()
//{
//	Array<int, 10> a1;        // 10
//	Array<double, 1000> a2;     // 1000
//
//	return 0;
//}

//int main()
//{
//	int a1[10];
//	array<int, 10> a2;
//	array<int, 100> a3;
//
//	// 对于越界的检查
//	// 越界读不检查
//	// 越界写  抽查
//	cout << a1[10] << endl;
//	cout << a1[11] << endl;
//
//	//a1[10] = 0;
//	a1[15] = 0;
//
//	// C++11  array
//	cout << a2[10] << endl;
//	cout << a2[11] << endl;
//
//	//a2[10] = 0;
//	//a2[15] = 0;
//
//	return 0;
//}

//template <class T, size_t N = 10> // N:非类型模板参数
//class _array
//{
//public:
//	int fun()
//	{
//		return N = 0;
//	}
//private:
//	T _array[N]; // 利用非类型模板参数指定静态数组的大小
//	size_t _size;
//};
//
//int main()
//{
//	_array<int, 100> a1;
//	cout << a1.fun() << endl;
//	return 0;
//}


class Date
{
public:
	Date(int year = 1900, int month = 1, int day = 1)
		: _year(year)
		, _month(month)
		, _day(day)
	{}

	bool operator<(const Date& d)const
	{
		return (_year < d._year) ||
			(_year == d._year && _month < d._month) ||
			(_year == d._year && _month == d._month && _day < d._day);
	}

	bool operator>(const Date& d)const
	{
		return (_year > d._year) ||
			(_year == d._year && _month > d._month) ||
			(_year == d._year && _month == d._month && _day > d._day);
	}

	friend ostream& operator<<(ostream& _cout, const Date& d)
	{
		_cout << d._year << "-" << d._month << "-" << d._day;
		return _cout;
	}

private:
	int _year;
	int _month;
	int _day;
};

// 函数模板——参数匹配
template <class T>
bool Less(T left, T right)
{
	return left < right;
}

int main()
{
	cout << Less(1, 3) << endl; // 可以比较，结果正确

	Date d1(2023, 1, 1);
	Date d2(2024, 1, 1);
	cout << Less(d1, d2) << endl; // 可以比较，结果正确

	/*Date* p1 = new Date(2023, 1, 1);
	Date* p2 = new Date(2025, 1, 1);*/
	Date* p1 = &d1;
	Date* p2 = &d2;
	cout << Less(p1, p2) << endl; // 可以比较，结果错误

	return 0;
}


// 函数模板 -- 参数匹配
template<class T>
bool Less(T left, T right)
{
	return left < right;
}

// 对Less函数模板进行特化
template<>
bool Less<Date*>(Date* left, Date* right)
{
		return *left < *right;
}
int main()
{
	cout << Less(1, 2) << endl;
	Date d1(2022, 7, 7);
	Date d2(2022, 7, 8);
	cout << Less(d1, d2) << endl;
	Date* p1 = &d1;
	Date* p2 = &d2;
	cout << Less(p1, p2) << endl; // 调用特化之后的版本，而不走模板生成了
	return 0;
}

bool Less(Date* left, Date* right)
{
	return *left < *right;
}


template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
private:
	T1 _d1;
	T2 _d2;
};

// 全特化
template<>
class Data<int, char>
{
public:
	Data() { cout << "Data<int, char>" << endl; }
private:
	int _d1;
	char _d2;
};
int main()
{
	Data<int, int> d1; // Data<T1, T2>
	Data<int, char> d2; // Data<int, char>
	Data<int, double> d3; // Data<T1, T2>

	return 0;
}

template<class T1, class T2>
class Data
{
public:
	Data() { cout << "Data<T1, T2>" << endl; }
private:
	T1 _d1;
	T2 _d2;
};

// 全特化：
template<>
class Data<int, double>
{
public:
	Data()
	{
		cout << "Data<int, double>" << endl;
	}
};

// 半特化：
template<class T1>
class Data<T1, char> // 将第二个参数特化为char
{
public:
	Data()
	{
		cout << "Data<T1, char" << endl;
	}
private:
	T1 _d1;
	int _d2;
};

template <class T1, class T2>
class Data <T1*, T2*> // 如果T1, T2是指针类型，就走此类
{
public:
	Data()
	{
		cout << "Data<T1*, T2*>" << endl;
	}
};

int main()
{
	Data<int, int> d1; 
	Data<int, double> d2;
	Data<char, double> d3;
	Data<char, char> d4;
	Data<int*, int*> d5;
	Data<int*, string*> d6;
	Data<int*, int> d7; 
	return 0;
}