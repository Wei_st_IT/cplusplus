#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include "Exception.h"
#include "Msg.h"

//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int len, time;
//	cin >> len >> time;
//	cout << Division(len, time) << endl;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	/*catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}*/
//	catch (int errid)
//	{
//		cout << errid << endl;
//	}
//	return 0;
//}


//double Division(int a, int b)
//{
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else
//		return ((double)a / (double)b);
//}
//void Func()
//{
//	int* p = new int[10];
//	int len, time;
//	cin >> len >> time;
//	cout << Division(len, time) << endl;
//	cout << "delete[]->" << p << endl;
//	delete[] p;
//}
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (const char* errmsg)
//	{
//		cout << errmsg << endl;
//	}
//	return 0;
//}


//double Division(int a, int b) {
//	// 当b == 0时抛出异常
//	if (b == 0)
//		throw "Division by zero condition!";
//	else {
//		return ((double)a / (double)b);
//	}
//}
//
//void Func() {
//	// 这里如果发生除0错误会抛出异常，但下面的资源没有得到释放
//	// 所以这里捕获异常后并不处理异常，而只释放资源，异常还是交给外面处理，这里捕获了再重新抛出去。
//	int* p = new int[10];
//	try {
//		int len, time;
//		cin >> len >> time;
//		cout << Division(len, time) << endl;
//	}
//	catch (const char* errmsg) {
//		cout << "delete[] ->" << p << endl;
//		delete[] p;
//		throw errmsg;
//	}
//
//	//如果上面没有抛异常，这里再正常释放
//	delete[] p;
//}
//
//int main() 
//{
//	try {
//		Func();
//	}
//	catch (const char* errmsg) {
//		cout << errmsg << endl;
//	}
//
//	return 0;
//}


//
//double Division(int a, int b) {
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		string s("Division by zero condition!");
//		throw s;
//	}
//	else {
//		return ((double)a / (double)b);
//	}
//}
//
//void Func() {
//	// 这里如果发生除0错误会抛出异常，但下面的资源没有得到释放
//	// 所以这里捕获异常后并不处理异常，而只释放资源，异常还是交给外面处理，这里捕获了再重新抛出去。
//	int* p = new int[10];
//	try {
//		int len, time;
//		cin >> len >> time;
//		cout << Division(len, time) << endl;
//	}
//	catch (...) {  //捕获任意类型异常
//		cout << "delete[] ->" << p << endl;
//		delete[] p;
//		throw;  //捕获到什么，就抛出什么
//	}
//
//	//如果上面没有抛异常，这里再正常释放
//	delete[] p;
//}
//
//int main() {
//	try {
//		Func();
//	}
//	catch (const char* errmsg) {
//		cout << errmsg << endl;
//	}
//	catch (int errid) {
//		cout << errid << endl;
//	}
//	catch (...) {
//		cout << "未知异常" << endl;
//	}
//	return 0;
//}

//double Division(int a, int b) noexcept{
//	// 当b == 0时抛出异常
//	if (b == 0)
//	{
//		string s("Division by zero condition!");
//		throw s;
//	}
//	else {
//		return ((double)a / (double)b);
//	}
//}
//int main() {
//	try {
//		int a = 0, b = 0;
//		cin >> a >> b;
//		cout << Division(a, b) << endl;
//	}
//	catch (const char* errmsg) {
//		cout << errmsg << endl;
//	}
//	catch (...) {
//		cout << "未知异常" << endl;
//	}
//	return 0;
//}


//int main() {
//	try {
//		vector<int> v(10, 5);
//		// 这里如果系统内存不够也会抛异常
//		//v.reserve(1000000000);
//
//		// 这里越界会抛异常
//		v.at(10) = 100;
//	}
//	catch (const exception& e) // 这里捕获父类对象就可以--多态
//	{
//		cout << e.what() << endl;
//	}
//	catch (...) {
//		cout << "Unkown Exception" << endl;
//	}
//	return 0;
//}



int main()
{
	//cpp::test_Exception();
	cpp::test_Msg();
	return 0;
}