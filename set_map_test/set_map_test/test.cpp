#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <set>
#include <map>

using namespace std;

void set_test1()
{
	// set底层是一个二叉搜索树
	set<int> s;
	s.insert(1);
	s.insert(7);
	s.insert(1);
	s.insert(3);
	s.insert(6);
	s.insert(9);
	// 排序+去重
	//set<int>::iterator it = s.begin();
	auto it = s.begin();
	while (it != s.end())
	{
		cout << *it << " ";
		++it;
	}
	cout << endl;

	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;
}

void set_test2()
{
	set<int> s;
	s.insert(1);
	s.insert(17);
	s.insert(12);
	s.insert(23);
	s.insert(16);
	s.insert(39);

	auto pos = s.find(12); // 默认找中序第一个12
	// auto pos = find(s.begin(), s.end(), 12);
	if (pos != s.end())
	{
		s.erase(pos);
	}
	//cout << s.erase(1) << endl;
	cout << s.erase(23) << endl;

	for (auto e : s)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << s.count(1) << endl;
}

//int main()
//{
//	set_test1();
//	set_test2();
//	return 0;
//}

//int main()
//{
//	std::set<int> myset;
//	std::set<int>::iterator itlow, itup;
//
//	for (int i = 1; i < 10; i++) 
//		myset.insert(i * 10); // 10 20 30 40 50 60 70 80 90
//
//	// 迭代器特点：左闭右开
//	itlow = myset.lower_bound(35);    // >= 35 返回大于等于35的边界         
//	itup = myset.upper_bound(75);     // > 70 返回大于70的边界             
//	// 配合使用删除 35-75 的边界
//	myset.erase(itlow, itup);                     // 10 20 30 80 90
//
//	std::cout << "myset contains:";
//	for (std::set<int>::iterator it = myset.begin(); it != myset.end(); ++it)
//		std::cout << ' ' << *it;
//	std::cout << '\n';
//
//	return 0;
//}

void map_test1()
{
	map<string, string> dict;
	dict.insert(pair<string, string>("排序", "sort"));
	dict.insert(pair<string, string>("左边", "left"));	
	dict.insert(pair<string, string>("右边", "right"));
	dict.insert(make_pair("字符串", "string")); // 自动识别类型
	dict["迭代器"] = "iterator"; // 插入+修改
	dict["insert"]; // 插入
	dict["insert"] = "插入"; // 修改
	cout << dict["左边"] << endl; // 查找 打印 left
	// 查找成功，返回键值, 查找失败，插入查找值

	dict.insert(make_pair<string, string>("排序", "sort")); // 插入失败，搜索树只比较K
	map<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << (*it).first << ":" << it->second << endl;
		it++;
	}
	cout << endl;

	for (const auto& kv : dict)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;
}

void map_test2()
{
	// 统计水果出现的次数
	string arr[] = { "苹果", "西瓜", "香蕉", "草莓", "苹果", "西瓜", "苹果", "苹果", "西瓜", "苹果", "香蕉", "苹果", "香蕉" };
	map<string, int> countMap;
	//for (auto& e : arr)
	//{
	//	//map<string, int>::iterator it = countMap.find(e);
	//	auto it = countMap.find(e);
	//	if (it == countMap.end())
	//	{
	//		countMap.insert(make_pair(e, 1));
	//	}
	//	else
	//	{
	//		it->second++;
	//	}
	//}

	for (auto& e : arr)
	{
		countMap[e]++;
	}

	for (const auto& kv : countMap)
	{
		cout << kv.first << ":" << kv.second << endl;
	}
	cout << endl;

}

void multimap_test1()
{
	multimap<string, string> dict;
	dict.insert(make_pair("apple", "苹果"));
	dict.insert(make_pair("banana", "香蕉"));
	dict.insert(make_pair("watermelon", "西瓜"));
	dict.insert(make_pair("strawberry", "草莓"));
	multimap<string, string>::iterator it = dict.begin();
	while (it != dict.end())
	{
		cout << it->first << ":" << it->second << endl;
		it++;
	}
}

int main()
{
	map_test1();
	map_test2();
	multimap_test1();
	return 0;
}