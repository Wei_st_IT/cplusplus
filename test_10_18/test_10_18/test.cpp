#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include "Date.h"
using namespace std;


//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail\n");
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//
//		cout << "Stack 构造" << endl;
//	}
//
//	~Stack()
//	{
//		free(_a);
//		_a = NULL;
//		_top = _capacity = 0;
//	}
//
//	Stack(const Stack& st)  //拷贝构造
//	{
//		_a = (int*)malloc(sizeof(int) * st._capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail\n");
//			exit(-1);
//		}
//		memcpy(_a, st._a, sizeof(int) * st._capacity);
//		_top = st._top;
//		_capacity = st._capacity;
//	}
//
//	void Push(int x)
//	{
//		_a[_top++] = x;
//	}
//
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};
//
//class MyQueue
//{
//public:
//	void Push(int x)
//	{
//		_pushST.Push(x);
//	}
//
//	Stack _pushST;
//	Stack _popST;
//};

//int main()
//{
//	MyQueue mq1;
//	mq1.Push(1);
//	mq1.Push(2);
//
//	MyQueue mq2(mq1);
//	mq2.Push(3);
//	return 0;
//}

//class Date
//{
//public:
//	//构造函数
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//拷贝构造函数
//	/*
//	Date(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//	}
//	*/
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//void Func(Date& d)
//{
//	d.Print();
//}
//int main()
//{
//	Date d1(2022, 10, 17);
//	Date d2(d1);
//	Func(d1);
//	d2.Print();
//} 

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year <<
//			"-" << _month <<
//			"-" << _day << endl;
//	}
//
//	bool operator==(const Date& d)
//		//编译器会处理成 bool operator(Date* const this, const Date& d)
//	{
//		return _year == d._year &&
//			_month == d._month &&
//			_day == d._day;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2022, 10, 17);
//	Date d2(2022, 10, 17);
//	//cout << (d1 == d2) << endl;
//	if (d1.operator == (d2))
//		cout << "==" << endl;
//	if (d1 == d2) 
//		//编译器会自动处理成d1.operator == (d2)
//		//编译器会自动处理成d1.operator == (&d1, d2)
//		cout << "==" << endl;
//	return 0;
//}

/*if(d1 < d2)
	{
		cout << "<" << endl;
	}
	else if(d1 == d2)
	{
		cout << "==" << endl;
	}
	else
	{
		cout << ">" << endl;
	}*/

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//日期比较大小
//	bool operator<(const Date& d)
//	{
//		if (_year > d._year ||
//			_year == d._year && _month > d._month ||
//			_year == d._year && _month == d._month && _day > d._day)
//			return false;
//		else
//			return true;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022, 10, 17);
//	Date d2(2022, 10, 18);
//	if (d1 < d2)
//		cout << "<" << endl;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	Date operator=(const Date& d)
//	{
//		_year = d._year;
//		_month = d._month;
//		_day = d._day;
//		return *this;
//	}
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1(2022, 10, 17);
//	Date d2(2022, 10, 18);
//	Date d3(d1);//拷贝构造 -- 一个存在的对象去初始化另一个要创建的对象
//	d3 = d2 = d1; //赋值重载/复制拷贝 -- 两个已经存在的对象之间赋值
//	d2.Print();
//	d3.Print();
//	return 0;
//}

void TestSatck()
{
	//Stack st1(10000);
	// 默认生成operator= 不行 需要自己实现
	Stack st1;
	st1.Push(1);
	st1.Push(2);

	Stack st2(10);
	st2.Push(10);
	st2.Push(20);
	st2.Push(30);
	st2.Push(40);

	//st2 = st1;
	st2 = st2;
}

//int main()
//{
//	TestSatck();
//
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1900, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	int _year;
//	int _month;
//	int _day;
//};
//// 赋值运算符重载成全局函数，注意重载成全局函数时没有this指针了，需要给两个参数
//Date& operator=(Date& left, const Date& right)
//{
//	if (&left != &right)
//	{
//		left._year = right._year;
//		left._month = right._month;
//		left._day = right._day;
//	}
//	return left;
//}


class MyQueue
{
public:
	void Push(int x)
	{
		_pushST.Push(x);
	}

	Stack _pushST;
	Stack _popST;
};

int main()
{
	MyQueue mq1;
	mq1.Push(1);
	mq1.Push(2);

	MyQueue mq2;
	mq2.Push(10);
	mq2.Push(20);
	mq2.Push(30);
	mq2.Push(40);
	mq1 = mq2;
	return 0;
}