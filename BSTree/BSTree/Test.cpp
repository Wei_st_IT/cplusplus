#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include"BSTree_K.h"
#include"BSTree_K_V.h"
namespace K
{
	void TestBSTree1()
	{
		BSTree<int> bst;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			bst.Insert(e);
		}
		bst.InOrder();

		bst.Insert(16);
		bst.Insert(9);
		bst.InOrder();
	}
	void TestBSTree2()
	{
		BSTree<int> bst;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			bst.Insert(e);
		}
		bst.InOrder();
		//删除3和8
		bst.Erase(3);
		bst.Erase(8);
		bst.InOrder();
		//删除14和7
		bst.Erase(14);
		bst.Erase(7);
		bst.InOrder();
		//删除所有值
		for (auto e : a)
		{
			bst.Erase(e);
		}
		bst.InOrder();
	}

	void TestBSTree3()
	{
		BSTree<int> bst;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			bst.Insert(e);
		}
		bst.InOrder();

		BSTree<int> copy = bst;
		copy.InOrder();
	}
	void TestBSTree4()
	{
		BSTree<int> bst;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			bst.InsertR(e);
		}
		bst.InOrder();

		BSTree<int> copy = bst;
		copy.InOrder();
	}
	void TestBSTree5()
	{
		BSTree<int> bst;
		int a[] = { 8, 3, 1, 10, 6, 4, 7, 14, 13 };
		for (auto e : a)
		{
			bst.InsertR(e);
		}
		bst.InOrder();
		//删除3和8
		bst.EraseR(3);
		bst.EraseR(8);
		bst.InOrder();
		//删除14和7
		bst.EraseR(14);
		bst.EraseR(7);
		bst.InOrder();
		//删除所有值
		for (auto e : a)
		{
			bst.EraseR(e);
		}
		bst.InOrder();
	}
}
namespace K_V
{
	void TestBSTree1()
	{
		// 输入单词，查找单词对应的中文翻译
		BSTree<string, string> dict;
		dict.InsertR("string", "字符串");
		dict.InsertR("tree", "树");
		dict.InsertR("left", "左边");
		dict.InsertR("right", "右边");
		dict.InsertR("sort", "排序");
		// 插入词库中所有单词
		string str;
		while (cin >> str)
		{
			BSTreeNode<string, string>* ret = dict.FindR(str);
			if (ret == nullptr)
			{
				cout << "单词拼写错误，词库中没有这个单词:" << str << endl;
			}
			else
			{
				cout << str << "中文翻译:" << ret->_value << endl;
			}
		}
	}
	void TestBSTree2()
	{
		// 统计水果出现的次数
		string arr[] = { "苹果", "西瓜", "苹果", "西瓜", "鸭梨", "苹果", "西瓜","苹果", "香蕉", "苹果", "香蕉" };
		BSTree<string, int> countTree;
		for (const auto& str : arr)
		{
			// 先查找水果在不在搜索树中
			// 1、不在，说明水果第一次出现，则插入<水果, 1>
			// 2、在，则查找到的节点中水果对应的次数++
			//BSTreeNode<string, int>* ret = countTree.Find(str);
			auto ret = countTree.FindR(str);
			if (ret == NULL)
			{
				countTree.InsertR(str, 1);
			}
			else
			{
				ret->_value++;
			}
		}
		countTree.InOrder();
	}
}

int main()
{
	//K::TestBSTree1();
	//K::TestBSTree2();
	//K::TestBSTree3();
	//K::TestBSTree4();
	//K::TestBSTree5();
	K_V::TestBSTree1();
	//K_V::TestBSTree2();
	return 0;
}
