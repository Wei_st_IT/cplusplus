#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;
#include "bitset.h"

int main()
{
	bitset_realize::test_bitset();
	bitset_realize::test_twobitset();

	return 0;
}