#pragma once
#include <iostream>
#include <vector>

namespace bitset_realize
{
	//N个比特位的位图
	template<size_t N>
	class bitset
	{
	public:
		//构造函数
		bitset()
		{
			_bits.resize(N / 8 + 1, 0);
			_bits.resize((N >> 3) + 1, 0);
		}

		//把x映射的位标记成1
		void set(size_t x)
		{
			//size_t i = x / 8;
			size_t i = x >> 3;
			size_t j = x % 8;
			_bits[i] |= (1 << j);
		}

		//把x映射的位标记成0
		void reset(size_t x)
		{
			size_t i = x >> 3;
			size_t j = x % 8;
			_bits[i] &= (~(1 << j));
		}

		//判断指定比特位x的状态是否为1
		bool test(size_t x)
		{
			size_t i = x >> 3;
			size_t j = x % 8;
			return _bits[i] & (1 << j);
		}

		//翻转指定pos
		void flip(size_t x)
		{
			size_t i = x >> 3;
			size_t j = x % 8;
			_bits[i] ^= (1 << j);
		}

		//获取位图中可以容纳位N的个数
		size_t size()
		{
			return N;
		}

		//统计set中1的位数
		size_t count()
		{
			size_t count = 0;
			for (auto e : _bits)
			{
				int n = e;
				while (n)
				{
					n = n & (n - 1);
					count++;
				}
			}
			return count;
		}

		//判断所有比特位若无置为1，返回true
		bool none()
		{
			for (auto e : _bits)
			{
				if (e != 0)
				{
					return false;
				}
			}
			return true;
		}

		//判断位图中是否有位被置为1，若有则返回true
		bool any()
		{
			return !none();
		}

		//全部NUM个bit位被set返回true
		bool all()
		{
			size_t size = _bits.size();
			for (size_t i = 0; i < size - 1; i++)
			{
				if (~(_bits[i]) != 0)
				{
					return false;
				}
			}
			for (size_t j = size - 1; j < N % 8; j++)
			{
				if ((_bits[size - 1] & (1 << j)) == 0)
				{
					return false;
				}
			}

			return true;
		}

	private:
		vector<char> _bits;//位图
	};

	void test_bitset()
	{
		//bitset<100> bs1;
		//bitset<-1> bs2;
		bitset<0xffffffff> bs2;

		bs2.set(10);
		bs2.set(10000);
		bs2.set(8888);

		cout << bs2.test(10) << endl;
		cout << bs2.test(10000) << endl;
		cout << bs2.test(8888) << endl;
		cout << bs2.test(8887) << endl;
		cout << bs2.test(9999) << endl << endl;

		bs2.reset(8888);
		bs2.set(8887);

		cout << bs2.test(10) << endl;
		cout << bs2.test(10000) << endl;
		cout << bs2.test(8888) << endl;
		cout << bs2.test(8887) << endl;
		cout << bs2.test(9999) << endl;
	}

	template<size_t N>
	class twobitset
	{
	public:
		void set(size_t x)
		{
			if (!_bs1.test(x) && !_bs2.test(x)) // 00
			{
				_bs2.set(x); // 01
			}
			else if (!_bs1.test(x) && _bs2.test(x)) // 01
			{
				_bs1.set(x);
				_bs2.reset(x); // 10
			}

			// 10 不变
		}

		void PirntOnce()
		{
			for (size_t i = 0; i < N; ++i)
			{
				if (!_bs1.test(i) && _bs2.test(i))
				{
					cout << i << endl;
				}
			}
			cout << endl;
		}

	private:
		bitset<N> _bs1;
		bitset<N> _bs2;
	};

	void test_twobitset()
	{
		twobitset<100> tbs;
		int a[] = { 3, 5, 6, 7, 8, 9, 33, 55, 67, 3, 3, 3, 5, 9, 33 };
		for (auto e : a)
		{
			tbs.set(e);
		}

		tbs.PirntOnce();
	}
}