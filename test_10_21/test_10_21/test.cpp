#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	void Print()
//	{
//		cout << _year << 
//			"-" << _month << 
//			"-" << _day << endl;
//	}
//private:
//	//内置类型
//	int _year;
//	int _month;
//	int _day;
//};
//
//
//int main()
//{
//	const Date d1(2022, 10, 18);
//	Date d2(d1);
//	d1.Print();
//	d2.Print();
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//取地址&重载
//	Date* operator&()
//	{
//		//return this;
//		return nullptr;
//	}
//	//const取地址&重载
//	const Date* operator&()const
//	{
//		//return this;
//		return nullptr;
//	}
//private:
//	int _year;
//	int _month;
//	int _day;
//};
//int main()
//{
//	Date d1;
//	Date d2;
//	cout << &d1 << endl;
//	cout << &d2 << endl;
//	return 0;
//}

//class A
//{
//public:
//	A(int a)
//		:_a1(a)
//		, _a2(_a1)
//	{}
//
//	void Print() {
//		cout << _a1 << " " << _a2 << endl;
//	}
//private:
//	int _a2;
//	int _a1;
//};
//
//class B
//{
//public:
//	B()
//	{}
//
//private:
//	A _aa;
//};
//
//int main()
//{
//	//A  aa;
//	B bb;
//
//	return 0;
//}

class Date
{
public:
	//explicit 
		Date(int year, int month = 1, int day = 1)
		:_year(year)
		, _month(month)
		, _day(day)
	{
		cout << "Date 构造" << endl;
	};

	//Date(const Date& d)
	//{
	//	_year = d._year;
	//	_month = d._month;
	//	_day = d._day;
	//	cout << "Date 拷贝构造" << endl;
	//}

private:
	int _year;
	int _month;
	int _day;
};

//int main()
//{
//	Date d1(2022);
//	// 隐式类型的转换
//	Date d2 = 2022;
//	const Date& d5 = 2022;
//
//	//Date d3(d1);
//	//Date d4 = d1;
//
//	//Date d3(2022, 10, 20);//构造
//	//Date d4(d3);//拷贝构造
//	//Date d5 = { 2022, 10, 20 }; //构造+拷贝构造-->构造
//	//const Date& d6 = { 2022, 10, 20 }; //构造
//	return 0;
//}

//int N = 0;
//class A
//{
//public:
//	A(int a = 0)
//		:_a(a)
//	{
//		++N;
//	}
//	A(const A& aa)
//		:_a(aa._a)
//	{
//		++N;
//	}
//private:
//	int _a;
//};
//void F1(A& aa)
//{}
//A& F2()
//{
//	A aa;
//	return aa;
//}
//int main()
//{
//	A aa1(1);
//	A aa2 = 2;
//	A aa3 = aa1;
//	cout << N << endl;
//	F1(aa1);
//	cout << N << endl;
//	F2();
//	cout << N << endl;
//	return 0;
//}

//class A
//{
//public:
//	A() { ++_scount; }
//	A(const A& t) { ++_scount; }
//	~A() { --_scount; }
//	static int GetACount() { return _scount; }
//private:
//	static int _scount;
//};
//int A::_scount = 0;
//void TestA()
//{
//	cout << A::GetACount() << endl;
//	A a1, a2;
//	A a3(a1);
//	cout << A::GetACount() << endl;
//}
//
//int main()
//{
//	TestA();
//	return 0;
//}


class A
{
public:
	A(int a = 0)
		:_a(a)
	{
		cout << "A 构造" << endl;
	}

	~A()
	{
		cout << "A 析构" << endl;
	}

private:
	int _a;
};

class Solution {
public:
	int Sum_Solution(int n) {
		//...
		return n;
	}
};

int main()
{
	// 有名对象
	//A aa0;
	//A aa1(1);
	//A aa2 = 2;

	//A aa3();

	// 匿名对象 --生命周期只在当前这一行
	A();
	A(3);

	Solution so;
	so.Sum_Solution(10);

	Solution().Sum_Solution(10);

	return 0;
}