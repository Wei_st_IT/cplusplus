#pragma once

class Stack
{
public:
	void Push(int x);
	void Init();
	void Pop();
private:
	int* _array;
	size_t _capacity;
	size_t _size;
};

