#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;
//属性 + 方法
//struct 升级 
//1.兼容C语言中struct的所有用法
//2.升级成了类

typedef int DataType;
//struct Stack
//{
//	void Init(size_t capacity)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (nullptr == _array)
//		{
//			perror("malloc fail");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//	void Push(const DataType& data)
//	{
//		// 扩容
//		_array[_size] = data;
//		++_size;
//	}
//	DataType Top()
//	{
//			return _array[_size - 1];
//	}
//	void Destroy()
//	{
//		if (_array)
//		{
//			free(_array);
//			_array = nullptr;
//			_capacity = 0;
//			_size = 0;
//		}
//	}
//	DataType* _array;
//	size_t _capacity;
//	size_t _size;
//};

typedef struct  Queue
{
	//
}Q;

void QueueInit(Q* pq)
{}
void QueuePush(Q* pq, DataType x);

//C++不加struct可以编译通过，因为这里ListNode变成类
//struct ListNode
//{
//	ListNode* next;//C++可以不加struct
//};
//
//int main()
//{
//	Stack s;
//	s.Init(10);
//	s.Push(1);
//	s.Push(2);
//	s.Push(3);
//	cout << s.Top() << endl;
//	s.Destroy();
//	Q q1;
//	struct Queue q2;
//	QueueInit(&q1);
//
//	return 0;
//}


class className
{
	// 类体：由成员函数和成员变量组成
}; // 一定要注意后面的分号


//class student
//{
//public:
//	//成员函数
//	void showInfo()
//	{
//		cout << _names << " " << _height << " " << _weight << " " << endl;
//	}
//private:
//	//成员变量
//	int _height;
//	int _weight;
//	char _names[20];
//};


//#include "test.h"
//void student::showInfo()
//{
//	cout << _names << " " << _height << " " << _weight << " " << endl;
//}

//class Stack
//{
//public:
//	void Init(size_t capacity)
//	{
//		_array = (DataType*)malloc(sizeof(DataType) * capacity);
//		if (nullptr == _array)
//		{
//			perror("malloc fail");
//			return;
//		}
//		_capacity = capacity;
//		_size = 0;
//	}
//protected:
//	void Push(const DataType& data)
//	{
//		// 扩容
//		_array[_size] = data;
//		++_size;
//	}
//private:
//	DataType* _array;
//	size_t _capacity;
//	size_t _size;
//};
//
//int main()
//{
//	Stack s;
//	s.Init(10);
//	s.Push(1);
//	cout << s._capacity << endl;
//	return 0;
//}

//栈
class Stack
{
public:
	void Push(int x)
	{}
};
//队列
class Queue
{
	void Push(int x)
	{}
};