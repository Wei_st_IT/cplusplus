#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>

using namespace std;

class Person
{
public:
	void Print()
	{
		cout << "name:" << _name << endl;
		cout << "age" << _age << endl;
	}
protected:
	string _name = "wei"; // 姓名
//private:
	int _age = 10; // 年龄
};

//继承后父类的Person的成员（成员函数+成员变量）都会变成子类的一部分。
//这里体现出了Student和Teacher复用了Person的成员。
//下面我们使用监视窗口查看Student和Teacher对象，可以看到变量的复用。
//调用Print可以看到成员函数的复用。

class Student : public Person
{
public:
	void Func()
	{
		cout << "name:" << _name << endl;
		cout << "age:" << _age << endl;
	}
protected:
	int _stuid; // 学号
};

class Teacher : public Person
{
protected:
	int _jobid; // 工号
};

int main()
{
	Student s1;
	Teacher t1;
	s1.Print();
	t1.Print();
	
	return 0;
}

//class Person
//{
//protected:
//	string _name; // 姓名
//	string _sex; // 性别
////private:
//public:
//	int _age; // 年龄
//};
//
//class Student : public Person
//{
//public:
//	int _No; // 学号
//};
//
//int main()
//{
//	Person p;
//	Student s;
//
//	// 中间不存在类型转换 - 没有产生临时变量
//	p = s;
//
//	int i = 1;
//	double d = 1.1;
//	i = d; // 产生类型转换，出现临时变量，具有常性
//
//	Person& rp = s;
//	rp._age = 1;
//
//	Person* ptrp = &s;
//	ptrp->_age++;
//
//	return 0;
//}


//// 父类
//class Person
//{
//protected:
//	string _name = "小李子"; // 姓名
//	int _num = 111; 	   // 身份证号
//};
//
//// 子类
//class Student : public Person
//{
//public:
//	void Print()
//	{
//		cout << " 姓名:" << _name << endl;
//		cout << " 学号:" << _num<< endl;
//		cout << " 身份证号:" << Person::_num << endl;
//	}
//protected:
//	int _num = 999; // 学号
//};
//
//int main()
//{
//	Student s;
//	s.Print();
//	return 0;
//}


// B中的fun和A中的fun不是构成重载，因为不是在同一作用域
// B中的fun和A中的fun构成隐藏，成员函数满足函数名相同就构成隐藏。
class A
{
public:
	void fun()
	{
		cout << "A::fun()" << endl;
	}
};
class B : public A
{
public:
	void fun(int i)
	{
		cout << "B::fun(int i)->" << i << endl;
	}
};
//int main()
//{
//	B b;
//	b.fun(10);//构成隐藏，屏蔽父类，调用子类的 B::fun(int i)->10
//	b.A::fun();//指定作用域调用父类   A::fun()
//	//b.fun(); 编译报错，构成隐藏，必须指定父类作用域或传参
//};

//// 父类
//class Person
//{
//public:
//	Person(const char* name = "wei")
//		:_name(name)
//	{
//		cout << "Person()" << endl;
//	}
//	// 拷贝构造
//	Person(const Person& p)
//		:_name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//	// 赋值重载
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//		{
//			_name = p._name;
//			return *this;
//		}
//	}
//	// 析构函数
//	~Person()
//	{
//		cout << "~Person()" << endl;
//	}
//protected:
//	string _name; // 姓名
//};
//
//// 子类
//class Student : public Person
//{
//public:
//	Student(const char* name, int num)
//		:Person(name)
//		,_num(num)
//	{}
//
//	Student(const Student& s)
//		:Person(s)
//		,_num(s._num)
//	{}
//
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			Person::operator=(s);
//			_num = s._num;
//		}
//	}
//protected:
//	int _num; // 年龄
//};
//
//int main()
//{
//	//Student s1;
//	Student s2("zhangsan", 18);
//	Student s3(s2);
//	return 0;
//}


