#pragma once
#include <iostream>
#include<assert.h>
using std::cout;
using std::cin;
using std::endl;
using std::ostream;
using std::istream;
class Date
{
public:
	//友元声明可以在类的任意位置
	friend ostream& operator<<(ostream& out, const Date& d);
	friend istream& operator>>(istream& out, Date& d);

	bool isLeapYear(int year)
	{
		//四年一闰百年不闰或四百年一闰
		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}
	//获取某月天数
	int GetMonthDay(int year, int month);
	//构造函数
	Date(int year = 1, int month = 1, int day = 1);
	//析构函数
	~Date();
	//构造函数
	Date(const Date& d);
	//赋值运算符重载
	Date& operator=(const Date& d);
	//取地址操作符重载
	Date* operator&();
	//const取地址操作符重载
	const Date* operator&() const;
	//打印
	void Print() const
	{
		cout << _year << "-" << _month << "-" << _day << endl;
	}
	// <运算符重载
	bool operator<(const Date& d) const;
	// ==运算符重载
	bool operator==(const Date& d) const;
	// <=运算符重载
	bool operator<=(const Date& d) const;
		// >运算符重载
	bool operator>(const Date& d) const;

	// >=运算符重载
	bool operator>=(const Date& d) const;
	// !=运算符重载
	bool operator!=(const Date& d) const;

	//日期 + 天数
	Date operator+(int day) const;
	//日期 += 天数
	Date& operator+=(int day);
	//日期 -= 天数
	Date& operator-=(int day);
	//日期 - 天数
	Date operator-(int day) const;

	//前置++
	Date& operator++(); //无参的为前置
	//后置++
	Date operator++(int i); //有参数的为后置
	//前置--
	Date& operator--(); //无参的为前置
	//后置--
	Date operator--(int i); //有参数的为后置

	//日期 - 日期
	int operator-(const Date& d) const;

	// cout << d1; // operator<<(cout, d1);
	//void operator<<(ostream& out);

private:
	int _year;
	int _month;
	int _day;
};
//法一：
//operator(cout, d1) cout << d1;
//static void operator<<(ostream& out, const Date& d)
//{
//	cout << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//}

////法二：
////void operator<<(std::ostream& out, const Date& d);
//
//链式调用
//ostream& operator<< (ostream& out, const Date& d);
//
//内联函数
//输出
inline ostream& operator<<(ostream& out, const Date& d)
{
	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
	return out;
}
//输入
//cin >> d1 operator(cin, d1)
inline istream& operator>>(istream& in, Date& d)
{
	in >> d._year >> d._month >> d._day;
	return in;
}
