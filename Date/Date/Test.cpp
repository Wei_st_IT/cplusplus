#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

void TestDate1()
{
	Date d1(2022, 10, 18);
	Date d2(2022, 10, 19);
	Date d3(2022, 10, 20);
	//注意优先级，<<的大于<，所以要加上括号
	if(d2 > d1)
	cout << "d2 > d1" << endl;
	if(d3 > d2)
	cout << "d3 > d2" << endl;
}
void TestDate2()
{
	Date d1(2022, 10, 18);
	Date d2 = d1 + 25;
	d1.Print();
	d2.Print();
	d1 += 25;
	d1.Print();
}
void TestDate3()
{
	Date d1(2022, 10, 18);
	Date d2 = d1 - 30;
	d2.Print();
	d1 -= 30;
	d1.Print();
	Date d3(2022, 10, 18);
	d3 += 100000000;
	d3.Print();
	d3 -= 100000000;
	d3.Print();
}

void TestDate4()
{
	Date d1(2022, 10, 18);
	d1 -= -100;
	d1.Print();

	d1 += 100;
	d1.Print();

}

void TestDate5()
{
	Date d1(2022, 10, 18);
	Date ret1 = ++d1; //d1.operator++()
	ret1.Print();
	d1.Print();
	Date ret2 = d1++; //d1.operator++(10)
	ret2.Print();
	d1.Print();
}

void TestDate6()
{
	Date d1(2022, 10, 18);
	Date d2(2020, 10, 19);
	cout << (d1 - d2) << endl;
	cout << (d2 - d1) << endl;
}

void TestDate7()
{
	Date d1(2022, 3, 5);
	Date d2(2022, 5, 4);
//	//d1 << cout;  //等价于d1.operator<<(cout);
	cout << d1;
	cout << d1 << d2;// 链式调用 (cout << d1) << d2;
	Date d3, d4;
	cin >> d3 >> d4;
	cout << d3 << d4;
	cout << d3 - d4 << endl;
	cout << d3 - 1000 << endl;
	cout << d4 + 1000 << endl;
}

int main()
{
	TestDate1();
	TestDate2();
	TestDate3();
	TestDate4();
	TestDate5();
	TestDate6();
	TestDate7();
	return 0;
}