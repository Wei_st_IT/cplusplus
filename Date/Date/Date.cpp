#define _CRT_SECURE_NO_WARNINGS 1
#include"Date.h"

//获取某月天数
int Date::GetMonthDay(int year, int month) 
{
	assert(year >= 0 && month > 0 && month < 13);
	static int monthDayArray[13] = { 0,31,28,31,30,31,30,31,31,30,31,30,31 };
	if (isLeapYear(month) && month == 2)
	{
		return 29;
	}
	else
	{
		return monthDayArray[month];
	}
}

//构造函数 - 可写可不写
Date::Date(int year, int month, int day)
{
	if (year >= 1 && month <= 12 && month >= 1 && day <= GetMonthDay(year, month))
	{
		_year = year;
		_month = month;
		_day = day;
	}
	else
		cout << "日期非法" << endl;
}

//析构函数 - 可写可不写
Date::~Date()
{
	;
}

//拷贝构造函数 - 可写可不写
Date::Date(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
}

//赋值运算符重载 - 可写可不写
Date& Date::operator=(const Date& d)
{
	_year = d._year;
	_month = d._month;
	_day = d._day;
	return *this;
}

//取地址操作符重载
Date* Date::operator&()
{
	return this;
	//return nullptr;
}

//const取地址操作符重载
const Date* Date::operator&()const
{
	return this;
	//return nullptr;
}

// <运算符重载
bool Date::operator<(const Date& d) const //类外访问成员函数需要设定类域
{
	if (_year < d._year ||
		_year == d._year && _month < d._month ||
		_year == d._year && _month == d._month && _day < d._day)
		return true;
	else
		return false;
}
// ==运算符重载
bool Date::operator==(const Date& d) const
{
	if (_year == d._year && _month == d._month && _day < d._day)
		return true;
	else
		return false;
}
// <=运算符重载
bool Date::operator<=(const Date& d) const
{
	return *this < d || *this == d;
}
// >运算符重载
bool Date::operator>(const Date& d) const
{
	return !(*this <= d);
	//return (d < *this);
}
// >=运算符重载
bool Date::operator>=(const Date& d) const
{
	return !(*this < d);
}
// !=运算符重载
bool Date::operator!=(const Date& d) const
{
	return !(*this == d);
}

//日期 + 天数 法一：
//d1 + 100  -- > d1.operator+(day)
//Date Date::operator+(int day) const
//{
//	Date ret(*this); //拷贝构造
//	//Date ret = *this;
//	ret._day += day;
//	while (ret._day < GetMonthDay(_year, _month))
//	{
//		ret._day -= GetMonthDay(_year, _month);
//		_month++;
//		if (_month > 12)
//		{
//			_year++;
//			_month = 1;
//		}
//	}
//	return ret;
//}
//日期 + 天数 法二：
Date Date::operator+(int day) const
{
	Date ret(*this);
	ret += day;
	return ret;
}

//日期 += 天数 法一：
Date& Date::operator+=(int day) 
{
	if (day < 0)
	{
		//return *this -= -day;
		return *this -= abs(day);
	}
          	_day += day;
	while (_day > GetMonthDay(_year, _month))
	{
		_day -= GetMonthDay(_year, _month);
		_month++;
		if (_month > 12)
		{
			_year++;
			_month = 1;
		}
	}
	return *this;
}
//日期 += 天数 法二：复用
//Date& Date::operator+=(int day)
//{
//	*this = *this + day; //让d1+过天数后再返回给自己从而实现+=
//	return *this;
//}

//日期 -= 天数  d1-=100
Date& Date::operator-=(int day)
{
	//如果减去的天数是负数，要单独处理，直接调用+=
	if (day < 0)
	{
		return *this += -day;
	}
	_day -= day;
	while (_day <= 0)
	{
		_month--;
		if (_month == 0)
		{
			_year--;
			_month = 12;
		}
		_day += GetMonthDay(_year, _month);
	}
	return *this;
}
//日期 - 天数
Date Date::operator-(int day) const
{
	Date ret(*this);
	ret -= day;
	return ret;
}
//前置++ 无参的为前置
Date& Date::operator++() 
{
	*this += 1;
	return *this;
}
//后置++ 有参数的为后置
Date Date::operator++(int i)
{
	Date tmp(*this);
	*this += 1;
	return *this;
}
//前置-- 无参的为前置
Date& Date::operator--()
{
	*this -= 1;
	return *this;
}
//后置-- 有参数的为后置
Date Date::operator--(int i)
{
	Date tmp(*this);
	*this -= 1;
	return *this;
}

//日期 - 日期
int Date::operator-(const Date& d) const
{
	int flag = 1;
	Date max = *this;
	Date min = d;
	if (*this < d)
	{
		min = *this;
		max = d;
		flag = -1;
	} //确保max是大的，min是小的
	int n = 0;
	while (min != max)
	{
		min++;
		n++;
	}//算出min和max之间绝对值差距
	return n * flag; //如果d1大，结果为正，d2大结果为负
}


//void Date::operator<<(ostream& out)
//{
//	cout << _year << "年" << _month << "月" << _day << "日" << endl;
//}

//法二：
//void operator<<(ostream& out, const Date& d)
//{
//	cout << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//}

//链式调用
//ostream& operator<<(ostream& out, const Date& d)
//{
//	out << d._year << "年" << d._month << "月" << d._day << "日" << endl;
//	return out;
//}
