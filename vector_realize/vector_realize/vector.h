#pragma once

// vector的模拟实现
namespace vector_realize
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		typedef const T* const_iterator;

		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}

		// 构造函数
		vector() // --> 无参构造
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{}

		// C++11构造函数 法一：
		vector(initializer_list<T> il)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			for (auto e : il)
			{
				push_back(e);
			}
		}

		// C++11构造函数 法二：
		vector(initializer_list<T> il)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			_start = new T[il.size()];
			_finish = _start + il.size();
			_endofstorage = _start + il.size();
			iterator vit = _start;
			// initializer_list只支持迭代器访问
			// typename auto it = il.begin();
			typename initializer_list<T>::iterator it = il.begin();
			while (it != il.end())
			{
				*vit++ = *it++;
			}

			/*for (auto e : il)
			{
				*vit++ = e;
			}*/
		}

		// --> 迭代器构造
		// 若使用iterator做迭代器，会导致初始化的迭代器区间[first,last)只能是vector的迭代器
		// 重新声明迭代器，迭代器区间[first,last)可以是任意容器的迭代器
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}
		
		//vector<int> v1(10, 5);
		//vector<char> v2(10, 'A');
		// 带参构造 --> 初始化容器为n个val
		vector(size_t n, const T& val = T())
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		//// 重载1
		//vector(long n, const T& val = T())
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	reserve(n);
		//	for (size_t i = 0; i < n; i++)
		//	{
		//		push_back(val);
		//	}
		//}

		//// 重载2
		//vector(int n, const T& val = T())
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	reserve(n);
		//	for (int i = 0; i < n; i++)
		//	{
		//		push_back(val);
		//	}
		//}

		/*
		* 重载原因解释：
		* 理论上将，提供了vector(size_t n, const T& value = T())之后
		* vector(int n, const T& value = T())就不需要提供了，但是对于：
		* vector<int> v(10, 5);
		* 编译器在编译时，认为T已经被实例化为int，而10和5编译器会默认其为int类型
		* 就不会走vector(size_t n, const T& value = T())这个构造方法，
		* 最终选择的是：vector(InputIterator first, InputIterator last)
		* 因为编译器觉得区间构造两个参数类型一致，因此编译器就会将InputIterator实例化为int
		* 但是10和5根本不是一个区间，编译时就报错了
		* 故需要增加该构造方法
		*/

		// 拷贝构造 v1(v)
		// 传统写法
		//vector(const vector<T>& v)
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	//_start = new T[capacity()]; // 开辟一块和v大小相同的空间
		//	// //memcpy(_start, v._start, sizeof(T) * size()); //error
		//	//for (size_t i = 0; i < size(); i++)
		//	//{
		//	//	_start[i] = v[i];
		//	//}
		//	_start = new T[v.capacity()]; // 开辟一块和v大小相同的空间
		//	for (size_t i = 0; i < v.size(); i++)
		//	{
		//		_start[i] = v._start[i];
		//	}

		//	_finish = _start + v.size();
		//	_endofstorage = _start + v.capacity();
		//}
		
		// 现代写法1
		/*vector(const vector<T>& v)
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			reverse(v.capacity());
			for (const auto& e : v)
			{
				push_back(e);
			}
		}*/

		// 现代写法2
		vector(const vector<T>& v)
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			vector<T> tmp(v.begin(), v.end()); // 调用迭代器构造函数
			swap(tmp);
		}

		// 析构函数
		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = _finish = _endofstorage = nullptr;
			}
		}

		// 赋值运算符重载
		// 传统写法
		/*vector<T>& operator=(const vector<T>& v)
		{
			if (this != &v)
			{
				T* tmp = new T[v.capacity()];
				for (size_t i = 0; i < v.size(); i++)
				{
					tmp[i] = v._start[i];
				}
				_start = tmp;

				_finish = _start + v.size();
				_endofstorage = _start + v.capacity();
			}

			return *this;
		}*/

		// 现代写法
		/*vector<T>& operator=(const vector<T>& v)
		{
			if (this != &v)
			{
				vector<T> tmp(v);
				swap(tmp);
			}

			return *this;
		}*/
		// 简化版 -- 不能检查自己给自己赋值
		// v1 = v2; v1 = v1;
		vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}

		// 赋值运算符重载
		vector<T>& operator=(initializer_list<T> il)
		{
			vector<T> tmp(l);
			std::swap(_start, tmp._start);
			std::swap(_finish, tmp._finish);
			std::swap(_endofstorage, tmp._endofstorage);

			return *this;
		}

		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		bool empty() const
		{
			return _endofstorage == _finish;
		}

		T& operator[](size_t n)
		{
			assert(n < size());
			return _start[n];
		}

		const T& operator[](size_t n) const
		{
			assert(n < size());
			return _start[n];
		}

		T& front()
		{
			return *_start;
		}

		T& back()
		{
			return *(_finish - 1);
		}

		const T& front() const
		{
			return *_start;
		}

		const T& back() const
		{
			return *(_finish - 1);
		}

		void reserve(size_t n)
		{
			int oldSize = size();
			if (capacity() < n)
			{
				// 1.开辟新空间
				T* tmp = new T[n];
				if (_start)
				{
					//2.拷贝元素
					// 这里直接用memcpy会有问题，发生浅拷贝
					//memcpy(tmp, _start, sizeof(T) * size());
					for (size_t i = 0; i < oldSize; i++)
					{
						tmp[i] = _start[i]; // 本质调用赋值运算符重载进行深拷贝
					}
					//3. 释放旧空间
					delete[] _start;
				}
				_start = tmp;
			}
			// 这里_start的位置变了，而_finish还是原来的位置
			//_finish = _start + size(); error 
			_finish = _start + oldSize;
			_endofstorage = _start + n;
		}

		void push_back(const T& x)
		{
			if (_finish == _endofstorage)
			{
				int newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
			}
			*_finish = x;
			_finish++;

			//复用：insert(end() - 1);
		}
		
		//假如size = 5, capacity = 10
		// n > 10 扩容+填充数据
		// 5 < n <= 10 填充数据
		// n <= 5 删除数据
		void resize(size_t n, const T val = T())
		{
			// 1.空间不够增容
			if (n > capacity())
			{
				reserve(n);
			}
			// 2.n > size(),填充数据,将size()扩大到n
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
			else
			{
				//3.n < size(), 将数据个数缩小到n
				_finish = _start + n;
			}
		}

		void pop_back()
		{
			if (_finish > _start)
			{
				_finish--;
			}
			/*assert(!empty());
			_finish--;*/

			// 复用：erase(end() - 1);
		}


		// 迭代器失效 : 野指针问题
		// 不能保证原地扩容，扩容就会导致迭代器失效：地址变化了 --> 
		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos <= _finish);
			if (_finish == _endofstorage)
			{
				size_t len = pos - _start;
				int newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
				// 扩容会导致迭代器失效，需要重新修改数据pos
				pos = _start + len;
			}
			// 挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}
			// 插入数据
			*pos = val;
			_finish++;

			return pos;
		}

		// 返回删除数据的下一个数据
		// 方便解决:一边遍历一边删除的迭代器失效问题
		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *begin;
				begin++;
			}
			_finish--; 

			return pos; //返回pos的下一个位置
		}

		void swap(vector<T>& v) // 不加const
		{
			// 调用算法库里的swap
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}

		void clear()
		{
			_finish = _start;
		}

	private:
		iterator _start;        //指向容器的头
		iterator _finish;       //指向有效数据的尾
		iterator _endofstorage; //指向容器的尾
	};

	void test_vector1()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//vector<int> v1(10, 5);
		vector<char> v2(10, 'A');

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		v.pop_back();
		v.pop_back();
		v.pop_back();
		v.pop_back();
		v.pop_back();

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector2()
	{
		vector<int> v;
		v.resize(10, -1);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		v.resize(5);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector3()
	{
		vector<int> v;
		//v.reserve(10);
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		
		cout << v.front() << endl;
		cout << v.back() << endl;

		for (size_t i = 0; i < v.size(); ++i)
		{
			cout << v[i] << " ";
		}
		cout << endl;

		v.insert(v.begin(), 0);
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int>::iterator it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			v.insert(it, 30);
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector4()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);

		vector<int>::iterator it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			v.insert(it, 30);
		}

		// insert以后 it还能否继续使用 -- 不能，可能迭代器失效（野指针）
		//(*it)++;
		//*it *= 100;

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector5()
	{
		std::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);

		// it失效还是不失效？ 失效
		std::vector<int>::iterator it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			v.erase(it);
		}

		// 读 
		cout << *it << endl;
		// 写
		(*it)++;

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	//void test_vector6()
	//{
	//	// 要求删除所有偶数
	//	std::vector<int> v;
	//	v.push_back(1);
	//	v.push_back(2);
	//	v.push_back(2);
	//	v.push_back(3);
	//	v.push_back(4);

	//	std::vector<int>::iterator it = v.begin();
	//	while (it != v.end())
	//	{
	//		if (*it % 2 == 0)
	//		{
	//			it = v.erase(it);
	//		}
	//		else
	//		{
	//			++it;
	//		}
	//	}

	//	for (auto e : v)
	//	{
	//		cout << e << " ";
	//	}
	//	cout << endl;
	//}

	void test_vector6()
	{
		// 要求删除所有偶数
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		//v.push_back(5);


		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
			{
				it = v.erase(it);
			}
			else
			{
				++it;
			}
		}

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}


	void test_vector7()
	{
		// 要求删除所有偶数
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);

		vector<int> v1(v);

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

		vector<int> v2;
		v2.push_back(10);
		v2.push_back(20);
		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;

	}

	void test_vector8()
	{
		std::string str("hello");

		// 要求删除所有偶数
		vector<int> v(str.begin(), str.end());
		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		//vector<int> v1(v.begin(), v.end());
		vector<int> v1(10, 1);
		//vector<char> v1(10, 'A');

		for (auto e : v1)
		{
			cout << e << " ";
		}
		cout << endl;
	}
}