#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>
#include <time.h>
using namespace std;

// N个数据需要排序，vector+ 算法sort  list+ sort
void test_op1()
{
	srand((unsigned int)time(0));
	const int N = 1500000;
	vector<int> v;
	v.reserve(N);
	list<int> lt1;

	for (int i = 0; i < N; ++i)
	{
		auto e = rand();
		v.push_back(e);
		lt1.push_back(e);
	}

	int begin1 = clock();
	sort(v.begin(), v.end());
	int end1 = clock();

	int begin2 = clock();
	//sort(lt2.begin(), lt2.end());
	lt1.sort();
	int end2 = clock();
	printf("测试一：");
	printf("vector sort:%d\n", end1 - begin1);
	printf("list sort:%d\n", end2 - begin2);
}

void test_op2()
{
	srand((unsigned int)time(0));
	const int N = 10000000;
	vector<int> v;
	v.reserve(N);

	list<int> lt1;
	list<int> lt2;
	for (int i = 0; i < N; ++i)
	{
		auto e = rand();
		//v.push_back(e);
		lt1.push_back(e);
		//lt2.push_back(e);
	}

	// 拷贝到vector排序，排完以后再拷贝回来
	int begin1 = clock();
	for (auto e : lt1)
	{
		v.push_back(e);
	}
	sort(v.begin(), v.end());
	size_t i = 0;
	for (auto& e : lt1)
	{
		//e = v[i++];
		lt2.push_back(e);
	}
	int end1 = clock();

	int begin2 = clock();
	//sort(lt2.begin(), lt2.end());
	lt1.sort();
	int end2 = clock();
	printf("测试二：");
	printf("vector sort:%d\n", end1 - begin1);
	printf("list sort:%d\n", end2 - begin2);
}

int main()
{
	test_op1();
	test_op2();
	return 0;
}
