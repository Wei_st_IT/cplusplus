#define _CRT_SECURE_NO_WARNINGS 1

#include <iostream>

using namespace std;

////不能被拷贝的类
////C++98:
//class CopyBan
//{
//public:
//	//……
//private:
//	//只声明不定义
//	CopyBan(const CopyBan&);//拷贝构造
//	CopyBan& operator=(const CopyBan&);//赋值运算符重载
//};
//
////C++11:
//class CopyBan
//{
//public:
//	CopyBan(const CopyBan&) = delete;
//	CopyBan& operator=(const CopyBan&) = delete;
//private:
//	//...
//};
//
////不能被继承的类
//class NonInherit
//{
//public:
//	static NonInherit GetInstance()
//	{
//		return NonInherit();
//	}
//private:
//	NonInherit()//私有化构造函数
//	{}
//};
//
//
//class NonInherit final
//{
//public:
//
//private:
//
//};


////只能在堆上创建对象的类
//class HeapOnly
//{
//public:
//	static HeapOnly* CreatObj()
//	{
//		return new HeapOnly;
//	}
//
//	//HeapOnly(const HeapOnly&) = delete;
//
//	~HeapOnly()
//	{
//		cout << "delete" << endl;
//	}
//
//private:
//	//构造函数私有化
//	HeapOnly()
//	{}
//	//拷贝构造私有化，防拷贝 - 只声明不实现
//	HeapOnly(const HeapOnly&);
//};
//
//int main()
//{
//	/*HeapOnly h1;
//	static HeapOnly h2;
//	HeapOnly* h3 = new HeapOnly;*/
//	HeapOnly* ph1 = HeapOnly::CreatObj();
//	HeapOnly* ph2 = HeapOnly::CreatObj();
//	HeapOnly* ph3(ph1);
//	//HeapOnly ph4(*ph3);
//	delete ph1;
//	delete ph2;
//	delete ph3;
//	return 0;
//}

class HeapOnly
{
public:
	static void DeleteObj(HeapOnly* ptr)
	{
		cout << "delete:" << ptr << endl;
		delete ptr;
	}
	void DeleteObj()
	{
		cout << "delete:" << this << endl;
		delete this;
	}
private:
	~HeapOnly()
	{}
};

int main()
{
	/*HeapOnly h1;
	static HeapOnly h2;
	HeapOnly ph4(*ph3);*/
	HeapOnly* ph2 = new HeapOnly;
	//释放
	HeapOnly::DeleteObj(ph2);
	//HeapOnly::DeleteObj(); //error
	ph2->DeleteObj();
	return 0;
}


//只能在栈上创建对象的类
class StackOnly
{
public:
	static StackOnly CreateObj()
	{
		return StackOnly();
	}
private:
	StackOnly()
	{}

};

