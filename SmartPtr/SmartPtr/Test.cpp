#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>

using namespace std;

#include "SmartPtr.h"

int div()
{
	int a, b;
	cin >> a >> b;
	if (b == 0)
		throw invalid_argument("除0错误");
	
	return a / b;
}

//void Func()
//{
//	// 1.如果p1这里new 抛异常会如何？
//	// 2.如果p2这里new 抛异常会如何？
//	// 3.如果div这里new 抛异常会如何？
//
//	int* p1 = new int[10];
//	int* p2 = nullptr;
//	try
//	{
//		p2 = new int[20];
//
//		try
//		{
//			cout << div() << endl;
//		}
//		catch (...)
//		{
//			delete[] p1;
//			delete[] p2;
//
//			throw;
//		}
//	}
//	catch (...)
//	{
//		// ...
//	}
//	delete[] p1;
//	delete[] p2;
//}

template <class T>
class smart_ptr
{
public:
	smart_ptr(T* ptr)
		:_ptr(ptr)
	{}

	~smart_ptr()
	{
		delete _ptr;
		cout << "delete:" << _ptr << endl;
	}

	T& operator*()
	{
		return *_ptr;
	}

	T* operator->()
	{
		return _ptr;
	}

	T& operator[](size_t pos)
	{
		return _ptr[pos];
	}

private:
	T* _ptr;
};

//void Func()
//{
//	//int* p1 = new int[10];
//	//SmartPtr<int> sp1(p1);
//	//int* p2 = new int[20];
//	//SmartPtr<int> sp2(p2);
//
//	SmartPtr<int> sp1(new int);
//	//SmartPtr<int> sp2(new int[20]);
//	SmartPtr<vector<int>> sp2(new vector<int>(5));
//	sp2[2];
//
//	*sp1 = 0;
//	//sp2[1] = 10;
//
//	cout << div() << endl;
//}
//
//int main()
//{
//	try
//	{
//		Func();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//
//	return 0;
//}
//


//int main()
//{
//	/*SmartPtr::auto_ptr<int> ap1(new int(10));
//	SmartPtr::auto_ptr<int> ap2(ap1);
//	*ap1 = 1;
//	cout << *ap1 << endl;
//	cout << *ap2 << endl;*/
//
//	/*SmartPtr::unique_ptr<int> ap1(new int(10));
//	SmartPtr::unique_ptr<int> ap2(ap1);
//	*ap1 = 1;
//	cout << *ap1 << endl;
//	cout << *ap2 << endl;*/
//
//	struct ListNode
//	{
//		/*shared_ptr<ListNode> _next = nullptr;
//		shared_ptr<ListNode> _prev = nullptr;*/
//		weak_ptr<ListNode> _next;
//		weak_ptr<ListNode> _prev;
//		int val = 0;
//		~ListNode()
//		{
//			cout << "~ListNode()" << endl;
//		}
//	};
//
//	std::shared_ptr<ListNode> sp1(new ListNode);
//	std::shared_ptr<ListNode> sp2(new ListNode);
//	cout << sp1.use_count() << endl;
//	cout << sp2.use_count() << endl;
//	sp1->_next = sp2;
//	sp2->_prev = sp1;
//	cout << sp1.use_count() << endl;
//	cout << sp2.use_count() << endl;
//	return 0;
//}

class Date
{
public:
	~Date()
	{
		cout << "~Date" << endl;
	}
private:
	int _year = 1;
	int _month = 1;
	int _day = 1;
};
//
//template <class T>
//struct DeleteArray
//{
//	void operator()(T* ptr)
//	{
//		cout << "delete[]:" << ptr << endl;
//		delete[] ptr;
//	}
//};
//
//template <class T>
//struct Free
//{
//	void operator()(T* ptr)
//	{
//		cout << "free:" << ptr << endl;
//		free(ptr);
//	}
//};
//
//template <class T> 
//struct Fclose
//{
//	//void operator()(T* ptr)
//	void operator()(FILE* ptr)
//	{
//		cout << "fclose:" << ptr << endl;
//		fclose(ptr);
//	}
//};
//
//int main()
//{
//	::unique_ptr<Date, DeleteArray<Date>> up1(new Date[10]);
//	::unique_ptr<Date, Free<Date>> up2((Date*)malloc(sizeof(Date) * 10));
//	::unique_ptr<FILE, Fclose<Date>> up3((FILE*)fopen("Test.cpp", "r"));
//	return 0;
//}


////针对new[]的释放
//template<class T>
//struct DeleteArray
//{
//	void operator()(T* ptr)
//	{
//		cout << "delete[]:" << ptr << endl;
//		delete[] ptr;
//	}
//};
////针对malloc的释放
//template<class T>
//struct Free
//{
//	void operator()(T* ptr)
//	{
//		cout << "free:" << ptr << endl;
//		free(ptr);
//	}
//};
////针对fopen的释放
//struct Fclose
//{
//	void operator()(FILE* ptr)
//	{
//		cout << "fclose:" << ptr << endl;
//		fclose(ptr);
//	}
//};
//int main()
//{
//	//针对new的释放
//	SmartPtr::shared_ptr<Date> sp1(new Date);//默认new的释放
//	//针对new[]的释放
//	std::shared_ptr<Date> sp2(new Date[10], DeleteArray<Date>());//传仿函数的匿名对象释放
//	std::shared_ptr<Date> sp3(new Date[10], [](Date* ptr) {delete[] ptr; });//传lambda表达式释放
//	//针对malloc的释放
//	std::shared_ptr<Date> sp4((Date*)malloc(sizeof(Date) * 10), Free<Date>());//传仿函数的匿名对象释放
//	//针对fopen的释放
//	std::shared_ptr<FILE> sp6((FILE*)fopen("Test.cpp", "r"), Fclose());//传仿函数的匿名对象释放
//	std::shared_ptr<FILE> sp5((FILE*)fopen("Test.cpp", "r"), [](FILE* ptr) {
//		cout << "fclose: " << ptr << endl;
//		fclose(ptr); });//传lambda表达式释放
//	return 0;
//}



int main()
{
	/*SmartPtr::shared_ptr<int> sp1(new int);
	SmartPtr::shared_ptr<int> sp2(sp1);*/
	SmartPtr::shared_ptr<int> sp1(new int);
	SmartPtr::shared_ptr<int> sp2(sp1);
	SmartPtr::shared_ptr<int> sp3(new int);
	sp1 = sp3;
	sp2 = sp3;
	return 0;
}