#pragma once

namespace SmartPtr
{
	template<class T>
	class auto_ptr
	{
	public:
		//构造函数
		auto_ptr(T* ptr)
			:_ptr(ptr)
		{}
		//析构函数
		~auto_ptr()
		{
			if (_ptr != nullptr)
			{
				delete _ptr;
				cout << "delete:" << _ptr << endl;
			}
		}
		//拷贝构造
		auto_ptr(const auto_ptr<T>& ap)
			:_ptr(ap._ptr)
		{
			ap._ptr = nullptr;
		}
		//赋值重载
		auto_ptr& operator=(const auto_ptr<T>& ap)
		{
			if (this != &ap)
			{
				delete _ptr;
				_ptr = ap._ptr;
				ap._ptr = nullptr;
			}
		}
		//像指针一样
		T* operator->()
		{
			return _ptr;
		}
		T& operator*()
		{
			return *_ptr;
		}
		T& operator[](size_t pos) 
		{
			return *(_ptr + pos);
			//return _ptr[pos];
		}
	private:
		T* _ptr;
	};

////////////////////////////////////////////////////////////
	template<class T>
	class unique_ptr
	{
	public:
		unique_ptr(T* ptr)
			:_ptr(ptr)
		{}

		~unique_ptr()
		{
			if (_ptr != nullptr)
			{
				delete _ptr;
				cout << "delete:" << _ptr << endl;
			}
		}

		unique_ptr(const unique_ptr<T>& up) = delete;

		unique_ptr& operator=(const unique_ptr<T>& up) = delete;

		T* operator->()
		{
			return _ptr;
		}
		
		T& operator*()
		{
			return *_ptr;
		}

		T* get() const
		{
			return _ptr;
		}
		
		T& operator[](size_t pos)
		{
			return _ptr[pos];
		}
	private:
		T* _ptr;
	};

/////////////////////////////////////////////////////////

	template<class T>
	class shared_ptr
	{
	public:
		shared_ptr(T* ptr)
			:_ptr(ptr)
			,_pCount(new int(1))
		{}

		void Release()
		{
			if (--(*_pCount) == 0)
			{
				delete _ptr;
				_ptr = nullptr;
				cout << "delete:" << _ptr << endl;
				delete _pCount;
				_pCount = nullptr;
			}
		}

		~shared_ptr()
		{
			if(_ptr != nullptr)
				Release();
		}

		shared_ptr(const shared_ptr<T>& sp)
			:_ptr(sp._ptr)
			,_pCount(sp._pCount)
		{
			(*_pCount)++;
		}

		shared_ptr& operator=(const shared_ptr<T>& sp)
		{
			//if (this != &sp) 不对
			if(_ptr != sp._ptr)
			{
				Release();
				_ptr = sp._ptr;
				_pCount = sp._pCount;
				(*_pCount)++;
			}

			return *this;
		}

		T* operator->()
		{
			return _ptr;
		}

		T& operator*()
		{
			return *_ptr;
		}

		int useCount()
		{
			return *_pCount;
		}
		
		T* get() const
		{
			return _ptr;
		}

		T& operator[](size_t pos)
		{
			return _ptr[pos];
		}
	private:
		T* _ptr;
		int* _pCount;
	};

/////////////////////////////////////////////////////

	template<class T>
	class weak_ptr
	{
	public:
		weak_ptr()
			:_ptr(nullptr)
		{}

		weak_ptr(const shared_ptr<T>& sp)
			:_ptr(sp.get())
		{}

		weak_ptr<T>& operator=(const shared_ptr<T>& sp)
		{
			if (_ptr != sp.get())
			{
				_ptr = sp.get();
			}

			return *this;
		}
	private:
		T* _ptr;
	};
}