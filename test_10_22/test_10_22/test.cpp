#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <cassert>
using namespace std;

//void Test()
//{
//    // 动态申请一个int类型的空间
//    int* ptr4 = new int;
//    // 动态申请一个int类型的空间并初始化为10
//    int* ptr5 = new int(10);
//    // 动态申请3个int类型的空间
//    int* ptr6 = new int[3];
//    // 动态申请10个int类型的空间,并初始化前5个空间为1，2，3，4，5
//    //跟数组的初始化很像，大括号有几个元素，初始化几个元素，其余为0。不过C++11才支持的语法
//    int* ptr7 = new int[10]{ 1,2,3,4,5 };
//    delete ptr4;
//    delete ptr5;
//    delete[] ptr6;
//    delete[] ptr7;
//}
//
//int main()
//{
//    Test();
//    return 0;
//}


//class A
//{
//public:
//	A()
//		:_a(0)
//	{
//		//构造函数
//		cout << "A():" << this << endl;
//	}
//	~A()
//	{
//		//析构函数
//		cout << "~A():" << this << endl;
//	}
//private:
//	int _a;
//};
//
//void Test2()
//{
//	//申请一个A类型的空间
//	A* ptr1 = new A;
//	//申请十个A类型的空间
//	A* ptr2 = new A[10];
//	delete ptr1;
//	delete[] ptr2;
//}
//void Test()
//{
//	//申请一个A类型的空间
//	A* ptr1 = (A*)malloc(sizeof(A));
//	//申请十个A类型的空间
//	A* ptr2 = (A*)malloc(sizeof(A) * 10);
//	free(ptr1);
//	free(ptr2);
//}

//int main()
//{
//	//Test();
//	Test2();
//	return 0;
//}

//struct ListNode
//{
//	struct ListNode* _next;
//	int _val;
//	ListNode(int val)
//		:_next(nullptr)
//	    ,_val(val)
//	{}
//};
//
//int main()
//{
//	ListNode* n1 = (ListNode*)malloc(sizeof(struct ListNode));
//	assert(n1);
//	ListNode* n2 = new ListNode(1);
//	ListNode* n3 = new ListNode(2);
//	ListNode* n4 = new ListNode(3);
//
//}

class A
{
public:
	A()
		:_a(0)
	{
		//构造函数
		cout << "A():" << this << endl;
	}
	~A()
	{
		//析构函数
		cout << "~A():" << this << endl;
	}
private:
	int _a;
};

int main()
{
	A* pa = new A;
	delete pa;
	return 0;
}

//int main()
//{
//	// 一定要匹配使用，否则可能会出现各种情况
//	A* p3 = new A[];
//	delete p3;
//
//	/*A* p4 = new A;
//	delete[] p4;*/
//	return 0;
//}



//int main()
//{
//	while (1)
//	{
//		// malloc失败 返回空指针
//		int* p1 = (int*)malloc(1024 * 100);
//		if (p1)
//		{
//			cout << p1 << endl;
//		}
//		else
//		{
//			cout << "申请失败" << endl;
//			break;
//		}
//	}
//	return 0;
//}

//void Test()
//{
//	while (1)
//	{
//		// new失败 抛异常 -- 不需要检查返回值
//		char* p1 = new char[1024 * 1024 * 1024];
//		cout << (void*)p1 << endl;
//	}
//}
//int main()
//{
//	try
//	{
//		Test();
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}


//int main()
//{
//	// malloc失败 返回空指针 
//	void* p1 = malloc(1024 * 1024 * 1024 * 2);
//	cout << p1 << endl;
//	try
//	{
//		while (1)
//		{
//			// new失败 抛异常 -- 不需要检查返回值
//			char* p2 = new char[1024 * 1024 * 1024];
//			cout << (void*)p2 << endl;
//		}
//	}
//	catch (exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}

	
//int main()
//{
//	//int* p3 = new int;
//	////new的底层原理：转换成调用operator new + 构造函数
//	//delete p3;
//	////delete的底层原理：转换成调用operator delete + 析构函数  
//
//	int* p4 = new int[10];
//	delete[] p4;
//}

//int main()
//{
//	int* p = new int[10];
//	// 将该函数放在main函数之后，每次程序退出的时候就会检测是否存在内存泄漏
//	_CrtDumpMemoryLeaks();
//	return 0;
//}
//
//// 将程序编译成x64的进程，运行下面的程序试试？
//#include <iostream>
//using namespace std;
//int main()
//{
//	void* p = new char[0xfffffffful];
//	cout << "new:" << p << endl;
//	return 0;
//}