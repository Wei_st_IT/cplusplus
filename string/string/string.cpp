#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
using namespace std;

//int main()
//{
//	char arr1[] = "abcd";
//	char arr2[] = "banana";
//	char arr3[] = "卧槽";
//	arr1[3]++;
//	arr1[3]--;
//	arr3[3]++;
//	arr3[3]++;
//	arr3[4]--;
//}

//template<class T>
//class basic_string
//{
//	T* _str;
//	size_t size;
//	size_t capacity;
//};
//
//typedef basic_string<char> string;

void test1()
{
	string s1;
	string s2 = "河南省新乡市";
	string s3("河南省新乡市");
	string s4(10, '#');

	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << s4 << endl;

	s2 += "宝龙广场";
	cout << s2 << endl;

	string s5(s2);
	string s6 = s2;
	cout << s5 << endl;
	cout << s5 << s2 << endl;

	string s7("hello world", 5);
	cout << s7 << endl;

	string s8("hello world", 0, 8);
	cout << s8 << endl;
	string s9(s7, 0, 20);
	cout << s9 << endl;
}

void test2()
{
	string s1("abcd");
	//遍历它

	for (size_t i = 0; i < s1.size(); ++i)
	{
		s1[i]++;
	}
	//s1[10];
	cout << s1 << endl;

	for (auto& ch : s1)
	{
		ch--;
	}
	cout << s1 << endl;

	// 反转一下
	size_t begin = 0, end = s1.size() - 1;
	while (begin < end)
	{
		swap(s1[begin++], s1[end--]);
	}
	cout << s1 << endl;
}

void test_string2()
{
	string s1("hello");
	cout << s1 << endl;
	string s2("world");
	cout << s2 << endl;

	s1 = s2; //world
	cout << s1 << endl;

	s2 = 'x';// y
	cout << s2 << endl;

}

int main()
{
	//test1();
	//test2();
	test_string2();
	return 0;
}