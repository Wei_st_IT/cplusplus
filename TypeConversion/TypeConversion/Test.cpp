#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

//int main()
//{
//	int i = 1;
//	//隐式类型转换 -- 相近类型（意义相近）
//	double d = i;
//	printf("%d, %.2f\n", i, d);
//	int* p = &i;
//	//显示的强制类型转换 -- 不相似类型
//	int address = (int)p;
//	printf("%x, %d\n", p, address);
//	return 0;
//}


class String
{
public:
	void insert(size_t pos, char ch)
	{
		//扩容……
		//挪动数据
		size_t end = _size;
		while (end >= pos)
		{
			_str[end + 1] = _str[end];
			--end;
		}
		//放入插入数据……
	}
private:
	char* _str;
	size_t _size;
	size_t _capacity;
};


//int main()
//{
//	double d = 11.11;
//	int a = static_cast<int>(d);
//	cout << a << endl; //11
//	//int* p = &a;
//	//int x = static_cast<int>(p); // 不是相近类型，无法转化
//	//cout << x << endl;
//	return 0;
//}


//int main()
//{
//	double d = 22.22;
//	int a = static_cast<int>(d);
//	cout << a << endl;
//	int* p = &a;
//	int addr = reinterpret_cast<int>(p);
//	cout << addr << endl;
//	int* pp = reinterpret_cast<int*>(&a);
//	cout << *pp << endl;
//	return 0;
//}


//int main()
//{
//	//const int a = 2;
//	volatile const int a = 2;
//	//int* p = const_cast<int*>(&a);
//	int* p = (int*)&a;
//	*p = 3;
//	cout << a << endl;
//	cout << *p << endl;
//	return 0;
//}


//class A
//{
//public:
//	virtual void f(){}
//};
//class B : public A
//{};
//
//void fun(A* pa)
//{
//
//}
//
//int main()
//{
//	A a;
//	B b;
//	fun(&a);
//	fun(&b);
//	return 0;
//}



class A
{
public:
	virtual void f()
	{}
};
class B : public A
{};
void func(A* pa)
{
	// dynamic_cast会先检查是否能转换成功，能成功则转换，不能则返回
	B* pb1 = (B*)pa;               //不安全
	B* pb2 = dynamic_cast<B*>(pa); //安全

	cout << "pb1: " << pb1 << endl;
	cout << "pb2: " << pb2 << endl;
}
int main()
{
	A a;
	B b;
	func(&a);
	func(&b);
	return 0;
}