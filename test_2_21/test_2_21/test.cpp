#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>

using namespace std;

//class A
//{
//public:
//	int _a;
//};
////class B : public A
//class B : virtual public A
//{
//public:
//	int _b;
//};
////class C : public A
//class C : virtual public A
//{
//public:
//	int _c;
//};
//class D : public B, public C
//{
//public:
//	int _d;
//};
//int main()
//{
//	D d;
//	d._b = 1;
//	d._c = 2;
//	d._d = 3;
//	d.B::_a = 4;
//	d.C::_a = 5;
//	d._a = 6;
//	return 0;
//}

//int main()
//{
//	B b;
//	D d;
//	b._a = 1;
//	b._b = 2;
//	B* ptr = &b;
//	ptr->_a = 10;
//	ptr = &d;
//	ptr->_a = 20;
//	return 0;
//}


//class Person
//{
//public:
//	// 虚函数
//	virtual void BuyTicket()
//	{
//		cout << "Person-买票-全价" << endl;
//	}
//};
//
//// 虚函数的重写/覆盖
//// 三同：函数名、参数、返回值
//// 1.子类虚函数可以不加virtual (建议：父类子类虚函数都加上)
//// 2.协变：三同中，返回值可以不同，但是要求返回值必须是一个父子类关系的指针或者引用
//
//class Student : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "Student-买票-半价" << endl;
//	}
//};
//
//class Soldier : public Person
//{
//public:
//	virtual void BuyTicket()
//	{
//		cout << "Soldier-买票-优先" << endl;
//	}
//};
//
//// 多态的条件
//// 1.虚函数重写
//// 2.父类的指针或者引用去调用虚函数
////void func(Person& People)
////{
////	People.BuyTicket();
////}
//void func(Person* People)
//{
//	People->BuyTicket();
//}

//int main()
//{
//	Person pn;
//	Student st;
//	Soldier sr;
//
//	/*func(pn);
//	func(st);
//	func(sr);*/
//	func(&pn);
//	func(&st);
//	func(&sr);
//}

//class A
//{};
//
//class B : public A
//{};
//
//class Person
//{
//public:
//	// 虚函数
//	virtual A* BuyTicket() { cout << "Person-买票-全价" << endl; return nullptr; }
//};
//
///*协变：三同中，返回值可以不同，但是要求返回值必须是一个父子类关系的指针或者引用
//	当然返回值也可以都是父类的指针或引用，但父类函数的返回值不能是子类的指针或引用*/
//class Student : public Person
//{
//public:
//	virtual B* BuyTicket() { cout << "Student-买票-半价" << endl; return nullptr; }
//};
//
//class Soldier : public Person
//{
//public:
//	virtual A* BuyTicket() { cout << "Soldier-买票-优先" << endl; return nullptr; }
//};
//
//int main()
//{
//	Person p;
//	Student s;
//	Person* ptr = &p;
//	//父类的指针指向父类对象，调用父类的虚函数
//	ptr->BuyTicket();//virtual A* Person::BuyTicket()
//
//	ptr = &s;
//	//父类的指针指向子类对象，调用子类的虚函数
//	ptr->BuyTicket();//virtual B* Student::BuyTicket()
//	return 0;
//}

class Person
{
public:
	 virtual ~Person()
	{
		cout << "Person delete:" << _p << endl;
		delete[] _p;
	}
protected:
	int* _p = new int[10];
};

class Student : public Person
{
public:
	~Student()
	{
		cout << "Student delete:" << _s << endl;
		delete[] _s;
	}
protected:
	int* _s = new int[10];
};

//int main()
//{
//	// 基类、派生类可以不用virtual修饰虚函数构成多态
//	/*Person p; 
//	Student s;*/
//	
//	// 基类必须用virtual修饰虚函数构成多态
//	Person* ptr1 = new Person;
//	Person* ptr2 = new Student;
//	// delete行为：
//	// 1. 使用指针调用析构函数
//	// 2. operator delete(ptr)
//	delete ptr1;
//	delete ptr2;
//
//	return 0;
//}

// 如何构造一个不能被继承的类
// 1. 构造私有：C++98
// 2. 类定义时加上final: C++11
//class A final
//{
//private:
//	//A() {}
//};
//class B : public A
//{};
//
//int main()
//{
//	B b;
//	B* ptr = new B;
//	return 0;
//}