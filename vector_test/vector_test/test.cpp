#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
using namespace std;

void test_vector1()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	
	for (size_t i = 0; i < v.size(); ++i)
	{
		cout << v[i] << "";
	}
	cout << endl;

	vector<int>::iterator it = v.begin();
	while (it != v.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << v.max_size() << endl;
}

void test_vector2()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	cout << v.capacity() << endl;
	cout << v.size() << endl;

	v.push_back(5);
	v.push_back(6);
	v.reserve(10);
	cout << v.capacity() << endl;
	cout << v.size() << endl;
	
	// 比当前容量小时，不缩容
	v.reserve(4);
	cout << v.capacity() << endl;
	cout << v.size() << endl;

	v.resize(8);
	v.resize(15, 1);
	v.resize(3);
}

// 测试vector的默认扩容机制
void TestVectorExpand()
{
	size_t sz;
	vector<int> v;
	sz = v.capacity();
	cout << "making v grow:\n";
	for (int i = 0; i < 100; ++i)
	{
		v.push_back(i);
		if (sz != v.capacity())
		{
			sz = v.capacity();
			cout << "capacity changed: " << sz << '\n';
		}
	}
}

void Func(const vector<int>& v)
{
	v.size();
	v[0]; //[]会使用断言检查
	//v[0]++;
	v.at(0);
}

void vector_test3()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	v.size();
	v[0];
	v[0]++;
	v.at(0);
	Func(v);
}

void test_vector4()
{
	vector<int> v;
	v.reserve(10); // size没有变化
	v.resize(10);
	//v[6];
	for (size_t i = 0; i < v.size(); i++)
	{
		v[i] = i; // assert
		v.at(i) = i; // 抛异常
	}
}

void test_vector5()
{
	vector<int> v;
	v.push_back(1);
	v.push_back(2);
	v.push_back(3);
	v.push_back(4);
	
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	v.assign(10, 1); // 赋值为10个1

	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30);

	v.assign(v1.begin(), v1.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;

	string s("hello world");
	v.assign(++s.begin(), --s.end());
	for (auto e : v)
	{
		cout << e << " ";
	}
	cout << endl;
}

void test_vector6()
{
	vector<int> v1;
	v1.push_back(10);
	v1.push_back(20);
	v1.push_back(30); 
	v1.insert(v1.begin(), 40);
	v1.insert(v1.begin()+2, 50);
	cout << v1.capacity() << endl;
	cout << v1.size() << endl;
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;
	
	// 没有find成员
	//vector<int>::iterator it = v.find(3);

	vector<int>::iterator it = find(v1.begin(), v1.end(), 20);
	if (it != v1.end())
	{
		v1.insert(it, 2);
	}
	for (auto e : v1)
	{
		cout << e << " ";
	}
	cout << endl;

	vector<int> v2;
	swap(v1, v2);
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
	// 不允许缩容
	v2.erase(v2.begin());
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
	v2.erase(v2.begin(), v2.end() - 2);
	for (auto e : v2)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	//test_vector1();
	//test_vector2();
	//TestVectorExpand();
	//test_vector3();
	/*try
	{
		test_vector4();
	}
	catch(const exception& e)
	{
		cout << e.what() << endl;
	}*/
	test_vector5();
	//test_vector6();

	return 0;
}