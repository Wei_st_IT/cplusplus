#define _CRT_SECURE_NO_WARNINGS 1

using namespace std;

#include "BloomFilter.h"

int main()
{
	//BloomFilter_realize::test_BloomFilter1();
	BloomFilter_realize::test_BloomFilter2();

	return 0;
}