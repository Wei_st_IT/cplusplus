#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>

using namespace std;

class Person {
public:
	virtual ~Person()
	{
		cout << "~Person()" << endl;
	}
};
class Student : public Person {
public:
	~Student()
	{
		cout << "~Student()" << endl;
	}
};
int main()
{
	Person* ptr = new Person;
	delete ptr;//~Person()
	ptr = new Student;
	delete ptr;//~Person()
	return 0;
}

//class Car final
//{
//public:
//	virtual void Drive() // final
//	{}
//};
//
//class Benz : public Car
//{
//public:
//	virtual void Drive()
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};

//class Car
//{
//public:
//	virtual void Drive(int)
//	{}
//};
//
//class Benz : public Car
//{
//public:
//	// override是写在子类中的，要求严格检测是否完成重写
//	virtual void Drive() override
//	{
//		cout << "Benz-舒适" << endl;
//	}
//};
//
//int main()
//{
//	return 0;
//}