#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <vector>
#include <queue>
#include <assert.h>
#include <functional>

using namespace std;
#include "priority_queue.h"


// 仿函数
struct Less
{
	bool operator()(int x, int y)
	{
		return x < y;
	}
};

struct Greater
{
	bool operator()(int x, int y)
	{
		return x > y;
	}
};


void test1()
{
	Less less; // Less -> 仿函数类型, less -> 仿函数对象
	cout << less(2, 3) << endl; 
	cout << less.operator()(2, 3) << endl;
	/* 这里我们会以为less是函数名或函数指针，其实并不是，但是它可以像函数一样去调用
	 * 它的本质被转换成less.operator(),它可以像函数一样去使用的对象，
	 * 这是C++为了避免使用C语言中复杂的函数指针，所以提出了仿函数 */
	// 如下同理
	Greater gt; 
	cout << gt(2, 3) << endl;
	cout << gt.operator()(2, 3) << endl;

}


void test2()
{
	priority_queue_realize::Less<int> less;
	cout << less(2, 3) << endl;
	cout << priority_queue_realize::Less<int>()(3, 2) << endl;
	cout << priority_queue_realize::Less<double>()(1.1, 2.3) << endl;
}


void test3()
{
	//priority_queue<Date> pq;
	priority_queue<Date, vector<Date>, greater<Date>> pq;
	pq.push(Date(2022, 12, 18));
	pq.push(Date(2023, 6, 18));
	pq.push(Date(2023, 1, 21));

	while (!pq.empty())
	{
		cout << pq.top();
		pq.pop();
	}
	cout << endl;
}

struct LessPDate
{
	bool operator()(const Date* d1, const Date* d2) const
	{
		// return *d1 < *d2; 
		return (d1->_year < d2->_year) ||
			(d1->_year == d2->_year && d1->_month < d2->_month) ||
			(d1->_year == d2->_year && d1->_month == d2->_month && d1->_day < d2->_day);
	}
};

// 如果数据类型，不支持比较，或者比较的方式，不是你想要的
// 那么可以自己实现仿函数，按照自己想要的方式去比较，控制比较逻辑
void test4()
{
	priority_queue<Date*, vector<Date*>, LessPDate> pq;
	pq.push(new Date(2022, 12, 18));
	pq.push(new Date(2023, 6, 18));
	pq.push(new Date(2023, 1, 21));

	while (!pq.empty())
	{
		cout << *pq.top();
		pq.pop();
	}
	cout << endl;
}

int main()
{
	priority_queue_realize::test();
	//test2();
	//test3();
	//test4();
	return 0;
}