#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <stack>
#include <queue>
#include <list>
#include <vector>
#include <string>
using namespace std;
#include "stack.h"
#include "queue.h"

int main()
{
	stack_realize::test_stack();
	queue_realize::test_queue();
	return 0;
}