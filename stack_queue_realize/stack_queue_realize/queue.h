#pragma once

// 适配器模式

namespace queue_realize
{
	// 函数参数控制的是对象，模板参数控制的是模板
	template<class T, class Container = deque<T>>
	class queue
	{
	public:
		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_back();
		}

		const T& front()
		{
			return _con.front();
		}
		
		const T& back()
		{
			return _con.back();
		}

		bool empty() const
		{
			return _con.empty();
		}

		size_t size() const
		{
			return _con.size();
		}

	private:
		//vector<T> _v; 太死板，有限制
		Container _con;
	};

	void test_queue()
	{
		// 后进先出 - 不支持迭代器
		//queue<int, vector<int>> q; 不能使用vector作适配器，因为vector不不支持头删
		//queue<int, list<int>> q;

		queue<int> q;
		q.push(1);
		q.push(2);
		q.push(3);
		q.push(4);

		cout << q.size() << endl;
		cout << q.back() << endl;

		while (!q.empty())
		{
			cout << q.back() << " ";
			q.pop();
		}
		cout << endl;
	}
}