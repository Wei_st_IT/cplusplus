#pragma once

// 适配器模式

namespace stack_realize
{
	// 函数参数控制的是对象，模板参数控制的是模板
	template<class T, class Container = deque<T>>
	class stack
	{
	public:
		void push(const T& val)
		{
			_con.push_back(val);
		}

		void pop()
		{
			_con.pop_back();
		}

		bool empty() const
		{
			return _con.empty();
		}

		const T& top() const
		{
			return _con.back();
		}
		 
		size_t size() const 
		{
			return _con.size();
		}

	private:
		//vector<T> _v; 太死板，有限制
		Container _con;
	};

	void test_stack()
	{
		// 后进先出 - 不支持迭代器
		stack<int, vector<int>> s;
		//stack<int, list<int>> s;
		/*stack<int, string> s;
		stack<string, string> st;
		st.push("1111");*/
		
		//stack<int> s;
		s.push(1);
		s.push(2);
		s.push(3);
		s.push(4);

		while (!s.empty())
		{
			cout << s.top() << " ";
			s.pop();
		}
		cout << endl;
	}
}