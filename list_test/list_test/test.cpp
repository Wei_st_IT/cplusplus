#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
#include <cassert>
using namespace std;

void list_test1()
{
	list<int> lt;
	lt.push_back(1);
	lt.push_back(2);
	lt.push_back(3);
	lt.push_back(4);

	list<int>::iterator it = lt.begin();
	while (it != lt.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	list<int>::reverse_iterator rit = lt.rbegin();
	while (rit != lt.rend())
	{
		cout << *rit << " ";
		rit++;
	}
	cout << endl;

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	lt.pop_back();
	lt.pop_back();
	lt.pop_back();
	lt.pop_back();
	lt.pop_back();
}

void list_test2()
{
	list<int> lt;
	lt.push_front(10);
	lt.push_front(20);
	lt.push_front(30);
	lt.push_front(40);

	 list<int>::iterator pos = find(lt.begin(), lt.end(), 30);
	if (pos != lt.end())
	{
		// insert之后，迭代器是否失效?
		lt.insert(pos, 3);
	}
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	cout << *pos << endl;
	(*pos)++;
	cout << endl;

	// 删除导致迭代器失效 ：形成野指针
	lt.erase(pos);
	//cout << *pos << endl;

	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void list_test3()
{
	list<int> lt;
	lt.push_back(2);
	lt.push_back(23);
	lt.push_back(6);
	lt.push_back(14);
	lt.push_back(10);
	lt.push_back(100);
	
	lt.remove(100);
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
	// list 双向循环链表提供
	lt.sort();
	// algorithm 算法库提供
	//sort(lt.begin(), lt.end()); 报错
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;

	//迭代器功能分类
	// 1.单向 ++
	// 2.双向 --
	// 3.随机 ++ -- + - ——>vector、string

	//先排序，再去重
	lt.unique();
	for (auto e : lt)
	{
		cout << e << " ";
	}
	cout << endl;
}

void list_test4()
{
	list<int> lt1;
	lt1.push_back(10);
	lt1.push_back(20);
	lt1.push_back(30);

	list<int> lt2;
	lt2.push_back(1);
	lt2.push_back(2);
	lt2.push_back(3);
	lt2.push_back(4);

	auto it = lt2.begin();
	++it; // 迭代器下标为2
	lt2.splice(it, lt1); // 把lt1转移到lt2迭代器下标为2
	for (auto e : lt2)
	{
		cout << e << " ";
	}
	cout << endl;
}

int main()
{
	//list_test1();
	//list_test2();
	//list_test3();
	list_test4();
	return 0;
}