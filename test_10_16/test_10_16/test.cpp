#define _CRT_SECURE_NO_WARNINGS 1
#include <stdlib.h>
#include <assert.h>
#include <iostream>
using namespace std;

//class N
//{
//public:
//	void Print()
//	{
//		_a = 10;
//		cout << _a << endl;
//	}
//	int _a;
//};
//
//int main()
//{
//	/*N* pn = nullptr;
//	pn->Print();*/
//	N n;
//	n.Print();
//	return 0;
//}

//class A
//{
//public:
//	A()
//	{
//		_a = 0;
//		cout << "A()构造函数" << endl;
//	}
//private:
//	int _a;
//};

//class Date
//{
//public:
//	/*void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}*/
//
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	
//	/*Date()
//	{
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}*/
//		
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	//内置类型
//	int _year;
//	int _month;
//	int _day;
//
//	//自定义类型
//	//A _aa;
//};
//
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		cout << "~Stack(int capacity = 4)" << endl;
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail");
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//	}
//	~Stack()
//	{
//		cout << "~Stack" << endl;
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//	void Push(int x)
//			{
//				// 扩容
//				_a[_top] = x;
//				_top++;
//			}
//			
//private:
//	int* _a;
//	int _capacity;
//	int _top;
//};

class Stack
{
public:
	//构造函数
	Stack(int capacity = 10)
	{
		_a = (int*)malloc(sizeof(int) * capacity);
		assert(_a);
		_top = 0;
		_capacity = capacity;
	}
	//析构函数
	~Stack()
	{
		cout << "~Stack():" << this << endl;
		free(_a);
		_a = nullptr;
		_top = _capacity = 0;
	}
	void Push(int x)
	{// 扩容
		_a[_top] = x;
		_top++;
	}
private:
	int* _a;
	int _top;
	int _capacity;

};
class MyQueue
{
public:
	void Push(int x)
	{
		_pushST.Push(x);
	}

	Stack _pushST;
	Stack _popST;
};


int main()
{
	/*Date d1(2011, 10, 10);
	d1.Init(2022, 10, 10);
	d1.Print();

	Date d2;
	d2.Init(2022, 10, 10);
	d2.Print();*/

	Stack st1;// 经常忘记Init,Destroy
	Stack st2;
	/*st.Push(1);
	st.Push(2);
	st.Push(3);*/

	//Date d1(2022, 10, 10);
	//Date d2(2022, 10, 11);
	//Date d3;
	//d1.Print();
	//d2.Print();
	//d3.Print();

	//MyQueue mq;
	//mq.Push(1);
	//mq.Push(2);
	return 0;
}
	/*void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}*/

//class Date
//{
//public:
//	//无参构造函数
//	Date
//	{
//		_year = 1;
//		_month = 1;
//		_day = 1;
//	}
//	//有参构造函数
//	Date(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	//内置类型
//	int _year;
//	int _month;
//	int _day;
//};
//
//int main()
//{
//	Date d1(2022, 10, 10);
//	d1.Print();
//	Date d2;
//	d2.Print();
//	return 0;
//}