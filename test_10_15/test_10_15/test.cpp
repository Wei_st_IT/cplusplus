#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <chrono>
using namespace std;

//class A
//{
//public:
//	void PrintA()
//	{
//		cout << _a << endl;
//	}
//private:
//	char _a;
//};
//
//class B
//{
//	void PrintB()
//	{
//		printf("PrintA");
//	}
//};
//
//class C
//{};
//
//int main()
//{
//	cout << sizeof(B) << endl;
//	cout << sizeof(C) << endl;
//}

//class Date
//{
//public:
//	//Print处理前
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//
//	//Print处理后
//	void Print(Data* const this) //注意const修饰，指针不能修改，指向的内容可以修改
//	{
//		cout << this->_year << "-" << this->_month << "-" << this -> _day << endl;
//	}
//
//	//Init处理前
//	void Init(int year, int month, int day)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	//Init处理后
//	void Init(Date* const this, int year, int month, int day)
//	{
//		this->_year = year;
//		this->_month = month;
//		this->_day = day;
//	}
//private:
//	int _year; // 年
//	int _month; // 月
//	int _day; // 日
//};
//int main()
//{
//	Date d1;
//	Date d2;
//	Date d3;
//	d1.Init(2022, 5, 16);
//	d2.Init(2022, 5, 17);
//
//	//实参改变
//	d1.Init(&d1, 2022, 5, 16);
//	d2.Init(&d2, 2022, 5, 17);
//
//	d1.Print();
//	d2.Print();
//
//	//实参改变
//	d1.Print(&d1);
//	d2.Print(&d2);
//
//	return 0;
//}


class A
{
public:
	void Print()
	{
		cout << this->_a << endl;
	}
	//private:
	int _a;
};

int main()
{
	A* p = nullptr;
	p->Print();
	p->_a;
	return 0;
}

//class A
//{
//public:
//	void Print(A* const this)
//	{
//		cout << this->_a << endl;
//	}
////private:
//	int _a;
//};
//
//int main()
//{
//	A* p = nullptr;
//	p->Print(p);
//	p->_a;
//	return 0;
//}