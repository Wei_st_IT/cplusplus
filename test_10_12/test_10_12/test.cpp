#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

//typedef char* pc;
//int main()
//{
//    //能否编译成功？
//    const pc p1;//失败
//    const pc* p2;//成功
//    return 0;
//}

//int TestAuto()
//{
//	return 10;
//}

using namespace std;
//int main()
//{
//	int a = 10;
//	auto b = a;
//	auto c = 'a';
//	auto d = TestAuto();
//	//auto e;  //无法通过编译，使用auto定义变量时必须对其进行初始化
//	cout << typeid(b).name() << endl;
//	cout << typeid(c).name() << endl;
//	cout << typeid(d).name() << endl;
//	return 0;
//}
//
//int main()
//{
//	int x = 10;
//	auto a = &x;
//	auto* b = &x;
//	auto& c = x;
//	cout << typeid(a).name() << endl; // int*
//	cout << typeid(b).name() << endl; // int*
//	cout << typeid(c).name() << endl; // int
//	*a = 20;
//	*b = 30;
//	c = 40;
//	return 0;
//}
//
//void TestAuto()
//{
//	auto a = 1, b = 2;
//	auto c = 3, d = 4.0; // 该行代码会编译失败，因为c和d的初始化表达式类型不同
//}

// 此处代码编译失败，auto不能作为形参类型，因为编译器无法对a的实际类型进行推导
//void TestAuto(auto a)
//{}

//void TestAuto()
//{
//	int a[] = { 1, 2, 3 };
//	auto b[] = { 4, 5, 6 }; //error 错误
//}

//auto Test()
//{
//	return 10; 
//}
//
//int main()
//{
//	return 0;
//}

//void TestFor()
//{
//	int array[] = { 1, 2, 3, 4, 5 };
//	for (int i = 0; i < sizeof(array) / sizeof(int); ++i)
//		array[i] *= 2;
//	for (auto e : array)
//		cout << e << " "; // 2 4 6 8 10
//	cout << endl;
//	for (auto& e : array)
//		e /= 2;
//	for (auto e : array)
//		cout << e << " "; //1 2 3 4 5
//}
//
//int main()
//{
//	TestFor();
//	return 0;
//}

//void f(int)
//{
//	cout << "f(int)" << endl;
//}
//
//void f(int*)
//{
//	cout << "f(int*)" << endl;
//}
//
//int main()
//{
//	f(0);
//	f(NULL);
//	f((int*)NULL);
//	f(nullptr);
//	return 0;
//}


#include "Add.h"

int main()
{
	int ret = Add(1, 2);
	cout << ret << endl;
	return 0;
}