#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

#include "List.h"
#include "string.h"

//lambda表达式
void test_lambda1()
{

	//int a = 0, b = 1;
	//auto Add1 = [](int x, int y)->int { return x + y; };
	//cout << Add1(a, b) << endl; // 1

	//int a = 0, b = 1;
	////返回值类型明确的情况下，返回值类型可以省略掉，由编译器自动推导
	//auto Add2 = [](int x, int y) { return x + y; };
	//cout << Add2(a, b) << endl;//7
	

	//int a = 40, b = 60;
	//auto Add1 = [](int x, int y)->int { return (x + y) / 3.0; };
	//cout << Add1(a, b) << endl;//33
	////传值捕捉
	//auto Add2 = [a, b] { return (a + b) / 3.0; };
	//cout << Add2() << endl;//33.3333

}


void test_lambda2()
{
	int a = 1, b = 2;
	auto add2 = [b](int x) {return x + b; };
	cout << add2(3) << endl; //5

	//auto swap1 = [](int& x, int& y) // 传引用
	//{
	//	int tmp = x;
	//	x = y;
	//	y = tmp;
	//};
	//swap1(a, b);
	//cout << a << " " << b << endl;//2 1

	//以引用的方式捕捉
	// auto swap2 = [&]()
	//auto swap2 = [&a, &b]()
	//{
	//	int tmp = a;
	//	a = b;
	//	b = tmp;
	//};
	//swap2();
	//cout << a << " " << b << endl;//2 1

	// 混合捕捉
	int x = 0, y = 1;
	auto func1 = [x, &y]()
	{};

	func1();

	auto func2 = [=, &y]()
	{
		//cout << m << endl; 无法捕捉它后面的变量
		cout << x << endl;
	};

	func2();

	int m = 0, n = 1;
}

class Rate
{
public:
	Rate(double rate) : _rate(rate)
	{}

	double operator()(double money, int year)
	{
		return money * _rate * year;
	}

private:
	double _rate;
};


//int main()
//{
//	//test_lambda1();
//	//test_lambda2();
//
//	// 函数对象
//	double rate = 0.49;
//	Rate r1(rate);
//	r1(10000, 2);
//
//	// lambda
//	auto r2 = [=](double monty, int year)->double {return monty * rate * year; };
//	r2(10000, 2);
//
//	auto r3 = [=](double monty, int year)->double {return monty * rate * year; };
//	r3(10000, 2);
//
//	return 0;
//}

#include <thread>

void Print1(int& x)
{
	for (; x < 100; ++x)
	{
		cout << "thread1:" << x << endl;
	}
}

void Print2(int& y)
{
	for (; y < 100; ++y)
	{
		cout << "thread2:" << y << endl;
	}
}

//int main()
//{
//	//int i = 0;
//	//thread t1(Print1, ref(i));
//	//thread t2(Print2, ref(i));
//
//	//t1.join();
//	//t2.join();
//
//	//cout << "XXXXXXXXXXXXXXXXXXX" << endl;
//	//return 0;
//
//////////////我是分割线//////////////////
//	////N个线程同时走
//	//int i = 0;
//	//thread t1([&i]()
//	//	{
//	//		for (; i < 100; ++i)
//	//		{
//	//			cout << "thread1:" << i << endl;
//	//		}
//	//	});
//
//	//thread t2([&i]()
//	//	{
//	//		for (; i < 100; ++i)
//	//		{
//	//			cout << "thread2:" << i << endl;
//	//		}
//	//	});
//
//	//// t1 = t2;
//
//	//t1.join();
//	//t2.join();
//
//	//cout << "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << endl;
//	//return 0;
//////////////我是分割线//////////////////
//
//	vector<thread> vThreads;
//	int n;
//	cin >> n;
//	vThreads.resize(n);
//
//	int i = 0;
//	int x = 0;
//	for (auto& t : vThreads)
//	{
//		t = thread([&i, x]
//			{
//				while (i < 1000)
//				{
//					cout << "thread" << x << "->" << i << endl;
//					++i;
//				}
//			});
//		x++;
//	}
//
//	for (auto& t : vThreads)
//	{
//		t.join();
//	}
//
//	return 0;
//}


//int main()
//{
//	int a = 10, b = 20;
//	//auto swap1 = [](int x, int y) {
//	auto swap1 = [a, b]()mutable {
//		int tmp = a;
//		a = b;
//		b = tmp;
//	};
//	swap1();
//	//swap1(100, 200);
//	cout << a << " " << b << endl;//20 10
//	return 0;
//}

/////////////////////////我是分割线////////////////////////////
// 右值引用

void cpp11_test4()
{
	//以下p、b、c、*p都是左值
	int* p = new int(0);
	int b = 1;
	const int c = 2;

	//以下几个是对上面左值的左值引用
	int*& rp = p;
	int& rb = b;
	const int& rc = c;
	int& pvalue = *p;

	cout << p << ":" << rp << endl;
	cout << b << ":" << rb << endl;
	cout << c << ":" << rc << endl;
	cout << *p << ":" << pvalue << endl;
}

void cpp11_test5()
{
	//double x = 1.1, y = 2.2;
	//// 以下几个都是常见的右值
	//10;//字面常量
	//x + y;//表达式返回值
	//fmin(x, y);//函数返回值（传值返回）

	//// 以下几个都是对右值的右值引用
	//int&& rr1 = 10;
	//double&& rr2 = x + y;
	//double&& rr3 = fmin(x, y);


	//这里编译会报错：error C2106: “=”: 左操作数必须为左值
	/*10 = 1;
	x + y = 1;
	fmin(x, y) = 1;*/


	/*
	这里编译会报错，右值不能取地址
	cout << &10 << endl;
	cout << &(x + y) << endl;
	cout << &fmin(x, y) << endl;
	*/

	double x = 1.1, y = 2.2;
	int&& rr1 = 10;
	const double&& rr2 = x + y;
	rr1 = 20;
	//rr2 = 5.5; // 报错

}

//void cpp11_test6()
//{
//	int a = 10;
//	int& ra1 = a;
//	// int& ra2 = 10;
//	
//	const int b = 20;
//	const int& rb = b;
//	const int& rb2 = 20;
//
//	int c = 10;
//	int&& rc = 10;
//	// int&& rc = c;
//	int&& rc2 = move(c);
//
//}
//
//int main()
//{
//	cpp11_test6();
//	return 0;
//}


//void func1(cpp::string s)
//{}
//void func2(const cpp::string& s)
//{}
//int main()
//{
//	//cpp::string s1("hello");
//	//func1(s1);//值传参
//	//func2(s1);//传引用传参
//
//	//// string operator+=(char ch) 传值返回存在深拷贝
//	//// string& operator+=(char ch) 传左值引用没有拷贝提高了效率
//	//s1 += '!';//左值引用作为返回值
//
//	//cpp::string ret = cpp::to_string(1234);
//
//	//cpp::string str;
//	////...
//	//str = cpp::to_string(1234);
//
//	cpp::string ret;//string(string&& s) -- 移动构造，资源转移
//	ret = cpp::to_string(1234);//string& operator=(string&& s) -- 移动赋值，资源转换
//	
//	cpp::string ret = cpp::to_string(1234); //string(string&& s) --移动构造，资源转移
//
//	cpp::string ret;
//	ret = cpp::to_string(1234);
//
//	return 0;
//}



void Fun(int& x) { cout << "左值引用" << endl; }
void Fun(const int& x) { cout << "const 左值引用" << endl; }
void Fun(int&& x) { cout << "右值引用" << endl; }
void Fun(const int&& x) { cout << "const 右值引用" << endl; }

//template<typename T>
//void PerfectForward(T&& t)
//{
//	Fun(t);
//}

//template<typename T>
//void PerfectForward(T&& t)
//{
//	//完美转发
//	Fun(std::forward<T>(t));
//	//std::forward<T>(t)在传参的过程中保持了t的原生类型属性。
//}

//int main()
//{
//	//PerfectForward(10);//右值
//	//int a;
//	//PerfectForward(a);//左值
//	//PerfectForward(std::move(a));//右值
//	//const int b = 8;
//	//PerfectForward(b);//const左值
//	//PerfectForward(std::move(b));//const右值
//
//	cpp::list<cpp::string> lt;
//	cpp::string s1("1111");//右值
//	lt.push_back(s1);//左值
//	lt.push_back("2222");//右值
//	lt.push_back(std::move(s1));//右值
//
//	return 0;
//}


// 以下代码在vs2013中不能体现，在vs2019及以上的编译器下才能演示体现上面的特性。
//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p)
//		:_name(p._name)
//		, _age(p._age)
//	{}
//	/*Person& operator=(const Person& p)
//	{
//		if (this != &p)
//		{
//			_name = p._name;
//			_age = p._age;
//		}
//		return *this;
//	}*/
//	/*~Person()
//	{}*/
//private:
//	cpp::string _name;
//	int _age;
//};

//class Person
//{
//public:
//	Person(const char* name = "", int age = 0)
//		:_name(name)
//		, _age(age)
//	{}
//	Person(const Person& p) = delete;
//
//private:
//	cpp::string _name;
//	int _age;
//};
//
//int main()
//{
//	Person s1;
//	Person s2 = s1;//拷贝构造
//	Person s3 = std::move(s1);//移动构造
//
//	return 0;
//}

