#define _CRT_SECURE_NO_WARNINGS 1
#include <string>
#include <iostream>
#include <list>
#include <functional>
#include <map>
using namespace std;

//template <class ...Args>
//void ShowList(Args... args)
//{
//	// cout << sizeof(args) << endl; 错误
//	cout << sizeof...(args) << endl;// 正确写法
//	
//	/*for (size_t i = 0; i < sizeof...(args); i++)
//	{
//		cout << args[i] << endl;
//	}*/ // 错误写法
//}

//void ShowList()
//{
//	cout << endl;
//}
//
//template <class T, class...Args>
//void ShowList(T val, Args...args)
//{
//	cout << val << " ";
//	ShowList(args...);
//}


//template <class T>
//void PrintArg(T t)
//{
//	cout << t << " ";
//}

////展开函数
//template <class ...Args>
//void ShowList(Args... args)
//{
//	int arr[] = { (PrintArg(args), 0)...};
//	cout << endl;
//}
//
//int main()
//{
//	ShowList(1);
//	ShowList(1, 1.1);
//	ShowList(1, 1.1, string("######"));
//	// ShowList();
//	return 0;
//}

//int main()
//{
//	std::list<int> list1;
//	list1.push_back(1);
//	list1.emplace_back(2);
//	//list1.emplace_back(1, 2, 3); 不允许
//	list1.emplace_back();
//	for (auto e : list1)
//	{
//		cout << e << " ";
//	}
//	cout << endl;
//	return 0;
//}


//
//template <class F, class T>
//T useF(F f, T x)
//{
//	static int count = 0;
//	cout << "count:" << ++count << endl;
//	cout << "count:" << &count << endl;
//	return f(x);
//}
//
//double f(double i)
//{
//	return i / 2;
//}
//
//struct Functor
//{
//	double operator()(double d)
//	{
//		return d / 3;
//	}
//};


//int main()
//{
//	////函数名
//	//cout << useF(f, 2.2) << endl;
//	////函数对象
//	//cout << useF(Functor(), 2.2) << endl;
//	////lambda表达式
//	//cout << useF([](double d)->double{ return d / 4; }, 2.2) << endl;
//	// 函数名
//	std::function<double(double)> func1 = f;
//	cout << useF(func1, 11.11) << endl;
//	// 函数对象
//	std::function<double(double)> func2 = Functor();
//	cout << useF(func2, 11.11) << endl;
//	// lamber表达式
//	std::function<double(double)> func3 = [](double d)->double { return d / 4; };
//	cout << useF(func3, 11.11) << endl;
//	return 0;
//}

//int f(int a, int b)
//{
//	return a + b;
//}
//struct Functor
//{
//public:
//	int operator() (int a, int b)
//	{
//		return a + b;
//	}
//};
//class Plus
//{
//public:
//	static int plusi(int a, int b)
//	{
//		return a + b;
//	}
//	double plusd(double a, double b)
//	{
//		return a + b;
//	}
//};
// 

//int main()
//{
//	// 函数名(函数指针)
//	std::function<int(int, int)> func1 = f;
//	cout << func1(1, 2) << endl;//3
//	// 仿函数
//	std::function<int(int, int)> func2 = Functor();
//	cout << func2(1, 2) << endl;//3
//	// lambda表达式
//	std::function<int(int, int)> func3 = [](int a, int b) {return a + b; };
//	cout << func3(1, 2) << endl;//3
//	//类的静态成员函数
//	std::function<int(int, int)> func4 = Plus::plusi;//非静态成员函数必须加&，静态可不加
//	cout << func4(1, 2) << endl;//3
//	//类的非静态成员函数
//	std::function<double(Plus, double, double)> func5 = &Plus::plusd;//非静态成员函数必须加&，静态可不加
//	cout << func5(Plus(), 1.1, 2.2) << endl;//3.3
//	return 0;
//}


////带参的终止函数
//template <class T>
//void ShowList(const T& val)
//{
//	cout << val << "->" << typeid(val).name() << " end" << endl;
//}
////递归函数
//template <class T, class ...Args>
//void ShowList(const T& val, Args... args)
//{
//	cout << sizeof...(args) << endl;//获取参数包中参数的个数
//	cout << val << "->" << typeid(val).name() << endl;
//	ShowList(args...);
//}
//int main()
//{
//	ShowList(1, 'x', 1.1);
//	return 0;
//}


////无参的终止函数
//void ShowList()
//{}
////递归函数
//template <class T, class ...Args>
//void ShowList(const T& val, Args... args)
//{
//	cout << sizeof...(args) << endl;//获取参数包中参数的个数
//	cout << val << "->" << typeid(val).name() << endl;
//	ShowList(args...);
//}
//int main()
//{
//	ShowList(1, 'x', 1.1);
//	cout << endl;
//	ShowList(1, 2, 3, 4, 5);
//	cout << endl;
//	ShowList();
//	return 0;
//}


//template <class T>
//void PrintArg(const T& t)
//{
//	cout << t << " ";
//}
//template <class ...Args>
//void ShowList(Args... args)
//{
//	//列表初始化+逗号表达式
//	int arr[] = { (PrintArg(args), 0)... };
//	cout << endl;
//}
//
//int main()
//{
//	ShowList(1);
//	ShowList(1, 1.1);
//	ShowList(1, 1.1, string("######"));
//	return 0;
//}
int f(int a, int b)
{
	return a + b;
}
struct Functor
{
public:
	int operator() (int a, int b)
	{
		return a + b;
	}
};
class Plus
{
public:
	Plus(int x = 2)
		:_x(x)
	{}
	int plusi(int a, int b)
	{
		return (a + b) * _x;
	}
private:
	int _x;
};

int main()
{
	map<string, std::function<int(int, int)>> opFuncMap =
	{
		{"普通函数指针", f },
		{"函数对象", Functor()},
		{"成员函数指针", &Plus::plusi}
	};
	return 0;
}