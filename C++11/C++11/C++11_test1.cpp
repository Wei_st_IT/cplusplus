#define _CRT_SECURE_NO_WARNINGS 1
#include <initializer_list>
#include <iostream>
#include <array>
using namespace std;

struct Point
{
	int _x;
	int _y;
};

void cpp11_test1()
{
	int array1[3] = { 1, 2, 3 };
	Point p = { 1, 2 };

	int x1 = 1;
	int x2{ 1 };
	int x3 = { 3 };
	int array2[3]{ 3, 4, 5 };

	int* p1 = new int(1);
	int* p2 = new int[4] {1, 2, 3, 4};

}

void cpp11_test2()
{
	/*std::initializer_list<int> ilt = { 1, 2, 3 };
	std::initializer_list<int>::iterator it = ilt.begin();
	while (it != ilt.end())
	{
		cout << *it << " ";
		it++;
	}
	cout << endl;

	for (auto e : ilt)
	{
		cout << e << " ";
	}
	cout << endl;*/

	auto il = { 10, 20, 30 };
	cout << typeid(il).name() << endl;

}

//// decltype的一些使用使用场景
//template<class T1, class T2>
//void F(T1 t1, T2 t2)
//{
//	decltype(t1 * t2) ret;
//	cout << typeid(ret).name() << endl;
//}
//int main()
//{
//	const int x = 1;
//	double y = 2.2;
//	decltype(x * y) ret; // ret的类型是double
//	decltype(&x) p; // p的类型是int*
//	cout << typeid(ret).name() << endl;//int const * __ptr64
//	cout << typeid(p).name() << endl;//int
//	F(1, 'a');
//	return 0;
//}

void cpp11_test3()
{
	std::array<int, 10> a1 = {1, 2, 3, 4, 5};
	std::array<double, 20> a2{1.1, 2.2, 3.3, 4.4};
	//cout << a1[12] << " "; // 检查越界读
	int a[10];
	//cout << a[12] << " "; // 抽查越界

	//a2[24] = 22.2;
	//a[13] = 22;
}

//int main()
//{
//	//cpp11_test1();
//	//cpp11_test2();
//	cpp11_test3();
//	return 0;
//}