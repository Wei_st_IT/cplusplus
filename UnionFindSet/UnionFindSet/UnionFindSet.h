#pragma once
#include <vector>
#include <iostream>
#include <string>
#include <unordered_map>
////并查集
//template <class T>
//class UnionFindSet {
//public:
//	//构造函数
//	UnionFindSet(int n)
//		:_ufs(n, -1) //初始时各个元素自成一个集合
//	{}
//	//查找元素所在的集合
//	int findRoot(int x)
//	{
//		while (_ufs[x] >= 0)
//		{
//			x = _ufs[x];
//		}
//		return x;
//	}
//	////递归
//	//int findRoot(int x)
//	//{
//	//	return x >= _ufs[x] ? findRoot(_ufs[x]) : 0;
//	//}
//
//	//路径压缩
//	int findRoot(int x)
//	{
//		int root = x;
//		while (_ufs[root] >= 0)
//		{
//			root = _ufs[root];
//		}
//
//		while (_ufs[x] >= 0)
//		{
//			x = _ufs[x];
//			_ufs[x] = root;
//		}
//		return root;
//	}
//
//	//递归
//	int findRoot(int x)
//	{
//		if (_ufs[x] >= 0)
//		{
//			x = findRoot(_ufs[x]);
//		}
//		return x;
//	}
//
//	//判断两个元素是否在同一个集合
//	bool inSameSet(int x1, int x2)
//	{
//		return findRoot(x1) == findRoot(x2);
//	}
//
//	//合并两个元素所在的集合
//	bool unionSet(int x1, int x2)
//	{
//		int root1 = findRoot(x1);
//		int root2 = findRoot(x2);
//
//		if (root1 == root2)
//			return false;
//
//		if (_ufs[root1]> _ufs[root2])
//			swap(root1, root2);
//		
//		_ufs[root1] += _ufs[root2];
//		_ufs[root2] = root1;
//		return true;
//	}
//
//	//获取并查集中集合的个数
//	int getNum()
//	{
//		int count = 0;
//		for (auto e : _ufs) {
//			if (e < 0)
//				count++;
//		}
//		return count;
//	}
//private:
//	vector<int> _ufs; //维护各个结点之间的关系
//};

namespace UnionFindSet
{
	//并查集
	template<class T>
	class UnionFindSet {
	public:
		//构造函数
		UnionFindSet(const vector<T>& v)
			:_ufs(v.size(), -1) //初始时各个元素自成一个集合
		{
			//建立元素与数组下标之间的映射关系
			for (int i = 0; i < v.size(); i++) {
				_indexMap[v[i]] = i;
			}
		}
		//查找元素所在集合的根结点（迭代）
		int findRoot(const T& x) {
			int parent = _indexMap[x]; //默认当前结点就是根结点
			while (_ufs[parent] >= 0) { //当前结点值不是负数则继续向上找
				parent = _ufs[parent];
			}
			return parent; //返回根结点
		}
		//合并两个元素所在的集合
		bool unionSet(const T& x1, const T& x2) {
			int parent1 = findRoot(x1), parent2 = findRoot(x2); //分别找到两个元素所在集合的根结点
			if (parent1 == parent2) //两个结点已经位于同一集合
				return false;
			if (_ufs[parent1] > _ufs[parent2]) //让parent1标记大集合根结点，parent2标记小集合根结点
				swap(parent1, parent2);
			//将小集合合并到大集合上
			_ufs[parent1] += _ufs[parent2]; //将小集合根结点的值累加到大集合的根结点上
			_ufs[parent2] = parent1; //将小集合根结点的值改为大集合根结点的编号
			return true;
		}
		//获取并查集中集合的个数
		int getNum() {
			int count = 0; //统计根结点的个数
			for (const int& val : _ufs) {
				if (val < 0) //元素值为负数则为根结点
					count++;
			}
			return count; //返回根结点的个数
		}
	private:
		vector<int> _ufs; //维护各个结点之间的关系
		unordered_map<T, int> _indexMap; //建立元素与数组下标之间的映射关系
	};

	void testUFS()
	{
		vector<string> v = { "张三", "李四", "王五", "赵六", "田七", "周八", "吴九" };
		UnionFindSet<string> ufs(v);
		cout << ufs.getNum() << endl; //7

		ufs.unionSet("张三", "李四");
		ufs.unionSet("王五", "赵六");
		cout << ufs.getNum() << endl; //5

		ufs.unionSet("张三", "赵六");
		cout << ufs.getNum() << endl; //4
	}
}