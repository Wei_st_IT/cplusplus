#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <list>
#include <vector>
#include <algorithm>

using namespace std;

#include "list.h"

int main()
{
	//list_realize::list_test1();
	list_realize::list_test2();
	list_realize::list_test3();
	list_realize::list_test4();
	list_realize::list_test5();

	return 0;
}

