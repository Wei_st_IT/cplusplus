#pragma once

namespace iterator_realize
{
	//// 普通迭代器
	//template<class T>
	//struct __list_iterator
	//{
	//	typedef List_node<T> node;
	//	node* _pnode;

	//	// 构造函数
	//	__list_iterator(node* p)
	//		:_pnode(p)
	//	{}

	//	T& operator*()
	//	{
	//		return _pnode->_data;
	//	}

	//	T* operator->()
	//	{
	//		return &_pnode->_data;
	//		return &(operator*());//返回结点指针所指结点的数据的地址
	//	}

	//	__list_iterator<T>& operator++()
	//	{
	//		_pnode = _pnode->_next;
	//		return *this;
	//	}

	//	__list_iterator<T> operator++(int)
	//	{
	//		__list_iterator tmp(*this);
	//		_pnode = _pnode->_next;
	//		return tmp;
	//	}

	//	__list_iterator<T>& operator--()
	//	{
	//		_pnode = _pnode->_prev;
	//		return *this;
	//	}

	//	__list_iterator<T> operator--(int)
	//	{
	//		__list_iterator tmp(*this);
	//		_pnode = _pnode->_prev;
	//		return tmp;
	//	}

	//	bool operator==(const __list_iterator<T>& it)
	//	{
	//		return _pnode == it._pnode;
	//	}

	//	bool operator!=(const __list_iterator<T>& it)
	//	{
	//		return _pnode != it._pnode;
	//	}
	//};

	// //const迭代器 跟普通迭代器的区别：遍历，不能用*it修改数据
	//template<class T>
	//struct __list_const_iterator
	//{
	//	typedef list_node<T> node;
	//	node* _pnode;

	//	__list_const_iterator(node* p)
	//		:_pnode(p)
	//	{}

	//	const T& operator*()
	//	{
	//		return _pnode->_data;
	//	}

	//	__list_const_iterator<T>& operator++()
	//	{
	//		_pnode = _pnode->_next;
	//		return *this;
	//	}

	//	__list_const_iterator<T> operator++(int)
	//	{
	//		__list_const_iterator tmp(*this);
	//		_pnode = _pnode->_next;
	//		return tmp;
	//	}

	//	__list_const_iterator<T>& operator--()
	//	{
	//		_pnode = _pnode->_prev;
	//		return *this;
	//	}

	//	__list_const_iterator<T> operator--(int)
	//	{
	//		__list_const_iterator tmp(*this);
	//		_pnode = _pnode->_prev;
	//		return tmp;
	//	}

	//	bool operator!=(const __list_const_iterator<T>& it)
	//	{
	//		return _pnode != it._pnode;
	//	}
	//};

	// SGI STL库的迭代器 ——> 大佬的写法
	template<class T, class Ref, class Ptr>
	struct __list_iterator
	{
		typedef List_node<T> node;
		typedef __list_iterator<T, Ref, Ptr> Self;
		node* _pnode;

		__list_iterator(node* p)
			:_pnode(p)
		{}

		/*
		 *it2 = it1 浅拷贝
		 * 拷贝构造和负值重载是否需要我们自己实现
		 * 析构函数呢？ 迭代器是借助节点的指针访问修改链表
		 * 节点属于链表，不属于迭代器，所以它不负责释放
		 * 拷贝构造、负值重载我们都不需要实现，默认生成的即可
		*/

		Ref operator*()
		{
			return _pnode->_data;
		}

		Ptr operator->()
		{
			return &_pnode->_data;
			return &(operator*());
		}

		// ++it
		Self& operator++()
		{
			_pnode = _pnode->_next;
			return *this;
		}

		// it++
		Self operator++(int)
		{
			Self tmp(*this);
			_pnode = _pnode->_next;
			return tmp;
		}
		// ++it
		Self& operator--()
		{
			_pnode = _pnode->_prev;
			return *this;
		}
		// it--
		Self operator--(int)
		{
			Self tmp(*this);
			_pnode = _pnode->_prev;
			return tmp;
		}

		bool operator!=(const Self& it) const
		{
			return _pnode != it._pnode;
		}

		bool operator==(const Self& it) const
		{
			return _pnode == it._pnode;
		}
	};
}
