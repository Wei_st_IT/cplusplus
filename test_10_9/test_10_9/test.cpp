//#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
using namespace std;
//
//void swap(int& m, int& n)
//{
//	int tmp = m;
//	m = n;
//	n = tmp;
//}
//
//typedef struct ListNode
//{
//	struct ListNode* next;
//	int val;
//}LTNode, *PLTNode;
//
//void listPushBack(LTNode** pphead, int x)
//{}
//void listPushBack(PLTNode& phead, int x)
//{}
//
//int main()
//{
//	int a = 10;
//	int& ra = a; // 引用
//	int& x = a;
//	int& y = x;
//
//	x++, y++, a++;
//	
//	cout << x << endl;//均为13
//	cout << y << endl;
//	cout << a << endl;
//	
//	//int c = 1, d = 2;
//	//swap(c, d);
//	//cout << c << endl;//2
//	//cout << d << endl;//1
//	return 0;
//}
//
//void TestRef()
//{
//	int a = 10;
//	int& ra = a;//引用定义类型
//	//int& rra; // 引用必须初始化
//	printf("%p\n", &a);//地址一样
//	printf("%p\n", &ra);
//}
//
//int main()
//{
//	TestRef();
//	return 0;
//}
//
////引用的价值
////1.做参数
//void Swap(int& left, int& right)
//{
//	int tmp = left;
//	left = right;
//	right = tmp;
//}
//
//void ListPushBack(LTNode** pphead, int x)
//{}
//void ListPushBack(LTNode*& phead, int x)
//{}
//
////2.做返回值
//int Count1()//传值返回
//{
//	static int n = 10;
//	n++;
//	return n;
//}
//int& Count2()//传引用返回
//{
//	static int n = 10;
//	n++;
//	return n;
//}

//int main()
//{
//	int a = 10;
//	int& ra = a;
//	//ra是a的引用，即ra是a的别名
//	a++;
//	cout << a << endl << ra << endl;
//	ra++;
//	cout << a << endl << ra << endl;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	int b = 1;
//	int& ra = a;
//	int& ra = b;
//	cout << a << endl;
//	cout << b << endl;
//	cout << ra << endl;
//	return 0;
//}

//int main()
//{
//	int a = 10;
//	const int& ra = a;
//	a = 20;
//	ra = 20;
//	return 0;
//}
//
//int main()
//{
//	double d = 2.2;
//	int a = d;
//	const int& e = d;
//	return 0;
//}


//void fun(const int& x)
//{
//
//}
//
//int main()
//{
//	int a = 10;
//	const int& b = a;
//	double d = 1.1;
//	const int& e = d;
//	fun(a);
//	fun(10);
//	fun(b);
//	fun(d);
//	fun(e);
//	return 0;
//}

int Count()
{
	int n = 0;
	n++;
	return n;
}
int main()
{
	const int& ret = Count();
	return 0;
}