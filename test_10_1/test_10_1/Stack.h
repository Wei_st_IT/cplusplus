# pragma once
#include <assert.h>
#include <stdlib.h>

namespace bit
{
	typedef struct Stack
	{
		int* arr;
		int top;
		int capacity;
		//...
	}ST;

void StackInit(ST* ps, int defaultCP = 4);
void StackPush(ST* ps, int x);
}
