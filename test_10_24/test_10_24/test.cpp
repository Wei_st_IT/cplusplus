#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

////交换int类型
//void Swap(int& left, int& right)
//{
//	int temp = left;
//	left = right;
//	right = temp;
//}
////利用C++支持的函数重载交换double类型
//void Swap(double& left, double& right)
//{
//	double temp = left;
//	left = right;
//	right = temp;
//}

//template<typename T>//或者template<class T>
//void Swap(T& left, T& right)
//{
//	T tmp = left;
//	left = right;
//	right = tmp;
//}
//
//int main()
//{
//	int a = 1, b = 9;
//	swap(a, b);
//	double c = 2.2, d = 8.2;
//	swap(c, d);
//	char e = 'e', f = 'f';
//	swap(e, f);
//	return 0;
//}

template<class T>
T Add(const T& left, const T& right)
{
    return left + right;
}
//int main()
//{
//    int a1 = 10, a2 = 20;
//    double d1 = 10.0, d2 = 20.0;
//    Add(a1, a2); //编译器推出T是int
//    Add(d1, d2); //编译器推出T是double
//}

//int main()
//{
//    int a1 = 10, a2 = 20;
//    double d1 = 10.0, d2 = 20.0;
//    Add(a1, d1); //err 编译器推不出来
//    /*
//    该语句不能通过编译，因为在编译期间，当编译器看到该实例化时，需要推演其实参类型
//    通过实参a1将T推演为int，通过实参d1将T推演为double类型，但模板参数列表中只有
//    一个T，编译器无法确定此处到底该将T确定为int 或者 double类型而报错
//    注意：在模板中，编译器一般不会进行类型转换操作，因为一旦转化出问题，编译器就需要背黑锅
//    */
//}

//template<class T>
//T Add(const T& left, const T& right)
//{
//	return left + right;
//}
//
//int main()
//{
//	int a1 = 10, a2 = 20;
//	double d1 = 10.1, d2 = 20.2;
//	// 自动推演实例化
//	cout << Add(a1, a2) << endl;
//	cout << Add(d1, d2) << endl;
//
//	cout << Add((double)a1, d2) << endl;
//	cout << Add(a1, (int)d2) << endl;
//
//	// 显示实例化
//	cout << Add<double>(a1, d2) << endl;
//	cout << Add<int>(a1, d2) << endl;
//
//	return 0;
//}

//
//template<class T1, class T2>
//T1 Add(const T1& left, const T2& right)
//{
//	return left + right;
//}

// 21:12继续
//int main()
//{
//	int a1 = 10, a2 = 20;
//	double d1 = 10.1, d2 = 20.2;
//	// 自动推演实例化
//	cout << Add(a1, a2) << endl;
//	cout << Add(d1, d2) << endl;
//
//	cout << Add(a1, d2) << endl;
//	cout << Add(d1, a2) << endl;
//
//	return 0;
//}


// 专门处理int的加法函数
//int Add(int left, int right) // _Z3Addii
//{
//	return left + right;
//}
//
//// 通用加法函数
//template<class T>
//T Add(T left, T right) // _Z3TAddii
//{
//	return left + right;
//}
//
//int main()
//{
//	int a = 1, b = 2;
//	Add(a, b);
//
//	Add<int>(a, b);
//
//	return 0;
//}


//template<typename T>
//class Stack
//{
//public:
//	Stack(int capacity = 4)
//	{
//		cout << "Stack(int capacity = )" <<capacity<<endl;
//
//		_a = (T*)malloc(sizeof(T)*capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail");
//			exit(-1);
//		}
//
//		_top = 0;
//		_capacity = capacity;
//	}
//	
//	~Stack()
//	{
//		cout << "~Stack()" << endl;
//
//		free(_a);
//		_a = nullptr;
//		_top = _capacity = 0;
//	}
//
//	void Push(const T& x)
//	{
//		// ....
//		// 扩容
//		_a[_top++] = x;
//	}
//
//private:
//	T* _a;
//	int _top;
//	int _capacity;
//};
//
//
//int main()
//{
//	// 类模板一般没有推演时机，函数模板实参传递形参，推演模板参数
//	// 显示实例化
//	// 他们是同一个类模板实例化出来的
//	// 但是模板参数不同，他们就是不同类型
//	Stack<double> st1; // double
//	st1.Push(1.1);
//
//	Stack<int> st2; // int
//	st2.Push(1);
//
//	return 0;
//}


////专门处理int的加法函数
//int Add(int left, int right)
//{
//	return left + right;
//}
////通用加法函数
//template<class T>
//T Add(T left, T right)
//{
//	return left + right;
//}
//int main()
//{
//	Add(1, 2); //会调用哪个Add函数？
//}


#define N  10
#include <assert.h>
namespace bit
{
	template<class T>
	class array
	{
	public:
		inline T& operator[](size_t i)
		{
			assert(i < N);
			return _a[i];
		}
	private:
		T _a[N];
	};
}

//int main()
//{
//	//int a2[10];
//	//a2[20] = 0;
//	//a2[10];
//
//	bit::array<int> a1;
//	for (size_t i = 0; i < N; ++i)
//	{
//		// a1.operator[](i)= i;
//
//		a1[i] = i;
//	}
//
//	for (size_t i = 0; i < N; ++i)
//	{
//		// a1.operator[](i)
//		cout << a1[i] << " ";
//	}
//	cout << endl;
//
//	for (size_t i = 0; i < N; ++i)
//	{
//		a1[i]++;
//	}
//
//	for (size_t i = 0; i < N; ++i)
//	{
//		cout << a1[i] << " ";
//	}
//	cout << endl;
//
//	//a1[20];
//	//a1[10];
//
//
//	return 0;
//}


//template<class T>
//T* func(int n)
//{
//	return new T[n];
//}
//
//int main()
//{
//	int* p = func(10);
//}
//int main()
//{
//	//函数模板显示实例化
//	int* p1 = func<int>(10);
//	double* p2 = func<double>(10);
//}

#include "Stack.h"
int main()
{
	Stack<int> st;
	st.Push(1);
	st.Push(2);

	Stack<double> stt;
	stt.Push(1.1);
	stt.Push(2.2);

	return 0;
}