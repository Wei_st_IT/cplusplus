#pragma once

// vector的模拟实现
namespace vector_realize
{
	template<class T>
	class vector
	{
	public:
		typedef T* iterator;

		iterator begin()
		{
			return _start;
		}

		iterator end()
		{
			return _finish;
		}

		typedef const T* const_iterator;

		const_iterator begin() const
		{
			return _start;
		}

		const_iterator end() const
		{
			return _finish;
		}

		// 构造函数
		vector() // --> 无参构造
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{}

		// --> 迭代器构造
		// 若使用iterator做迭代器，会导致初始化的迭代器区间[first,last)只能是vector的迭代器
		// 重新声明迭代器，迭代器区间[first,last)可以是任意容器的迭代器
		template<class InputIterator>
		vector(InputIterator first, InputIterator last)
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			while (first != last)
			{
				push_back(*first);
				first++;
			}
		}

		//vector<int> v1(10, 5);
		//vector<char> v2(10, 'A');
		// 带参构造 --> 初始化容器为n个val
		vector(size_t n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		// 重载1
		vector(long n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			reserve(n);
			for (size_t i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		// 重载2
		vector(int n, const T& val = T())
			:_start(nullptr)
			, _finish(nullptr)
			, _endofstorage(nullptr)
		{
			reserve(n);
			for (int i = 0; i < n; i++)
			{
				push_back(val);
			}
		}

		/*
		* 重载原因解释：
		* 理论上将，提供了vector(size_t n, const T& value = T())之后
		* vector(int n, const T& value = T())就不需要提供了，但是对于：
		* vector<int> v(10, 5);
		* 编译器在编译时，认为T已经被实例化为int，而10和5编译器会默认其为int类型
		* 就不会走vector(size_t n, const T& value = T())这个构造方法，
		* 最终选择的是：vector(InputIterator first, InputIterator last)
		* 因为编译器觉得区间构造两个参数类型一致，因此编译器就会将InputIterator实例化为int
		* 但是10和5根本不是一个区间，编译时就报错了
		* 故需要增加该构造方法
		*/

		// 拷贝构造 v1(v)
		// 传统写法
		vector(const vector<T>& v)
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			_start = new T[v.capacity()]; // 开辟一块和v大小相同的空间
			//memcpy(_start, v._start, sizeof(T) * size()); //error
			for (size_t i = 0; i < v.size(); i++)
			{
				_start[i] = v[i];
				//_start[i] = v._start[i];
			}

			_finish = _start + v.size();
			_endofstorage = _start + v.capacity();
		}

		// 现代写法1
		/*vector(const vector<T>& v)
			:_start(nullptr)
			,_finish(nullptr)
			,_endofstorage(nullptr)
		{
			reserve(v.capacity());
			for (const auto& e : v)
			{
				push_back(e);
			}
		}*/

		// 现代写法2
		//vector(const vector<T>& v)
		//	:_start(nullptr)
		//	, _finish(nullptr)
		//	, _endofstorage(nullptr)
		//{
		//	vector<T> tmp(v.begin(), v.end()); // 调用迭代器构造函数
		//	swap(tmp);
		//}

		// 析构函数
		~vector()
		{
			if (_start)
			{
				delete[] _start;
				_start = _finish = _endofstorage = nullptr;
			}
		}

		// 赋值运算符重载
		// 传统写法
		vector<T>& operator=(const vector<T>& v)
		{
			if (this != &v)
			{
				T* tmp = new T[v.capacity()];
				for (size_t i = 0; i < v.size(); i++)
				{
					//tmp[i] = v._start[i];
					tmp[i] = v[i];
				}
				_start = tmp;

				_finish = _start + v.size();
				_endofstorage = _start + v.capacity();
			}

			return *this;
		}
		// 现代写法
		/*vector<T>& operator=(const vector<T>& v)
		{
			if (this != &v)
			{
				vector<T> tmp(v);
				swap(tmp);
			}

			return *this;
		}*/
		// 简化版 -- 不能检查自己给自己赋值
		// v1 = v2; v1 = v1;
		/*vector<T>& operator=(vector<T> v)
		{
			swap(v);
			return *this;
		}*/

		size_t size() const
		{
			return _finish - _start;
		}

		size_t capacity() const
		{
			return _endofstorage - _start;
		}

		bool empty() const
		{
			return _endofstorage == _finish;
		}

		T& operator[](size_t n)
		{
			assert(n < size());
			return _start[n];
		}

		const T& operator[](size_t n) const
		{
			assert(n < size());
			return _start[n];
		}

		T& front()
		{
			return *_start;
		}

		T& back()
		{
			return *(_finish - 1);
		}

		const T& front() const
		{
			return *_start;
		}

		const T& back() const
		{
			return *(_finish - 1);
		}

		void reserve(size_t n)
		{
			size_t oldSize = size();
			if (capacity() < n)
			{
				// 1.开辟新空间
				T* tmp = new T[n];
				if (_start)
				{
					//2.拷贝元素
					// 这里直接用memcpy会有问题，发生浅拷贝
					//memcpy(tmp, _start, sizeof(T) * size());
					for (size_t i = 0; i < oldSize; i++)
					{
						tmp[i] = _start[i]; // 本质调用赋值运算符重载进行深拷贝
					}
					//3. 释放旧空间
					delete[] _start;
				}
				_start = tmp;
			}
			// 这里_start的位置变了，而_finish还是原来的位置
			//_finish = _start + size(); error 
			_finish = _start + oldSize;
			_endofstorage = _start + n;
		}

		void push_back(const T& x)
		{
			if (_finish == _endofstorage)
			{
				int newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
			}
			*_finish = x;
			_finish++;

			//复用：insert(end() - 1);
		}

		//假如size = 5, capacity = 10
		// n > 10 扩容+填充数据
		// 5 < n <= 10 填充数据
		// n <= 5 删除数据
		void resize(size_t n, const T val = T())
		{
			// 1.空间不够增容
			if (n > capacity())
			{
				reserve(n);
			}
			// 2.n > size(),填充数据,将size()扩大到n
			if (n > size())
			{
				while (_finish < _start + n)
				{
					*_finish = val;
					_finish++;
				}
			}
			else
			{
				//3.n < size(), 将数据个数缩小到n
				_finish = _start + n;
			}
		}

		void pop_back()
		{
			if (_finish > _start)
			{
				_finish--;
			}
			/*assert(!empty());
			_finish--;*/

			// 复用：erase(end() - 1);
		}

		//void insert(iterator pos, const T& x)
		//{
		//	//检测参数合法性
		//	assert(pos >= _start);
		//	assert(pos <= _finish);
		//	//检测是否需要扩容
		//	if (_finish == _endofstorage)
		//	{
		//		size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
		//		reserve(newcapcacity);
		//	}
		//	//挪动数据
		//	iterator end = _finish - 1;
		//	while (end >= pos)
		//	{
		//		*(end + 1) = *end;
		//		end--;
		//	}
		//	//插入指定的数据
		//	*pos = x;
		//	_finish++;
		//}

		//void insert(iterator pos, const T& x)
		//{
		//	//检测参数合法性
		//	assert(pos >= _start);
		//	assert(pos <= _finish);
		//	//检测是否需要扩容,扩容以后pos就失效了，需要更新一下
		//	if (_finish == _endofstorage)
		//	{
		//		size_t n = pos - _start;//计算pos和start的相对距离
		//		size_t newcapcacity = capacity() == 0 ? 4 : capacity() * 2;
		//		reserve(newcapcacity);
		//		// 扩容会导致pos迭代器失效，需要更新处理一下
		//		pos = _start + n;//防止迭代器失效，要让pos始终指向与_start间距n的位置
		//	}
		//	//挪动数据
		//	iterator end = _finish - 1;
		//	while (end >= pos)
		//	{
		//		*(end + 1) = *end;
		//		end--;
		//	}
		//	//插入指定的数据
		//	*pos = x;
		//	_finish++;
		//}

		// 迭代器失效 : 野指针问题
		// 不能保证原地扩容，扩容就会导致迭代器失效：地址变化了 --> 
		iterator insert(iterator pos, const T& val)
		{
			assert(pos >= _start);
			assert(pos <= _finish);
			if (_finish == _endofstorage)
			{
				size_t len = pos - _start;
				int newCapacity = capacity() == 0 ? 4 : capacity() * 2;
				reserve(newCapacity);
				// 扩容会导致迭代器失效，需要重新修改数据pos
				pos = _start + len;
			}
			// 挪动数据
			iterator end = _finish - 1;
			while (end >= pos)
			{
				*(end + 1) = *end;
				end--;
			}
			// 插入数据
			*pos = val;
			_finish++;

			return pos;
		}

		// 返回删除数据的下一个数据
		// 方便解决:一边遍历一边删除的迭代器失效问题
		iterator erase(iterator pos)
		{
			assert(pos >= _start);
			assert(pos < _finish);

			iterator begin = pos + 1;
			while (begin < _finish)
			{
				*(begin - 1) = *begin;
				begin++;
			}
			_finish--;

			return pos; //返回pos的下一个位置
		}

		void swap(vector<T>& v) // 不加const
		{
			// 调用算法库里的swap
			std::swap(_start, v._start);
			std::swap(_finish, v._finish);
			std::swap(_endofstorage, v._endofstorage);
		}

		void clear()
		{
			_finish = _start;
		}

	private:
		iterator _start;        //指向容器的头
		iterator _finish;       //指向有效数据的尾
		iterator _endofstorage; //指向容器的尾
	};

	void test_vector1()
	{
		vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);

		for (size_t i = 0; i < v.size(); ++i)
		{
			cout << v[i] << " ";
		}
		cout << endl;

		vector<int>::iterator it = v.begin();
		while (it != v.end())
		{
			cout << *it << " ";
			++it;
		}
		cout << endl;

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;

		v.pop_back();
		v.pop_back();
		v.pop_back();
		v.pop_back();
		v.pop_back();

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	void test_vector2()
	{
		std::vector<int> v;
		//v.reserve(10);
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		cout << v.size() << ":" << v.capacity() << endl;
		std::vector<int>::iterator it = find(v.begin(), v.end(), 4);
		if (it != v.end())
		{
			v.erase(it);
		}
		cout << *it << endl; // 读
		(*it)++; // 写
		cout << *it << endl << endl;
		cout << v.size() << ":" << v.capacity() << endl;
		for (auto e : v)
		{
			cout << e << " ";
		}
	}

	//void test_vector2()
	//{
	//	vector<int> v;
	//	//v.reserve(10);
	//	v.push_back(1);
	//	v.push_back(2);
	//	v.push_back(3);
	//	v.push_back(4);
	//	vector<int>::iterator it = find(v.begin(), v.end(), 1);

	//	if (it != v.end())
	//	{
	//		v.insert(it, 20);
	//	}

	//	cout << *it << endl;
	//}

	void test_vector7()
	{
		//删除所有的偶数
		std::vector<int> v;
		//v.reserve(10);
		// 第一组测试用例:
		v.push_back(1);
		v.push_back(2);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.push_back(5);
		auto it = v.begin();
		while (it != v.end())
		{
			if (*it % 2 == 0)
			{
				v.erase(it);
			}
			it++;
		}
		for (auto e : v)
		{
			cout << e << " ";
		}
	}

	void test_vector3()
	{
		std::vector<int> v;
		//v.reserve(10);
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);

		std::vector<int>::iterator it = find(v.begin(), v.end(), 1);
		//v.insert(v.begin(), 0);

		/*while (it!= v.end())
		{
			if (*it % 2 == 0)
			{
				v.insert(it, 20);
			}
			it++;
		}*/
		if (it != v.end())
		{
			v.insert(it, 20);
		}
		
		cout << *it << endl;
	}

	void test_vector4()
	{
		std::vector<int> v;
		v.push_back(1);
		v.push_back(2);
		v.push_back(3);
		v.push_back(4);
		v.insert(v.begin(), 0);

		// it失效
		std::vector<int>::iterator it = find(v.begin(), v.end(), 3);
		if (it != v.end())
		{
			v.erase(it);
		}

		// 读 
		cout << *it << endl;
		// 写
		(*it)++;

		for (auto e : v)
		{
			cout << e << " ";
		}
		cout << endl;
	}

	/*class Solution {
	public:
		vector<vector<int>> generate(int numRows) {
			vector<vector<int>> vv;
			vv.resize(numRows);
			for (size_t i = 0; i < vv.size(); ++i)
			{
				vv[i].resize(i + 1, 0);
				vv[i][0] = vv[i][vv[i].size() - 1] = 1;
			}

			for (size_t i = 0; i < vv.size(); ++i)
			{
				for (size_t j = 0; j < vv[i].size(); ++j)
				{
					if (vv[i][j] == 0)
					{
						vv[i][j] = vv[i - 1][j] + vv[i - 1][j - 1];
					}
				}
			}

			return vv;
		}
	};*/

	class Solution {
	public:
		// 核心思想：找出杨辉三角的规律，发现每一行头尾都是1，中间第[j]个数等于上一行[j-1]+[j]
		vector<vector<int>> generate(int numRows) {
			vector<vector<int>> vv;
			// 先开辟杨辉三角的空间
			vv.resize(numRows);
			for (size_t i = 1; i <= numRows; ++i)
			{
				vv[i - 1].resize(i, 0);
				// 每一行的第一个和最后一个都是1
				vv[i - 1][0] = 1;
				vv[i - 1][i - 1] = 1;
			}
			for (size_t i = 0; i < vv.size(); ++i)
			{
				for (size_t j = 0; j < vv[i].size(); ++j)
				{
					if (vv[i][j] == 0)
					{
						vv[i][j] = vv[i - 1][j - 1] + vv[i - 1][j];
					}
				}
			}
			return vv;
		}
	};

	

	void test_vector9()
	{

		vector<vector<int>> vv;
		vector<int> v(5, 1);
		vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);
		vv.push_back(v);

		for (size_t i = 0; i < vv.size(); ++i)
		{
			for (size_t j = 0; j < vv[i].size(); ++j)
			{
				cout << vv[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;

		/*vector<vector<int>> vvRet = Solution().generate(5);
		for (size_t i = 0; i < vvRet.size(); ++i)
		{
			for (size_t j = 0; j < vvRet[i].size(); ++j)
			{
				cout << vvRet[i][j] << " ";
			}
			cout << endl;
		}
		cout << endl;*/
	}
}