#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>
#include <cassert>
using namespace std;

void test1()
{
	// insert 效率低下，尽量少用
	string s1("hello string");
	s1.insert(0, "wei");
	s1.insert(11, "stu");
	s1.insert(5, "bit");
	s1.erase(0, 3);
	s1.erase(2, 3);
	s1.erase(8, 3);
	cout << s1 << endl;

	string s2("");
	s2.insert(0, 10, 'f');
	s2.erase(0, 5);
	cout << s2 << endl;
}

void test2()
{
	string s1("hello world hello world");
	string s2 = "hello world hello world";
	string s3(s1);

	s1.assign("hello string");
	//s2.assign("hello string", 5);

	//s3.replace(6,5,"stdiofcd");
	
	//将空格替换为“%20”
	size_t pos = s2.find(' ');
	while (pos != string::npos)
	{
		s2.replace(pos, 1, "%20");
		pos = s2.find(' ', pos + 3);
	}

	string ret;
	ret.reserve(s3.size());
	for (auto ch : s3)
	{
		if (ch == ' ')
		{
			ret += "%20";
		}
		else
		{
			ret += ch;
		}
	}

	cout << s1 << endl;
	cout << s2 << endl;
	cout << s3 << endl;
	cout << ret << endl;
}

void test3()
{
	string file("string.cpp");
	FILE* fout = fopen(file.c_str(), "r");
	assert(fout);
	
	char ch = fgetc(fout);
	while (ch != EOF)
	{
		cout << ch;
		ch = fgetc(fout);
	}
	fclose(fout);
}

void test4()
{
	string file;
	cin >> file;
	size_t pos = file.rfind('.');
	//取后缀
	if (pos != string::npos)
	{
		//string suffix = file.substr(pos, file.size() - pos);
		string suffix = file.substr(pos);
		cout << suffix << endl;
	}
}

void test5()
{
	string s1("please tell me how to do it");
	size_t pos = s1.find_first_of("aeiou"); // 缺省值默认为0
	while (pos != string::npos)
	{
		s1[pos] = '*';
		pos = s1.find_first_of("aeiou", pos + 1);
	}
	cout << s1 << endl;
}

//int main()
//{
//	//test1();
//	//test2();
//	//test3();
//	//test4();
//	test5();
//	return 0;
//}

//void test()
//{
//	string s1("hello");
//	s1.at(6) = 'x';
//}
//int main()
//{
//	try
//	{
//		test();
//	}
//	catch (const exception& e)
//	{
//		cout << e.what() << endl;
//	}
//	return 0;
//}


//void test()
//{
//	string str("hello world");
//	// 
//	str.assign("str");
//	cout << str << endl;
//	//
//	str.assign("tostring", 5, 2);
//	cout << str << endl;
//
//	//
//	str.assign("string", 3);
//	cout << str << endl;
//
//	//
//	str.assign(5, 'a');
//	cout << str << endl;
//
//	//
//	str.assign(str.begin(), str.end()-2);
//	cout << str << endl;
//
//}

//void test()
//{
//	string s1("hello");
//	cout << s1 << endl; //hello
//	cout << s1.data() << endl;//hello
//}


//void test()
//{
//	string s("hello");
//	char arr[] = "abedef";
//	s.copy(arr, 2, 2);
//	cout << s << endl;
//}

//void test()
//{
//	string s("hello world");
//	string str = s.substr(2, 6);
//	cout << str << endl;
//}

//void test()
//{
//	string str1("green apple");
//	string str2("red apple");
//
//	if (str1.compare(str2) != 0) // 不等
//		cout << str1 << " is not " << str2 << '\n'; 
//	// 取str1从下标为6开始的5个字符，相等
//	if (str1.compare(6, 5, "apple") == 0) 
//		cout << str1 << "\n"; // 
//	// 取下标为size()-5位置开始的5个字符，相等
//	if (str2.compare(str2.size() - 5, 5, "apple") == 0)
//		cout << str2 << "\n";
//}

void test()
{
	string str("how old are you?");
	size_t found = str.find_first_of("aeiou");
	while (found != string::npos)
	{
		str.erase(found, 1);
		found = str.find_first_of("aeiou", found);
	}
	cout << str << '\n';
}

int main()
{
	test();
	return 0;
}