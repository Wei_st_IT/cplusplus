#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>

using namespace std;

//class A
//{
//public:
//	A()
//	{
//		_a = 0;
//		cout << "A()构造函数" << endl;
//	}
//private:
//	int _a;
//};
//
//class Date
//{
//public:
//	void Print()
//	{
//		cout << _year << "-" << _month << "-" << _day << endl;
//	}
//private:
//	int _year = 2000;
//	int _month = 12;
//	int _day = 31;
//	A _aa;
//};
//int main()
//{
//	Date d1;
//	d1.Print();
//}

//class Stack
//{
//public:
//	//显示定义构造函数
//	Stack(int capacity = 4)
//	{
//		_a = (int*)malloc(sizeof(int) * capacity);
//		if (_a == nullptr)
//		{
//			perror("malloc fail\n");
//			exit(-1);
//		}
//		_top = 0;
//		_capacity = capacity;
//		cout << "Stack构造函数" << endl;
//	}
//
//	void Push(int x)
//	{
//		_a[_top++] = x;
//	}
//
//private:
//	int* _a;
//	int _top;
//	int _capacity;
//};

//int main()
//{
//	Stack st;
//	st.Push(1);
//	st.Push(2);
//	return 0;
//}

//class MyQueue
//{
//public:
//	void Push(int x)
//	{
//		_pushST.Push(x);
//	}
//
//	Stack _pushST;
//	Stack _popST;
//};
//
//int main()
//{
//	MyQueue mq;
//	mq.Push(1);
//	mq.Push(2);
//	return 0;
//}

//class Date
//{
//public:
//	Date(int year = 1, int month = 1, int day = 1)
//	{
//		_year = year;
//		_month = month;
//		_day = day;
//	}
//	Date(Date& d)
//	{
//		d._year = _year;
//		d._month = _month;
//		d._day = _day;
//	}
//	void Print()
//	{
//		cout << _year << 
//			"-" << _month << 
//			"-" << _day << endl;
//	}
//private:
//	//内置类型
//	int _year;
//	int _month;
//	int _day;
//};

//int main()
//{
//	Date d1;
//	Date d2(d1);
//	return 0;
//}

//Date(int year, int month, int day)
//{
//	_year = year;
//	_month = month;
//	_day = day;
//}

/*void Init(int year, int month, int day)
	{
		_year = year;
		_month = month;
		_day = day;
	}*/

//Date(Date& d)
//{
//	_year = d._year;
//	_month = d._month;
//	_day = d._day;
//}

class Stack
{
public:
	Stack(int capacity = 4)
	{
		_a = (int*)malloc(sizeof(int) * capacity);
		if (_a == nullptr)
		{
			perror("malloc fail\n");
			exit(-1);
		}
		_top = 0;
		_capacity = capacity;
		cout << "Stack 构造" << endl;
	}
	//不写拷贝构造，编译器调用默认拷贝构造
	/*
	浅拷贝
	Stack(const Stack& st)
	{
		_a = st._a;
		_top = st._top;
		_capacity = st._capacity;
	}
	*/
	~Stack()
	{
		free(_a);
		_a = NULL;
		_top = _capacity = 0;
	}

	void Push(int x)
	{
		_a[_top++] = x;
	}

private:
	int* _a;
	int _top;
	int _capacity;
};

int main()
{
	Stack st1;
	st1.Push(1);
	st1.Push(1);
	Stack st2(st1);
	st2.Push(2);
	return 0;
}