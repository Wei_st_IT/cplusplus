#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <string>

using namespace std;

//class Person
//{
//public:
//	Person(const char* name = "peter")
//		: _name(name)
//	{
//		cout << "Person()" << endl;
//	}
//
//	Person(const Person& p)
//		: _name(p._name)
//	{
//		cout << "Person(const Person& p)" << endl;
//	}
//
//	Person& operator=(const Person& p)
//	{
//		cout << "Person operator=(const Person& p)" << endl;
//		if (this != &p)
//			_name = p._name;
//
//		return *this;
//	}
//
//	~Person()
//	{
//		cout << "~Person()" << endl;
//		// delete[] p;
//	}
//protected:
//	string _name; // 姓名
//
//	// int* p = new int[10]; 
//};
//
//
////子类
//class Student : public Person
//{
//	//子类构造函数原则：a、调用父类构造函数初始化继承父类的成员。b、自己再初始化自己的成员
//	//析构、拷贝构造、赋值重载也是类似
//public:
//	/*1、构造函数*/
//	Student(const char* name, int num = 10)
//		:Person(name)//调用基类的构造函数初始化基类的那一部分成员,不允许初始化父类的成员 错误：_name(name)
//		, _num(num)//初始化派生类成员
//		, _address("上海")
//	{
//		cout << "Student(const char* name, int num)" << endl;
//	}
//	/*2、拷贝构造函数*/
//	Student(const Student& s)
//		:Person(s)//调用基类的拷贝构造函数完成基类成员的拷贝构造
//		, _num(s._num)//拷贝构造派生类成员
//	{
//		cout << "Student(const Student& s)" << endl;
//	}
//	/*3、赋值运算符重载函数*/
//	Student& operator=(const Student& s)
//	{
//		if (this != &s)
//		{
//			//子类operator=和父类operator=构成隐藏，为了避免无穷递归，要加上指定父类作用域
//			Person::operator=(s);//调用基类的operator=完成基类成员的赋值，显式调用父类赋值重载，防止自己不断调用自己
//			_num = s._num;//完成派生类的赋值
//		}
//		cout << "Student& operator=(const Student& s)" << endl;
//		return *this;
//	}
//	/*4、析构函数*/
//	// 第一怪：1、子类析构函数和父类析构函数构成隐藏关系。（由于多态关系需求，所有析构函数都会特殊处理成destructor函数名）
//	// 第二怪：2、子类先析构，父类再析构。子类析构函数不需要显示调用父类析构，子类析构后会自动调用父类析构
//	~Student()
//	{
//		// Person::~Person(); 不需要显示调用父类析构
//		cout << "~Student()" << endl;
//	}//自动调用父类的析构函数
//protected:
//	int _num; //学号
//	string _address; // 地址
//};
//
//int main()
//{
//	Student s("张三");
//
//	return 0;
//}


//// 友元关系不继承
//class Student;
//class Person
//{
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	string _name; // 姓名
//};
//class Student : public Person
//{
//	// 想访问Display函数，同时变成子类的友元即可
//public:
//	friend void Display(const Person& p, const Student& s);
//protected:
//	int _stuNum; // 学号
//};
//
//void Display(const Person& p, const Student& s)
//{
//	//Display是父类的友元，不是子类的友元
//	cout << p._name << endl;//可以访问父类
//	// cout << s._stuNum << endl; 错误 不能访问子类
//}
//int main()
//{
//	Person p;
//	Student s;
//	Display(p, s);
//}


//class Person
//{
//public:
//	Person() { ++_count; }
//
//	void Print()
//	{
//		cout << this << endl;
//		cout << _name << endl;
//		cout << _count << endl;
//	}
//
////protected:
//	string _name; // 姓名
//public:
//	static int _count; // 统计人的个数。
//};
//
//int Person::_count = 0;
//
//class Student : public Person
//{
//protected:
//	int _stuNum; // 学号
//};
//
//// 静态成员属于整个类，所有对象。同时也属于所有派生类及对象
//int main()
//{
//	Person p;
//	Student s;
//	p._name = "张三";
//	s._name = "李四";
//	p._count++;
//	s._count++;
//	cout << p._count << endl;
//	cout << s._count << endl;
//	cout << &p._count << endl;
//	cout << &s._count << endl;
//	cout << Person::_count << endl;
//	cout << Student::_count << endl;
//
//	Person* ptr = nullptr;
//	//cout << ptr->_name << endl;  // no
//	ptr->Print();                // ok
//	cout << ptr->_count << endl; // ok
//
//	(*ptr).Print();             // ok
//	cout << (*ptr)._count << endl; // ok
//
//	return 0;
//}


class Person
{
public:
	string _name; // 姓名
};

class Student : public Person
{
protected:
	int _num; // 学号
};

class Teacher : public Person
{
protected:
	int _id; // 教职工编号
};

class Assistant : public Student, public Teacher
{
protected:
	string _majorCourse; // 主修课程
};

// 数据冗余/二义性
//int main()
//{
//	Assistant a;
//	a.Student::_name = "张三";
//	a.Teacher::_name = "罗老师";
//	return 0;
//}


class A
{
public:
	int _a;
};
class B : public A
// class B : virtual public A
{
public:
	int _b;
};
class C : public A
//class C :virtual public A
{
public:
	int _c;
};
class D : public B, public C
{
public:
	int _d;
};

int main()
{
	D d;
	d._b = 1;
	d._c = 2;
	d._d = 3;
	d.B::_a = 4;
	d.C::_a = 5;

	return 0;
}


