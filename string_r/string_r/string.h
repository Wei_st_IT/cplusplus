#pragma once
#include <iostream>
#include <cstring>
#include <cassert>
using namespace std;

namespace string_r
{
	class string
	{
	public:
		string(const char* str)
		{
			_capacity = strlen(str);
			_size = _capacity;
			_str = new char[_capacity + 1];
		}

		~string()
		{
			delete _str;
			_str = nullptr;
			_size = _capacity = 0;
		}
		// 传统写法
		string(const string& str)
		{
			_str = new char[_capacity + 1];
			_size = str._size;
			_capacity = str._capacity;
			strcpy(_str, str._str);
		}
		// 现代写法
		string(const string& str)
			:_str(nullptr)
			,_size(0)
			,_capacity(0)
		{
			string tmp(str._str);
			swap(tmp);
		}
		// 传统写法
		string& operator=(const string& str)
		{
			delete _str;
			_str = new char[_capacity + 1];
			_size = str._size;
			_capacity = str._capacity;
			strcpy(_str, str._str);

			return *this;
		}

		// 现代写法 
		string& operator=(const string& str)
		{
			if (this != &str)
			{
				string tmp(str._str);
				swap(tmp);
				return *this;
			}
		}
		// 简洁版：
		string& operator=(string str)
		{
			swap(str);
			return *this;
		}

		size_t size() const
		{
			return _size;
		}

		size_t capacity() const
		{
			return _capacity;
		}

		void clear() 
		{
			_size = 0;
			_str[0] = '\0';
		}

		void reverse(size_t n)
		{
			if (n > _capacity)
			{
				char* tmp = new char[n + 1];
				strcpy(tmp, _str);
				delete _str;
				_str = tmp;
				_capacity = n;
			}
		}

		void resize(size_t n, char ch = '\0')
		{
			if (n > _size)
			{
				reverse(n);
				_size = n;

				for (int i = _size; i < n; i++)
				{
					_str[i] = ch;
				}
			}
			else
			{
				_str[n] = '\0';
				_size = n;
			}
		}

		void push_back(char ch)
		{
			if (_size == _capacity)
			{
				int newCapacity = _capacity == 0 ? 4 : _capacity * 2;
				reverse(newCapacity);
			}
			_str[_size] = ch;
			_size++;
			_str[_size] = '\0';
		}

		string& append(const char* str)
		{
			size_t len = strlen(str);

			if (_size + len > _capacity)
			{
				reverse(_size + len);
			}
			strcpy(_str + _size, str);
		}

		string& operator+=(const char* str)
		{
			append(str);
			return *this;
		}

		string& operator+=(char ch)
		{
			push_back(ch);
			return *this;
		}

		string& insert(size_t pos, char ch)
		{
			assert(pos < _size);
			if (_size == _capacity)
			{
				int newCapacity = _capacity == 0 ? 4 : _capacity * 2;
				reverse(newCapacity);
			}
			/*int end = _size;
			while (end >= (int)pos)
			{
				_str[end + 1] = _str[end];
				end--;
			}*/
			int end = _size + 1;
			while (end > pos)
			{
				_str[end] = _str[end - 1];
				end--;
			}
			_str[pos] = ch;
			_size++;
			return *this;
		}


		string& insert(size_t pos, const char* str)
		{
			assert(pos < _size);
			if (_size == _capacity)
			{
				int newCapacity = _capacity == 0 ? 4 : _capacity * 2;
				reverse(newCapacity);
			}
			int len = strlen(str);
			/*int end = _size;
			while (end >= (int)pos)
			{
				_str[end + len] = _str[end];
				end--;
			}*/

			int end = _size + len;
			while (end > pos)
			{
				_str[end] = _str[end - len];
				end--;
			}

			strncpy(_str + pos, str, len);
			return *this;
		}

		string& erase(size_t pos, size_t len = npos)
		{
			if (len == npos || pos + len >= _size)
			{
				pos = '\0';
				_size = pos;
			}
			else
			{
				int begin = pos;
				while (begin < pos + len)
				{
					_str[begin] = _str[begin + len];
					begin++;
				}
				_size -= len;
			}

			return *this;
		}

		char& operator[](size_t pos)
		{
			assert(pos <= _size);
			return _str[pos];
		}

		const char& operator[](size_t pos) const
		{
			assert(pos <= _size);
			return _str[pos];
		}

		typedef char* iterator;

		iterator begin()
		{
			return _str;
		}

		iterator end()
		{
			return _str + _size;
		}

		typedef char* const_iterator;

		const_iterator begin() const
		{
			return _str;
		}
		
		const_iterator end() const
		{
			return _str + _size;
		}

		size_t find(char ch, size_t pos = 0) const
		{
			assert(pos < _size);
			for (int i = pos; i < _size; i++)
			{
				if (_str[i] == ch)
				{
					return i;
				}
			}
			return npos; // -1
		}

		size_t find(const char* str, size_t pos = 0) const
		{
			assert(pos < _size);
			const char* ptr = strstr(_str + pos, str);// 返回子字符串的地址
			if (ptr == nullptr)
			{
				return npos;
			}
			else
			{
				return ptr - _str;
			}
		}

		const char* c_str() const
		{
			return _str;
		}

		void swap(string& str)
		{
			std::swap(_str, str._str);
			std::swap(_size, str._size);
			std::swap(_capacity, str._capacity);
		}

	private:
		char* _str;
		size_t _capacity;
		size_t _size;

		const static int npos = -1;
	};

	//1、operator<
	bool operator<(const string& str1, const string& str2)
	{
		return strcmp(str1.c_str(), str2.c_str()) < 0;
	}
	//2、operator==
	bool operator==(const string& str1, const string& str2)
	{
		return strcmp(str1.c_str(), str2.c_str()) == 0;
	}
	//3、operator<=
	bool operator<=(const string& str1, const string& str2)
	{
		return str1 < str2 || str1 == str2;
	}
	//4、operator>
	bool operator>(const string& str1, const string& str2)
	{
		return !(str1 <= str2);
	}
	//5、operator>=
	bool operator>=(const string& str1, const string& str2)
	{
		return !(str1 < str2);
	}
	//6、operator!=
	bool operator!=(const string& str1, const string& str2)
	{
		return !(str1 == str2);
	}

	// <<流插入运算符
	ostream& operator<<(ostream& out, const string& str)
	{
		for (size_t i = 0; i < str.size(); i++)
		{
			out << str[i];
		}
		return out;
	}

	//>>流提取运算符重载
	istream& operator>>(istream& in, string& str)
	{
		//法二：
		//要先把原字符串的所有数据给清空才可以输入新的数据，否则会累加到原数据后面，出错
		str.clear();
		char ch;
		ch = in.get();//使用get()函数才能获取空格或者换行字符
		char buff[128] = { '\0' };
		size_t i = 0;
		while (ch != ' ' && ch != '\n')
		{
			buff[i++] = ch;
			if (i == 127)
			{
				str += buff;
				memset(buff, '\0', 128);
				i = 0;
			}
			ch = in.get();
		}
		str += buff;
		return in;
	}

	istream& getline(istream& in, string& str)
	{
		str.clear();
		char ch;
		ch = in.get();
		while (ch != '\n') // getline只有遇到换行才停止输入
		{
			str += ch;
			ch = in.get();
		}
		return in;
	}
}

