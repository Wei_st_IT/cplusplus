#define _CRT_SECURE_NO_WARNINGS 1
#include <iostream>
#include <queue>
#include <vector>
#include <functional>

using namespace std;

void test_priority_queue()
{
	// 默认是大的优先级高 ——> 默认给的仿函数为less
	priority_queue<int> pq;
	// 控制小的优先级高 ——> 给一个greater的仿函数
	//priority_queue<int, vector<int>, greater<int>> pq;
	pq.push(3);
	pq.push(1);
	pq.push(7);
	pq.push(9);
	pq.push(3);

	while (!pq.empty())
	{
		cout << pq.top() << " ";
		pq.pop();
	}
}

int main()
{
	test_priority_queue();
	return 0;
}